#include "critical_cliques.hpp"

#include <cassert>
#include <iostream>
#include <vector>
#include <tuple>

/*!
 * Compute the critical clique decomposition in O(n+mlogn)
 */
vector<vector<int>> get_critical_clique_decomposition(const Graph & G) {
    int n = G.get_vertices_count();

    /**
     * First phase.
     *
     * We create a 'sorted_closed_neighborhoods' of 3-tuples (|N[v]|, N[v], v),
     * where |N[v]| is the size of the closed neighborhood of v, N[v] is ascending SORTED closed neighborgood of v, and vertex v.
     * There is an entry for each vertex v of G in sorted_closed_neighborhoods.
     */
    vector<std::tuple<int, vector<int>, int>> sorted_closed_neighborhoods;
    for(int v = 0; v < n; ++v) {
        auto adj = G.get_adjacency_list(v);

        vector<int> closed_neighborhood(adj.begin(), adj.end());
        closed_neighborhood.push_back(v);

        sort(closed_neighborhood.begin(), closed_neighborhood.end());

        sorted_closed_neighborhoods.push_back(make_tuple(closed_neighborhood.size(), closed_neighborhood, v));
    }

    /**
     * Second phase.
     *
     * We sort the 'sorted_closed_neighborhoods' lexicographically. Observe, that after the sort,
     * the list 'sorted_closed_neighborhoods' is sorted first by the size of the closed neighborhoods,
     * then by the closed neighborhoods themselves.
     *
     * This means, that the vertices, that have the same closed neighborhoods, will form a contiguous segment in the 'sorted_closed_neighborhoods' list,
     * as the closed neighborhoods have the same size and contain same vertices.
     */
    sort(sorted_closed_neighborhoods.begin(), sorted_closed_neighborhoods.end());


    /**
     * Third phase.
     *
     * Construction of the 'critical_clique_decomposition'.
     * We iterate over the 'sorted_closed_neighborhoods' and check if adjacent entries have the same closed neighborhoods.
     * If they do, they are in the same critical clique.
     * If they don't, they are in different critical cliques.
     *
     * Since all vertices of each critical clique form a contiguous segment in 'sorted_closed_neighborhoods',
     * the check of adjacent entries is sufficient to uncover all critical cliques.
     */
    vector<vector<int>> critical_clique_decomposition;

    critical_clique_decomposition.push_back({std::get<2>(sorted_closed_neighborhoods[0])});
    for(int i = 1; i < n; ++i) {
        if(std::get<1>(sorted_closed_neighborhoods[i]) != std::get<1>(sorted_closed_neighborhoods[i-1]))  {
            critical_clique_decomposition.push_back({});
        }
        critical_clique_decomposition.back().push_back(std::get<2>(sorted_closed_neighborhoods[i]));
    }


    /**
     * Optional phase.
     *
     * Mainly for debugging reasons (?).
     * We sort the critical cliques by their size and then we sort the contents of each critical clique.
     */
    for(auto & critical_clique : critical_clique_decomposition) {
        sort(critical_clique.begin(), critical_clique.end());
    }
    sort(critical_clique_decomposition.begin(), critical_clique_decomposition.end(),
        [](const std::vector<int> & a, const std::vector<int> & b){
            if(a.size()==b.size())return a<b;
            return a.size()<b.size();
        });

    return critical_clique_decomposition;
}

/*!
 * For debug only.
 *
 * Test that the given critical_clique_decomposition is indeed a critical clique decomposition in the graph G.
 *
 * The critical clique decomposition are the classes of the equivalence relation 'having the same closed neighborhood'.
 * We naively check this property.
 */
void assert_critical_clique_decomposition(const Graph & G, const vector<vector<int>> & critical_clique_decomposition) {
    int n = G.get_vertices_count();

    /**
     * Check if the decomposition contains all vertices and all critical cliques are disjoint.
     */
    vector<int> vertex_seen(n, 0);
    for(const auto cc : critical_clique_decomposition) {
        for(int v : cc) {
            assert(vertex_seen[v] == 0);
            vertex_seen[v] = 1;
        }
    }
    for(int v = 0; v < n; ++v) {
        assert(vertex_seen[v] == 1);
    }

    /**
     * For each pair of critical cliques we check that their vertices do not have the same closed neighborhoods.
     */
    for(int i = 0; i < (int)critical_clique_decomposition.size(); ++i) {
        for(int j = i + 1; j < (int)critical_clique_decomposition.size(); ++j) {
            const auto & cc_i = critical_clique_decomposition[i];
            const auto & cc_j = critical_clique_decomposition[j];
            for(int u : cc_i) {
                vector<int> closed_adj_u(G.get_adjacency_list(u).begin(), G.get_adjacency_list(u).end());
                closed_adj_u.push_back(u);
                sort(closed_adj_u.begin(), closed_adj_u.end());

                for (int v : cc_j) {
                    vector<int> closed_adj_v(G.get_adjacency_list(v).begin(), G.get_adjacency_list(v).end());
                    closed_adj_v.push_back(v);
                    sort(closed_adj_v.begin(), closed_adj_v.end());

                    assert(closed_adj_u != closed_adj_v);
                }
            }
        }
    }

    /**
     * For each critical clique check if the vertices inside it have the same neighborhood.
     */
    for(const auto cc : critical_clique_decomposition) {
        for(int i = 0; i < (int)cc.size(); ++i) {
            int u = cc[i];

            vector<int> closed_adj_u(G.get_adjacency_list(u).begin(), G.get_adjacency_list(u).end());
            closed_adj_u.push_back(u);
            sort(closed_adj_u.begin(), closed_adj_u.end());

            for(int j = i + 1; j < (int)cc.size(); ++j) {
                int v = cc[j];

                vector<int> closed_adj_v(G.get_adjacency_list(v).begin(), G.get_adjacency_list(v).end());
                closed_adj_v.push_back(v);
                sort(closed_adj_v.begin(), closed_adj_v.end());

                assert(closed_adj_u == closed_adj_v);
            }
        }
    }
}

