#pragma once

#include "graph.hpp"
#include "solver.hpp"

class ComponentDivider
{
public:
    /*!
    * Finds components of a graph and runs given solver on all of them. Then combines their solution into one.
    */
    static Solution * runOnComponents(const Graph & graph, Solver<Graph> & solver);
};
