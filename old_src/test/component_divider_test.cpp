#include "../component_divider.hpp"
#include <cassert>
#include <iostream>

class MockSolver : public Solver<Graph>{
    virtual Solution * solve ( Graph * g ){
        Solution * result = new Solution();
        for(int i = 0; i < (int)g->get_vertices_count(); ++i){
            for(int j = 0; j < i; ++j){
                if(!g->contains_edge(i, j)){
                    result->add_edge(i, j);
                }
            }
        }
        return result;
    }
};



int main(void){
    MockSolver mockSolver;

    Graph k3(3,0);
    k3.add_edge(0,1);
    k3.add_edge(1,2);
    k3.add_edge(0,2);
    Solution * sk3 = ComponentDivider::runOnComponents(k3, mockSolver);
    assert(sk3->size() == 0);

    Graph gempty(4,0);
    Solution * sempty = ComponentDivider::runOnComponents(gempty, mockSolver);
    assert(sempty->size() == 0);

    Graph cherry(3, 0);
    cherry.add_edge(0,1);
    cherry.add_edge(0,2);
    Solution * scherry = ComponentDivider::runOnComponents(cherry, mockSolver);
    assert(scherry->size() == 1);
    assert(scherry->isInSolution(1, 2));

    Graph doublecherry(6, 0);
    doublecherry.add_edge(0,1);
    doublecherry.add_edge(0,2);
    doublecherry.add_edge(3,4);
    doublecherry.add_edge(3,5);
    Solution * sdoublecherry = ComponentDivider::runOnComponents(doublecherry, mockSolver);
    assert(sdoublecherry->size() == 2);
    assert(sdoublecherry->isInSolution(1, 2));
    assert(sdoublecherry->isInSolution(4, 5));

    Graph doublec4andCherry(11, 0);
    doublec4andCherry.add_edge(0,1);
    doublec4andCherry.add_edge(1,2);
    doublec4andCherry.add_edge(2,3);
    doublec4andCherry.add_edge(3,0);
    doublec4andCherry.add_edge(4,5);
    doublec4andCherry.add_edge(5,6);
    doublec4andCherry.add_edge(6,7);
    doublec4andCherry.add_edge(7,4);
    doublec4andCherry.add_edge(8,9);
    doublec4andCherry.add_edge(8,10);
    Solution * sdoublec4andCherry = ComponentDivider::runOnComponents(doublec4andCherry, mockSolver);
    assert(sdoublec4andCherry->size() == 5);
    assert(sdoublec4andCherry->isInSolution(0, 2));
    assert(sdoublec4andCherry->isInSolution(1, 3));
    assert(sdoublec4andCherry->isInSolution(4, 6));
    assert(sdoublec4andCherry->isInSolution(5, 7));
    assert(sdoublec4andCherry->isInSolution(9, 10));

    return 0;

}

