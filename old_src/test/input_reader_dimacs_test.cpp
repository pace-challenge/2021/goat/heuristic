#include "../io/input_reader.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <cassert>

using std::string;
using std::istringstream;

int main ( void ) {
    std::cout << "Testing DIMACS format reader...";
    const string str1(
        "c This file describes a path with five vertices and four edges.\n"
        "p cep 5 4\n"
        "1 2\n"
        "2 3\n"
        "c we are half-way done with the instance definition.\n"
        "3 4\n"
        "4 5\n"
    );
    istringstream iss1( str1 );

    Graph * G = InputReader::read( iss1 );

    assert( G->get_vertices_count() == 5 );
    assert( G->get_edges_count() == 4 );
    // we index vertices from 0, not from 1
    assert( G->contains_edge( 0, 1 ) );
    assert( G->contains_edge( 1, 2 ) );
    assert( G->contains_edge( 2, 3 ) );
    assert( G->contains_edge( 3, 4 ) );

    delete G;
    std::cout << "\033[32mOK\033[0m" << std::endl;
}
