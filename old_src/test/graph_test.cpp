#include "../graph.hpp"
#include <cassert>
#include <iostream>

#define MAX_V 10
#define DEFAULT_E 5

using std::cout, std::endl;

int main ( void ) {
    cout << "Testing graph construction...";
    Graph * G = new Graph( MAX_V, DEFAULT_E );

    assert( G->get_vertices_count() == MAX_V );
    assert( G->get_edges_count() == 0 );

    cout << "\033[32mOK\033[0m" << endl;

    cout << "Testing edges adding...";
    for ( int i = 0; i < MAX_V; ++i ) {
        for ( int j = i + 1; j < MAX_V; ++ j ) {
            G->add_edge( i, j );
            assert( G->contains_edge( j, i ) );
            assert( G->contains_edge( i, j ) );
        }
    }
    cout << "\033[32mOK\033[0m" << endl;

    cout << "Testing edges removal...";
    for ( int i = 0; i < MAX_V; ++i ) {
        for ( int j = i + 1; j < MAX_V; ++ j ) {
            G->remove_edge( i, j );
            assert( ! G->contains_edge( j, i ) );
            assert( ! G->contains_edge( i, j ) );
        }
    }
    cout << "\033[32mOK\033[0m" << endl;

    delete G;
    return 0;
}
