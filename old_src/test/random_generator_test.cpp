#include "../random_generator.hpp"

#include <cassert>


int main(void){
    RandomGenerator generator;
    Graph graph1 = generator.generate_graph(10, 0, 1);
    assert(graph1.get_edges_count() == 10*9/2);

    Graph graph2 = generator.generate_graph(10, 20, 10);
    assert(graph2.get_edges_count() == 20);

    Graph graph3 = generator.generate_graph(10, 20, 1);
    assert(graph3.get_edges_count() == 10*9/2 - 20);

    Graph graph4 = generator.generate_graph(120, 120*119/2, 1);
    assert(graph4.get_edges_count() == 0);

    Graph graph5 = generator.generate_graph(120, 120*119/2, 120);
    assert(graph5.get_edges_count() == 120*119/2);

    return 0;
}
