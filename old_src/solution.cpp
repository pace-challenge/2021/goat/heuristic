#include "solution.hpp"

#include <iostream>
#include <cassert>

using std::swap;
using std::make_pair;
using std::endl;

void Solution::add_edge ( int from, int to ) {
    if(from > to) swap(from, to);
    /* std::cerr << ">> SOL added " << from << ' ' << to << std::endl; */
    auto new_edge = make_pair(from, to);
    assert(!solutionEdges.count(new_edge));
    solutionEdges.insert(new_edge);
}

void Solution::remove_edge ( int from, int to ) {
    if(from > to) swap(from, to);
    /* std::cerr << ">> SOL remove " << from << ' ' << to << std::endl; */
    solutionEdges.erase(make_pair(from, to));
}
bool Solution::contains_edge ( int from, int to ){
    if(from > to) swap(from, to);
    return solutionEdges.count(make_pair(from, to));
}

int optional_map ( int value, const map<int,int> & mapping ) {
    auto it = mapping.find( value );
    return ( it != mapping.end() ) ? it->second : value;
}

void Solution::map_vertices ( const map<int,int> & mapping ) {
    set<pair<int,int>> newSolutionEdges;
    for ( const pair<int,int> & p : solutionEdges ) {
        int from = optional_map( p.first, mapping );
        int to = optional_map( p.second, mapping );
        auto mapped_edge = make_pair ( from, to );
        newSolutionEdges.insert ( mapped_edge );
    }
    solutionEdges = newSolutionEdges;
}

void Solution::map_vertices ( const vector<int> & mapping ) {
    set<pair<int,int>> newSolutionEdges;
    for ( const pair<int,int> & p : solutionEdges ) {
        int from = mapping[p.first];
        int to = mapping[p.second];
        auto mapped_edge = make_pair ( from, to );
        newSolutionEdges.insert ( mapped_edge );
    }
    solutionEdges = newSolutionEdges;
}

void Solution::merge(const Solution & other){
    for(const pair<int, int> & p : other.solutionEdges){
        solutionEdges.insert(p);
    }
}

size_t Solution::size() const{
    return solutionEdges.size();
}

set<pair<int,int>> Solution::getSolution() const{
    return solutionEdges;
}

bool Solution::isInSolution( int from, int to ) const{
    if(from > to) swap(from, to);
    return solutionEdges.count(make_pair(from, to));
}

ostream & operator<< ( ostream & os, const Solution & s ) {
    os << s.solutionEdges.size() << endl;
    for ( const pair<int,int> & p : s.solutionEdges ) {
        os << p.first << ' ' << p.second << endl;
    }
    return os;
}
