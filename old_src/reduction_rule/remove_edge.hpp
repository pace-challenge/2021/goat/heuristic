#pragma once

#include "../reduction_rule.hpp"

/*!
 * Removes an edge in an attempt to find the solution.
 */
class RemoveEdgeReduction : public ReductionRule<Graph> {
	public:

		RemoveEdgeReduction ( int from, int to );

		void apply ( Graph & graph ) ;

		void revert ( Graph & graph, Solution * solution ) const ;

	private:
		int from, to; /*!< The edded edge endpoints */
};

