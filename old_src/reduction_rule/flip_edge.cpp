#include "flip_edge.hpp"

FlipEdgeReduction::FlipEdgeReduction ( int from, int to )
	: from ( from ), to ( to ) { }

void FlipEdgeReduction::apply ( WeightedGraph & graph ) {
	graph.flip_edge( from, to );
}

void FlipEdgeReduction::revert ( WeightedGraph & graph, Solution * solution ) const {
	graph.flip_edge( from, to );
	if ( solution ) solution->add_edge( from, to );
}
