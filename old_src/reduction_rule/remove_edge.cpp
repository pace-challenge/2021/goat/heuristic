#include "remove_edge.hpp"

RemoveEdgeReduction::RemoveEdgeReduction ( int from, int to )
	: from ( from ), to ( to ) { }

void RemoveEdgeReduction::apply ( Graph & graph ) {
	graph.remove_edge( from, to );
}

void RemoveEdgeReduction::revert ( Graph & graph, Solution * solution ) const {
	graph.add_edge( from, to );
	if ( solution ) solution->add_edge( from, to );
}
