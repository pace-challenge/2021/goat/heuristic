#include "merge.hpp"
#include <cassert>

MergeReduction::MergeReduction ( int what, int into ) : what ( what ), into ( into ) { }

void MergeReduction::apply ( WeightedGraph & graph ) {
    assert(graph.contains_edge(what, into));
    copy = graph.copy();
    /* graph.print(std::cerr); */
    graph.merge( what, into );
    /* graph.print(std::cerr); */
}

void MergeReduction::revert ( WeightedGraph & graph, Solution * solution ) const {
    if ( solution ){
        /* std::cerr << "revert " << what << ' ' << into << std::endl; */
        // find what edges are incident to merged-into vertex
        for(int v : graph.all_vertices()){
            if(v != into){
                int whatSign = copy->get_edge_sign(v, what);
                int intoSign = copy->get_edge_sign(v, into);
                if(whatSign == intoSign || whatSign == 0 || intoSign == 0){
                    // the edges are in the same state
                    // so either both are in the solution, or both are not
                    if(solution->contains_edge(v, into)){
                        solution->add_edge(v, what);
                    }
                }else{
                    // edges are in different states
                    // so exactly one will be in the solution depending on their state
                    bool smallerInto = copy->get_edge_weight(v, into) < copy->get_edge_weight(v, what);
                    bool solutionInto = solution->contains_edge(v, into);
                    if(solutionInto) solution->remove_edge(v, into); // standardize

                    bool finalSolutionIsInto = solutionInto != smallerInto;
                    solution->add_edge(v, finalSolutionIsInto ? into : what);
                }
            }
        }
    }
    graph = *copy;
}
