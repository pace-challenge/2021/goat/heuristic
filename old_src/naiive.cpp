#include "graph.hpp"
#include "io/input_reader.hpp"
#include "solution.hpp"
#include "solver/simple.hpp"
#include "solver/unknownk.hpp"

#include <iostream>
#include <vector>

int main ( void ) {
    InputReader in;
    vector<Solver<Graph>*> solvers;

    solvers.push_back( new UnknownKSolverWrapper<Graph>([](int k) {return new SimpleSolver(k); }));

    Graph * graph = in.read( std::cin );
    for ( Solver<Graph> *solver : solvers ) {
        Solution * solution = solver->solve( graph );
        std::cout << (*solution);
        delete solution;
    }
    delete graph;

    return 0;
}
