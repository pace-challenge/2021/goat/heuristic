#include "io/input_reader.hpp"
#include "critical_cliques.hpp"

int main ( void ) {
    InputReader in;

    Graph g = *in.read( std::cin );

    auto critical_clique_decomposition = get_critical_clique_decomposition(g);

    // assert_critical_clique_decomposition(g, critical_clique_decomposition);

    std::map<int, int> cc_size_to_count;

    for(const auto & cc : critical_clique_decomposition) {
        cc_size_to_count[cc.size()] += 1;
    }

    for(auto p : cc_size_to_count) {
        std::cout<<p.first<<' '<<p.second<<std::endl;
    }

}

