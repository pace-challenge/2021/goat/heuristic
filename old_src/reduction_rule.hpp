#pragma once

#include "graph.hpp"
#include "solution.hpp"

/*!
 * Pure virtual class representing single reduction rule.
 */
template < class GRAPH_TYPE >
class ReductionRule {
    public:

        /*!
         * Applies reduction rule to an instance.
         *
         * \param[in,out] graph Instance to be modified.
         */
        virtual void apply ( GRAPH_TYPE & graph ) = 0;

        /*!
         * Reverts application of this reduction rule.
         *
         * \param[in,out] graph Instance to be modified.
         * \param[in,out] solution Instance solution.
         */
        virtual void revert ( GRAPH_TYPE & graph, Solution * solution ) const = 0;

};
