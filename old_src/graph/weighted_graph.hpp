#pragma once

#include <vector>
#include <iostream>
using std::vector;
using std::ostream;

/*!
 * Graph representation with weighted edges.
 *
 * All edges are always 'present', however actual edges have always positive weight -- this is considered when searching for neighbors, counting degree, etc. Changes to this graph DOESN'T change indices of represented vertices -- these always stay valid, unless you delete or merge them.
 */
class WeightedGraph {

public:
    virtual ~WeightedGraph(){};

    virtual bool contains_edge(int u, int v) const = 0;

    virtual void flip_edge(int u, int v) = 0;

    virtual int get_edge_weight(int u, int v) const = 0;

    virtual void set_edge_weight(int u, int v, int val) = 0;

    virtual int get_edge_sign(int u, int v) const = 0;

    virtual vector<int> all_vertices() const = 0;

    virtual int size() const = 0;

    virtual int add_vertex() = 0;

    virtual void delete_vertex(int v) = 0;

    virtual vector<int> neighbors(int u) const = 0;

    virtual int degree(int u) const = 0;

    /*!
     * Merges 'what' vertex into 'into' vertex.
     * Removes what vertex.
     */
    virtual void merge(int what, int into) = 0;

    virtual WeightedGraph* copy() const = 0;

    virtual void print(ostream &os) const = 0;
};
