#pragma once

#include <vector>
#include <iostream>
#include <cmath>
#include <cassert>
using std::vector;
using std::ostream;

const int FORBIDDEN = -(1<<26);

struct RadovanGraph {

    int n;
    int n_valid;
    vector<bool> vertices_valid;
    // we will store it as the lower triangle half of the full matrix
    // e.g for a graph with n=4, we will store only * entries
    // - - - -
    // * - - -
    // * * - -
    // * * * -
    // we will keep the first entry of weighted_adj_matrix as empty vector for indexing convenience
    // therefore the weight of the edge {u, v} is the entry weighted_adj_matrix[v][u]
    vector<vector<int>> weighted_adj_matrix;

    RadovanGraph();

    RadovanGraph(int n, const vector<vector<int>> & weighted_adj_matrix);

    int get_edge_weight(int u, int v) const;

    void set_edge_weight(int u, int v, int s);

    vector<int> get_valid_vertices() const;

    bool contains_edge(int u, int v) const;

    void flip_edge(int u, int v);

    vector<int> neighbors(int v) const;

    void delete_vertex(int v);

    RadovanGraph get_induced_subgraph(const vector<int> & vertices) const;

    /**
     * Modifying implementation of merge operation.
     *
     * Returns cost of the operation.
     *
     * For simplicity, we allow merging of only true edges (s(u,v) = positive weight).
     */
    int merge(int u, int v);

    /**
     * Computes the lower bound cost for setting edge uv to forbidden
     */
    int induced_forbidden_cost(int u, int v) const;

    /**
     * Computes the lower bound cost for setting edge uv to permanent
     */
    int induced_permanent_cost(int u, int v) const;

    /**
     * Creates new weighted graph with only valid vertices.
     */
    RadovanGraph consolidate() const;

    void print(ostream & out) const;
};
