#pragma once

#include "weighted_graph.hpp"

#include <iostream>
#include <map>
#include <set>
#include <vector>
using std::map;
using std::ostream;
using std::set;
using std::vector;
using std::pair;

const int FORBIDDEN_WEIGHT = -(1<<26);

/*!
 * Really dumb weighted graph implementation.
 *
 * Uses set for vertices, and map<lower,higher> for edge weights. All operations should be quite trivial to implement, but slow.
 */
class DumbWeightedGraph : public WeightedGraph {

public:

    DumbWeightedGraph();

    DumbWeightedGraph(int N=0);

    DumbWeightedGraph(const vector<int> &vertices, const map<pair<int,int>,int> &edges, int mx);

    DumbWeightedGraph(const DumbWeightedGraph &db);

    virtual bool contains_edge(int u, int v) const;

    virtual void flip_edge(int u, int v);

    virtual int get_edge_weight(int u, int v) const;

    virtual void set_edge_weight(int u, int v, int val);

    virtual int get_edge_sign(int u, int v) const;

    virtual vector<int> all_vertices() const;

    virtual int size() const;

    virtual int add_vertex();

    virtual void delete_vertex(int v);

    virtual vector<int> neighbors(int u) const;

    virtual int degree(int u) const;

    virtual void merge(int what, int into);

    virtual WeightedGraph* copy() const;

    virtual void print(ostream &os) const;

private:

    set<int> vertices;
    map<pair<int,int>,int> edges;
    int mx_vertex_id;
};
