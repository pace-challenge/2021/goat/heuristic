#pragma once

#include "graph.hpp"

#include <algorithm>

/*!
 * Compute the critical clique decomposition in O(n+mlogn)
 */
vector<vector<int>> get_critical_clique_decomposition(const Graph & G);

/*!
 * For debug only.
 *
 * Test that the given critical_clique_decomposition is indeed a critical clique decomposition in the graph G.
 *
 * The critical clique decomposition are the classes of the equivalence relation 'having the same closed neighborhood'.
 * We naively check this property.
 */
void assert_critical_clique_decomposition(const Graph & G, const vector<vector<int>> & critical_clique_decomposition);

