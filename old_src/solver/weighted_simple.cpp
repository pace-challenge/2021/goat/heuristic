#include "weighted_simple.hpp"
#include "../reduction_rule.hpp"
#include "../reduction_rule/flip_edge.hpp"

#include <string>

using std::string;

WeightedSimpleSolver::WeightedSimpleSolver ( int K ) : K ( K ), depth ( 0 ) { }

Solution * WeightedSimpleSolver::flip_edge ( WeightedGraph * g, int i, int j ) {
    depth += 1;
    FlipEdgeReduction flip(i, j);
    flip.apply(*g);
    Solution *s = solve(g);
    flip.revert(*g, s);
    depth -= 1;
    return s;
}

Solution * WeightedSimpleSolver::solve ( WeightedGraph * g ) {
    int N = g->size();
    Solution * s = nullptr;
    for ( int i = 0; i < N; ++i ) {
        // from i+1 because vertices i and j of a cherry are symmetric
        for ( int j = i+1; j < N; ++j ) {
            if ( g->contains_edge( i, j ) ) continue;
            for ( int k = 0; k < N; ++k ) {
                if ( i==k || j==k ) continue;
                if ( ! g->contains_edge ( i, k ) ) continue;
                if ( ! g->contains_edge ( j, k ) ) continue;
                // found a cherry -- an induced path on three vertices
                /* std::cerr << string(depth, ' ') << "found a cherry " << i << ' ' << j << ' ' << k << std::endl; */
                if(depth < K){
                    if( (s = flip_edge( g, i, j )) ) return s;
                    if( (s = flip_edge( g, i, k )) ) return s;
                    if( (s = flip_edge( g, j, k )) ) return s;
                }
                return nullptr;
            }
        }
    }
    /* std::cerr << string(depth, ' ') << "is solved!\n"; */
    return new Solution();
}
