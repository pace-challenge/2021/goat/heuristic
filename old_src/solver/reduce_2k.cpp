#include "reduce_2k.hpp"
#include "weighted_simple.hpp"
#include "../reduction_rule/reduce_2k.hpp"
#include "../reduction_rule/flip_edge.hpp"
#include "../reduction_rule/merge.hpp"
#include "unknownk.hpp"

#include <string>

using std::string;

Reduce2kSolver::Reduce2kSolver ( ) { }

Solution * Reduce2kSolver::solve ( WeightedGraph * g ) {
    pair<int,int> p = find_2k_reducible_vertex(g);
    if(p.first == 0){
        std::cerr << "finished!\n";
        return new Solution(); // no cherry -> we have a solution
    }
    if(p.first == -1){ // nothing can be reduced -- call another solution to finish this
        std::cerr << "unreducible!\n";
        return UnknownKSolverWrapper<WeightedGraph>([](int k){return new WeightedSimpleSolver(k);}).solve(g);
    }
    TwokReduction reduction(p.second);
    reduction.apply(*g);
    Solution *s = solve(g);
    reduction.revert(*g, s);
    return s;
}

