#include "simple.hpp"
#include "../io/input_reader.hpp"
#include "../reduction_rule.hpp"
#include "../reduction_rule/add_edge.hpp"
#include "../reduction_rule/remove_edge.hpp"

#include <string>

using std::string;

SimpleSolver::SimpleSolver ( int K ) : K ( K ), depth ( 0 ) { }

Solution * SimpleSolver::flip_edge ( Graph * g, int i, int j, bool add_edge ) {
    /* std::cerr << string(depth, ' ') << "apply  : " << (add_edge?"add":"rem") << ' ' << i << ' ' << j << std::endl; */
    depth += 1;
    Solution * s;
    if ( add_edge ){
        AddEdgeReduction er( i, j );
        er.apply( *g );
        s = solve( g );
        er.revert( *g, s );
    }else{
        RemoveEdgeReduction er( i, j );
        er.apply( *g );
        s = solve( g );
        er.revert( *g, s );
    }
    depth -= 1;
    /* std::cerr << string(depth, ' ') << "revert : " << (add_edge?"add":"rem") << ' ' << i << ' ' << j << std::endl; */
    return s;
}

Solution * SimpleSolver::solve ( Graph * g ) {
    int N = g->get_vertices_count();
    Solution * s = nullptr;
    for ( int i = 0; i < N; ++i ) {
        // from i+1 because vertices i and j of a cherry are symmetric
        for ( int j = i+1; j < N; ++j ) {
            if ( g->contains_edge( i, j ) ) continue;
            for ( int k = 0; k < N; ++k ) {
                if ( i==k || j==k ) continue;
                if ( ! g->contains_edge ( i, k ) ) continue;
                if ( ! g->contains_edge ( j, k ) ) continue;
                // found a cherry -- an induced path on three vertices
                /* std::cerr << string(depth, ' ') << "found a cherry " << i << ' ' << j << ' ' << k << std::endl; */
                if(depth < K){
                    if( (s = flip_edge( g, i, j, true  )) ) return s;
                    if( (s = flip_edge( g, i, k, false )) ) return s;
                    if( (s = flip_edge( g, j, k, false )) ) return s;
                }
                return nullptr;
            }
        }
    }
    /* std::cerr << string(depth, ' ') << "is solved!\n"; */
    return new Solution();
}
