#pragma once

#include "../graph.hpp"
#include "../solution.hpp"
#include "../solver.hpp"

#include <iomanip>
#include <functional>

using std::function;

/*!
 * Uses other solver which finds solution based on wanted solution size (k).
 */
template < class GRAPH_TYPE >
class UnknownKSolverWrapper : public Solver<GRAPH_TYPE>{
	public:

        /*!
		 * Constructor.
		 *
		 * \param[in] solverFactory Function that creates solver for given k.
        */
		UnknownKSolverWrapper ( const function<Solver<GRAPH_TYPE>*(int)> & solverFactory); 

        /*!
        * Tries to find solution by iterating number of edge modification using solvers from factory. 
        */
		virtual Solution * solve ( GRAPH_TYPE * g );

	private:
        function<Solver<GRAPH_TYPE>*(int)> factory;
        int nextk;
};

template < class GRAPH_TYPE >
UnknownKSolverWrapper<GRAPH_TYPE>::UnknownKSolverWrapper ( const function<Solver<GRAPH_TYPE>*(int)> & solverFactory): factory(solverFactory), nextk(0){}

template < class GRAPH_TYPE >
Solution * UnknownKSolverWrapper<GRAPH_TYPE>::solve ( GRAPH_TYPE * g ){
    Solution * solution = nullptr;
    while(solution == nullptr){
        Solver<GRAPH_TYPE> * solver = factory(nextk);
        solution = solver->solve(g);
        std::cerr << nextk << std::endl;
        ++nextk;
    }
    return solution;
}
