#include "graph/weighted_graph.hpp"
#include "graph/dumb_weighted_graph.hpp"
#include "io/input_reader.hpp"
#include "solution.hpp"
#include "graph/conversion.hpp"
#include "solver/reduce_2k.hpp"
#include "solver/weighted_simple.hpp"
#include "solver/unknownk.hpp"

#include <iostream>
#include <vector>

int main ( ) {
    InputReader in;
    Reduce2kSolver solver;

    auto graphFactory = [](int K){ return new DumbWeightedGraph(K); };
    Graph *raw_graph = in.read( std::cin );
    WeightedGraph *graph = convert_graph_to_weighted_graph(*raw_graph, graphFactory);
    /* graph->print(std::cerr); */
    delete raw_graph;

    Solution * solution = solver.solve( graph );
    if(solution) std::cout << (*solution);
    delete solution;

    return 0;
}
