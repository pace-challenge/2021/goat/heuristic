#pragma once

#include <tuple>

#include "graph.hpp"

int calculate_edge_connectivity(Graph graph);

std::tuple<int, int, int> calculate_min_max_median_degree(Graph graph);