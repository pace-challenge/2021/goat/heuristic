#!/bin/bash

output_file='./analysis/metrics.csv'

echo '"index","vertices","edges","min degree","max degree","median degree","avg degree","connectivity"' >$output_file 

for i in ./instances/public/exact*.gr
do
    echo $i
    printf "$i" | grep -o '[1-9][0-9]*' | tr '\n' ',' >>$output_file
    ./exe/generate_metrics <$i >>$output_file
done

exit 0