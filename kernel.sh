#!/usr/bin/env bash

process_id=''

function cleanup(){
    echo "Hey, you pressed Ctrl-C.  Time to quit."
    if [ "$process_id" != '' ]; then
        kill -SIGKILL "$process_id" 2>/dev/null
    fi
    rm -f "$TMP_FILE"
    exit 1
}

trap cleanup INT # handles interrupt of this script

FILE=""
make -f Makefile-kernel -j4 exe_kernel/kernel/judge exe_kernel/main_kernel || exit 1

TMP_RES=".kernel_tmp.txt"
FINAL_RES=".kernel_best.txt"

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
DEFAULT="\033[0m"

# How long should the time to SIGTERM be and how long then to wait before killing it
max_time=300 # 30s here, 5m on server
wait_time=300 # 30s

CNT=1
TOTAL_DIF=0
TMP_FILE="$(mktemp)"

rm -f "$TMP_RES"

for FILE in instances/public/*; do
    echo -n "Running $CNT $FILE:"
    start_time=0

    #running main_kernel start
    ./exe_kernel/main_kernel <"$FILE" >"$TMP_FILE" &
    process_id=$!
    while [ $start_time -lt $max_time ]; do
        if ! kill -0 $process_id 2>/dev/null; then break; fi
        sleep 0.1
        start_time=$((start_time+1))
    done
    kill -SIGTERM $process_id 2>/dev/null

    start_time=0
    while [ $start_time -lt $wait_time ]; do
        if ! kill -0 $process_id 2>/dev/null; then break; fi
        sleep 0.1
        start_time=$((start_time+1))
    done
    kill -SIGKILL $process_id 2>/dev/null

    ./exe_kernel/kernel/judge "$FILE" "$TMP_FILE" >> "$TMP_RES"
    #running main_kernel end

    RET="$?"
    RES="$(tail -1 "$TMP_RES")"
    BEST=""
    if [[ -f "$FINAL_RES" ]]; then
        BEST="$(sed -n "$CNT"p "$FINAL_RES")"
    fi
    CNT="$((CNT + 1))"
    DIF=''

    if [[ $RES == ?(-)+([[:digit:]]) ]] && [[ $BEST == ?(-)+([[:digit:]]) ]]; then
        DIF="$((RES - BEST))"
        TOTAL_DIF="$((TOTAL_DIF + DIF))"
    fi
    echo -n " ret: $RET"
    echo -n " res: $RES"
    if [[ "$BEST" != '' ]]; then echo -n " best: $BEST"; fi
    if [[ "$DIF" != '' ]]; then
        if [[ $DIF -gt 0 ]]; then echo -en "$RED"; fi
        if [[ $DIF -lt 0 ]]; then echo -en "$GREEN"; fi
        if [[ $DIF -eq 0 ]]; then echo -en "$YELLOW"; fi
        echo -n " dif: $DIF"
        echo -en "$DEFAULT"
    fi
    echo ''
	if [[ $# -ge 2 ]] && [[ $1 == '-v' ]]; then
		python3 visualise.py "tmp.txt" "$FILE.html"
	fi
done

echo "to make this measurement the best we know, run:"
echo "cp \"$TMP_RES\" \"$FINAL_RES\""

rm -f "$TMP_FILE"
