
* We collect ideas and notes in [notes.md](notes.md).
* Papers are stored as pdfs in `docs/` folder.

### Codestyle

* **Codestyle** is not well decided yet but we should follow one -- for now it is the initial codestyle of Šimon who initialized the project, see `graph.{cpp,hpp}` for an example.
* No codestyle linter nor general editor config has been set up yet (**todo**).
* **Indentation** with 4-spaces.
* **Documentation** All code should be reasonably documented.

