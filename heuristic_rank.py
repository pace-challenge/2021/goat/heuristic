#!/usr/bin/python3

optil_scores = []
with open('.heuristic.best_optil.txt', 'r') as f:
    for l in f:
        optil_scores.append(float(l))

our_best_scores = []
with open('.heuristic.best.txt', 'r') as f:
    for l in f:
        our_best_scores.append(float(l))

assert len(our_best_scores) == len(optil_scores)

for idx, (optil, our_best) in sorted(enumerate(zip(optil_scores, our_best_scores)), key=lambda x: x[1][1] / x[1][0]):
    print('{: 4} : heur{:03}.gr => {:.2f}% worse {}/{}'.format(idx + 1, (idx + 1)*2 - 1, 100 * (our_best / optil - 1), our_best, optil))



rank = 100 * sum(optil / our_best for optil, our_best in zip(optil_scores, our_best_scores)) / len(our_best_scores)

print('final rank:', rank)
