from pyvis.network import Network
from random import randint
import sys

if len( sys.argv ) != 3:
    print( "Usage: ", argv[ 0 ], " [input_file] [output_file]" );

out_file = open( sys.argv[ 1 ], 'r' )
lines    = out_file.readlines() 
net      = Network( "950px", "950px" )
E        = 0

count = 0;
for line in lines:
    if count == 0:
        d = int(line)
    elif count == 1:
        p, cep, N, E = line.split()
        E = int(E)
        for v in range(1,int(N)+1):
            net.add_node( v, size=2 )
    elif count < E:
        u, v = [int(x) for x in line.split()]
        net.add_edge( u, v )

    count += 1

net.toggle_physics( False )
net.show( sys.argv[ 2 ] )
