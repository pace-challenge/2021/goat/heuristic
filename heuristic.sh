#!/usr/bin/env bash

make -j4 exe/main_heuristic || exit 1

TMP_RES=".heuristic.tmp.txt"
BEST_RES=".heuristic.best.txt"
BEST_OPTIL_RES=".heuristic.best_optil.txt"

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
DEFAULT="\033[0m"

CNT=0
TOTAL_DIF=0
TMP_FILE="$(mktemp)"

rm -f "$TMP_RES"

for FILE in heur_instances/heur/*; do
    CNT="$((CNT + 1))"
    if [[ $CNT -lt 0 ]]; then continue; fi

    echo -n "Running $CNT $FILE:"
    start_time=0


    #running main_heuristic start
    ./exe/main_heuristic <"$FILE" >"$TMP_FILE"
    RET="$?"
    echo -n " ret: $RET"

    RES="$(head -1 "$TMP_FILE")"
    echo -n " res: $RES"
    echo $RES >> $TMP_RES

    echo -n " || "

    BEST=""
    if [[ -f "$BEST_RES" ]]; then
        BEST="$(sed -n "$CNT"p "$BEST_RES")"
    fi
    DIF=''
    if [[ $RES == ?(-)+([[:digit:]]) ]] && [[ $BEST == ?(-)+([[:digit:]]) ]]; then
        DIF="$((RES - BEST))"
        TOTAL_DIF="$((TOTAL_DIF + DIF))"
    fi
    if [[ "$BEST" != '' ]]; then echo -n " best: $BEST"; fi
    if [[ "$DIF" != '' ]]; then
        if [[ $DIF -gt 0 ]]; then echo -en "$RED"; fi
        if [[ $DIF -lt 0 ]]; then echo -en "$GREEN"; fi
        if [[ $DIF -eq 0 ]]; then echo -en "$YELLOW"; fi
        echo -n " dif: $DIF"
        echo -en "$DEFAULT"
    fi

    echo -n " || "

    BEST_OPTIL=""
    if [[ -f "$BEST_OPTIL_RES" ]]; then
        BEST_OPTIL="$(sed -n "$CNT"p "$BEST_OPTIL_RES")"
    fi
    DIF_OPTIL=''
    if [[ $RES == ?(-)+([[:digit:]]) ]] && [[ $BEST_OPTIL == ?(-)+([[:digit:]]) ]]; then
        DIF_OPTIL="$((RES - BEST_OPTIL))"
        TOTAL_DIF="$((TOTAL_DIF + DIF_OPTIL))"
    fi
    if [[ "$BEST_OPTIL" != '' ]]; then echo -n " best_optil: $BEST_OPTIL"; fi
    if [[ "$DIF_OPTIL" != '' ]]; then
        if [[ $DIF_OPTIL -gt 0 ]]; then echo -en "$RED"; fi
        if [[ $DIF_OPTIL -lt 0 ]]; then echo -en "$GREEN"; fi
        if [[ $DIF_OPTIL -eq 0 ]]; then echo -en "$YELLOW"; fi
        echo -n " dif_optil: $DIF_OPTIL"
        echo -en "$DEFAULT"
    fi

    echo ''
done

echo "to make this measurement the best we know, run:"
echo "cp \"$TMP_RES\" \"$BEST_RES\""

rm -f "$TMP_FILE"
