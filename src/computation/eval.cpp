#include "eval.hpp"
#include <cassert>

bool should_be_evaluated(EvalState state){ return state == NEW || state == LATER_AGAIN; }

bool is_resolved(EvalState state){ return state == SUCCESS || state == FAIL || state == SKIPPED; }

EvalResult eval_success(CliqueSolution *solution){ return {SUCCESS, solution, {}}; }

EvalResult eval_fail(){ return {FAIL, nullptr, {}}; }

EvalResult eval_expand(vector<Node *> children){
    assert(children.size() != 0);
    return {WAITING, nullptr, children};
}

EvalResult eval_expand(Node * child){ return {WAITING, nullptr, {child}}; }

EvalResult eval_wait(){ return {WAITING, nullptr, {}}; }

EvalResult eval_void(){ return {IGNORE, nullptr, {}}; }

