#include "tree.hpp"

#ifdef tree_log
#include "../utils/timer.hpp"
#endif

#include <cassert>
#include <functional>
#include <map>
#include <vector>

using std::vector;
using std::map;
using std::function;
using std::cerr;
using std::endl;


ComputationTree::ComputationTree(SelectNodeStrategy *select_node_strategy, const int * end_immediatelly_flag)
    :select_node_strategy(select_node_strategy),root(nullptr),original_graph(nullptr), end_immediatelly(end_immediatelly_flag){}

ComputationTree::ComputationTree(SelectNodeStrategy *select_node_strategy): ComputationTree(select_node_strategy, nullptr){}

ComputationTree::~ComputationTree(){
    delete select_node_strategy;
    if(root)delete root;
}

bool ComputationTree::will_be_resolved(EvalResult res, Node *node){
    bool was_not_resolved = !is_resolved(node->state);
    bool is_now_resolved = is_resolved(res.result_state);
    return was_not_resolved && is_now_resolved;
}

bool ComputationTree::write_through_eval(EvalResult res, Node *node){
    if(res.result_state == IGNORE) return false;
    node->remaining_children += res.new_nodes.size();
    for(Node *new_node : res.new_nodes){
#ifdef tree_log
        cerr << "CHILD par " << node << " child " << new_node << endl;
#endif
        node->children.push_back(new_node);
        new_node->parent = node;
        new_node->depth = node->depth + 1;
        select_node_strategy->created_node(new_node);
    }
    bool got_resolved = will_be_resolved(res, node);
    node->state = res.result_state;
    if(res.solution != nullptr){
#ifdef tree_log
        cerr << "SOLUTION node " << node << endl;
#endif
        node->solution = res.solution;
        assert(node->solution->vertex_count() == node->graph->size());
        int sz = node->solution->get_solution(node->graph)->weight();
        if(!(node->original_bound.lower <= sz && sz <= node->original_bound.upper)){
            cerr << "ERROR: Node has solution out of bounds! sz:" << sz << ", bounds:" << node->original_bound << "\n";
#ifdef bt_solution
            cerr << "graph\n";
            cerr << node->graph;
            cerr << "subtree\n";
            string prefix = "";
            backtrack_solution(node, prefix);
            cerr << "solution\n";
            cerr << (*node->solution) << endl;
#endif
            assert(false);
        }
    }
    select_node_strategy->changed_node(node);
    return got_resolved;
}

void backtrack_solution(Node *node, string prefix){
    static string red = "\033[31m";
    static string green = "\033[32m";
    static string clear = "\033[0m";
    string color = (node->solution != nullptr) ? green : red;
    string first_prefix = prefix;
    if(first_prefix.size() >= 2){
        bool back = first_prefix[first_prefix.size()-2] == '|';
        for(int i=0; i<2; ++i) first_prefix.pop_back();
        if(back) first_prefix += "|-";
        else first_prefix += "L-";
    }

    cerr << first_prefix << color;
    cerr << node->get_name() << ", " << node->original_bound << ", " << node->bound;
    if(node->solution){
        int sz = node->solution->get_solution(node->graph)->weight();
        cerr << ", sz:" << sz;
        // cerr << '\n' << (*node->solution);
    }
    cerr << clear << endl;
    const auto &children = node->get_children();
    assert(prefix.size()%2==0);
    for(int i=0; i<(int)children.size(); ++i){
        if(i != ((int)children.size())-1){
            prefix += "| ";
        }else{
            prefix += "  ";
        }
        backtrack_solution(children[i], prefix);
        for(int i=0; i<2; ++i) prefix.pop_back();
    }
}

Solution * ComputationTree::evaluate(Node *root_node){
    original_graph = root_node->graph->copy();
    CliqueSolution * clique_solution = evaluate_clique(root_node);
#ifdef bt_solution
    string prefix = "";
    backtrack_solution(root_node, prefix);
#endif
    if(clique_solution == nullptr) return nullptr;
    Solution *solution = clique_solution->get_solution(original_graph);
    delete original_graph;
    delete clique_solution;
    return solution;
}

CliqueSolution * ComputationTree::evaluate_clique(Node *root_node){
    root = root_node;
#ifdef tree_log
    cerr << "ROOT node " << root << endl;
#endif
    select_node_strategy->created_node(root);
    Node *current_node;
#ifdef tree_log
    Timer timer;
#endif
    while((current_node = select_node_strategy->next_node()) != nullptr) {

        if(end_immediatelly != nullptr && *end_immediatelly == 1){
            return nullptr;
        }

        assert(should_be_evaluated(current_node->state));
        Node *child = nullptr;
        EvalResult res;
        bool got_resolved;
        while(true){ // evaluates path going from the node to its predecessors
            assert(!is_resolved(current_node->state));
            if(child == nullptr){
#ifdef tree_log
                cerr << "EVAL node " << current_node << endl;
                timer.start();
#endif
                res = current_node->evaluate();
#ifdef tree_log
                cerr << "TIME node " << current_node << " eval " << timer.stop() << " ms" << endl;
#endif
                got_resolved = write_through_eval(res, current_node);
            }else{
#ifdef tree_log
                cerr << "EVAL node " << current_node << endl;
                timer.start();
#endif
                res = current_node->child_finished(child);
                // if(child->get_state() != SUCCESS) {delete_children(child);}
#ifdef tree_log
                cerr << "TIME node " << current_node << " eval " << timer.stop() << " ms" << endl;
#endif
                got_resolved = write_through_eval(res, current_node);
                if(!got_resolved && current_node->remaining_children == 0){
                    res = current_node->all_children_finished();
                    got_resolved = write_through_eval(res, current_node);
                }
            }
            if(got_resolved){
                if(current_node->parent == nullptr) break; // root node was resolved!
                // in case we have resolved node we can remove its subtree
                skip_unsolved_subtree(current_node);
#ifndef bt_solution
                delete_children(current_node);
#endif
                // and we need to process its parent
                child = current_node;
                current_node = current_node->parent;
                current_node->remaining_children -= 1;
                continue;
            }
            break;
        }
    }
    return root->solution;
}

void ComputationTree::delete_children(Node *node){
    for(Node *child : node->children){
        select_node_strategy->deleted_node(child);
        delete child;
    }
    node->children.clear();
}

void ComputationTree::skip_unsolved_subtree(Node *node){
    for(Node *child : node->children){
        if(!is_resolved(child->state)){
            skip_unsolved_subtree(child);
            child->state = SKIPPED;
            select_node_strategy->changed_node(child);
        }
    }
}
