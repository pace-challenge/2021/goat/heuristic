#include "computation/eval.hpp"
#include "node/node.hpp"
#include "strategy/dumb_strategy.hpp"
#include "strategy/priority_strategy.hpp"
#include "computation/tree.hpp"
#include "graph/conversion.hpp"
#include "graph/weighted_graph.hpp"
#include "graph/dumb_weighted_graph.hpp"
#include "io/input_reader.hpp"
#include "solution.hpp"
#include "node/cherry.hpp"
#include "node/iter.hpp"

#include "graph/materialized_weighted_graph.hpp"
#include "lower_bounds/greedy_disjoint_conflict_triples.hpp"
#include "upper_bounds/greedy_induced_costs.hpp"

#include <iostream>
#include <vector>
#include <fstream>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;

#define INF (1<<30)


int main (int argc, char *argv[]) {
    assert(argc==3);
    std::cerr<<"input instance path: "<< argv[1]<<std::endl;
    std::cerr<<"solution path: "<<argv[2]<<std::endl;

    std::ifstream input_instance_f(argv[1]);

    std::istream * p_solution_f;
    if(std::string(argv[2]) == "-") {
        p_solution_f = &cin;
    }
    else {
        p_solution_f = new std::ifstream(argv[2]);
    }
    std::istream & solution_f = *p_solution_f;

    InputReader in;
    Graph *g = in.read( input_instance_f );

    for(int v = 0; v < (int)g->get_vertices_count(); ++v) {
        std::cerr<<v<<" =>";
        for(int u : g->get_adjacency_list(v)) std::cerr<<" "<<u;
        std::cerr<<std::endl;
    }

    int sol_cnt;
    solution_f>>sol_cnt;
    std::cerr<<"sol_cnt => "<<sol_cnt<<std::endl;
    for(int i = 0; i < sol_cnt; ++i) {
        int u,v;
        solution_f>>u>>v;
        u--;
        v--;
        std::cerr<<"sol => "<<u<<" "<<v<<std::endl;
        if(u>v)std::swap(u, v);
        g->flip_edge(u, v);
    }

    for(int v = 0; v < (int)g->get_vertices_count(); ++v) {
        std::cerr<<v<<" =>";
        for(int u : g->get_adjacency_list(v)) std::cerr<<" "<<u;
        std::cerr<<std::endl;
    }

    std::map<std::set<int>, std::set<int>> cliques;
    for(int v = 0; v < (int)g->get_vertices_count(); ++v) {
        auto open_nv = g->get_adjacency_list(v);
        std::set<int> closed_nv(open_nv.begin(), open_nv.end());
        closed_nv.insert(v);
        cliques[closed_nv].insert(v);
    }

    bool ok = true;
    for(auto c : cliques) {
        if(c.first != c.second) ok = false;
        for(int x : c.first) std::cerr<<x<<' ';
        std::cerr<<" => ";
        for(int x : c.second) std::cerr<<x<<' ';
        std::cerr<<std::endl;
    }
    if(ok) {
        std::cout<<"ok"<<std::endl;
        std::cout<<"input_instance: "<<argv[1]<<std::endl;
        std::cout<<"n: "<<g->get_vertices_count()<<std::endl;
        std::cout<<"edges: "<<g->get_vertices_count()*(g->get_vertices_count()-1)/2<<std::endl;
        std::cout<<"sol_cnt: "<<sol_cnt<<std::endl;
    }
    else {
        std::cout<<"fail"<<std::endl;
    }

    return 0;
}
