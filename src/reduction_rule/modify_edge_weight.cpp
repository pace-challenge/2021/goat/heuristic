#include "modify_edge_weight.hpp"
#include <assert.h>

ModifyEdgeWeightReduction::ModifyEdgeWeightReduction ( int from, int to, int w )
    : from ( from ), to ( to ), new_weight ( w ), original_weight ( 0 ) {}

void ModifyEdgeWeightReduction::apply ( WeightedGraph *& g, Bound & bound ) {
    original_weight = g->get_edge_weight( from, to );
    assert( original_weight != 0 );
    g->set_edge_weight( from, to, new_weight );
    (void) bound; // eh, todo
}

void ModifyEdgeWeightReduction::reconstruct_solution ( CliqueSolution * sol ) const {
    // TODO
    (void) sol;
}
