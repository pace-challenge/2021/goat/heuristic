#include "almost_clique.hpp"
#include "../utils/min_cut.hpp"


bool is_C_almost_clique(const WeightedGraph & g, const vector<int> & C) {
    vector<int> sup_C;
    for(int v : g.all_vertices()) {
        bool in_C = false;
        for(int u : C) if(u==v) in_C = true;
        if(!in_C)sup_C.push_back(v);
    }

    int ir1 = 0;
    for(int i = 0; i < (int)C.size(); ++i) {
        for(int j = i+1; j < (int)C.size(); ++j) {
            int u = C[i];
            int v = C[j];
            int w = g.get_edge_weight(u, v);
            // std::cerr<<"w: "<<u<<' '<<v<<' '<<w<<std::endl;
            if(w<=0) ir1 += -w;
        }
    }
    // std::cerr<<"ir1: "<<ir1<<std::endl;

    int ir2 = 0;
    for(int u : C) {
        for(int v : sup_C) {
            int w = g.get_edge_weight(u, v);
            if(w>0)ir2 += w;
        }
    }
    // std::cerr<<"ir2: "<<ir2<<std::endl;

    int ub_k_C = -1;
    for(int v : C) {
        int v_ub_k_C = 0;

        for(int u : C) {
            if(u != v && g.get_edge_weight(u, v) > 0) v_ub_k_C += g.get_edge_weight(u, v);
        }
        if(ub_k_C == -1 || ub_k_C > v_ub_k_C) ub_k_C = v_ub_k_C;
    }

    // std::cerr<<"ub_k_C: "<<ub_k_C<<std::endl;

    if(ub_k_C < ir1 + ir2) return 0;


    auto mc_k_C = min_cut_of_induced_graph(g, C);
    int k_C = mc_k_C.value;

    // std::cerr<<"k_C: "<<k_C<<std::endl;


    return k_C >= ir1 + ir2;
}


void check_almost_clique_has_no_forbidden_edges(const WeightedGraph & g, const vector<int> & C) {
    for(int u : C) {
        for(int v : C) {
            if(u<v)assert(g.get_edge_weight(u, v) != FORBIDDEN_WEIGHT);
        }
    }
}


vector<int> find_almost_clique_neighbor(const WeightedGraph & g) {
    for(int v : g.all_vertices()) {
        // std::cerr<<"   v="<<v<<std::endl;
        vector<int> C;
        C.push_back(v);
        for(int u : g.neighbors(v)) C.push_back(u);
        bool ok=true;
        for(int i = 0; i < (int)C.size(); ++i) {
            for(int j = i+1; j < (int)C.size(); ++j) {
                if(g.get_edge_weight(C[i], C[j]) == FORBIDDEN_WEIGHT){
                    ok = false;
                    i = C.size();
                    break;
                }
            }
        }
        if(!ok)continue;

        if(is_C_almost_clique(g, C)) return C;
    }
    return {};
}

vector<int> _find_almost_clique_heur(const WeightedGraph & g, const vector<int> & vs, int max_v) {
    vector<int> C = {max_v};
    vector<int> sup_C;
    for(int v : vs) {
        if(v==max_v) continue;
        sup_C.push_back(v);
    }

    while(sup_C.size()) {
        // std::cerr<<"sup_C: "<<sup_C.size()<<std::endl;
        int max_v=-1;
        int max_v_i=-1;
        int max_v_w=-1;
        for(int i = 0; i < (int)sup_C.size(); ++i) {
            int v = sup_C[i];
            int v_w = 0;
            bool connected_through_forbidden = false;
            for(int u : C) {
                if(g.get_edge_weight(u, v) == FORBIDDEN_WEIGHT) {
                    connected_through_forbidden = true;
                    break;
                }
                if(g.get_edge_weight(u, v) >= 0){
                    v_w += g.get_edge_weight(u, v);
                }
            }
            if(connected_through_forbidden) continue;

            if(max_v == -1 || v_w > max_v_w) {
                max_v_w = v_w;
                max_v = v;
                max_v_i = i;
            }
        }
        if(max_v==-1) break;

        C.push_back(max_v);
        sup_C[max_v_i] = sup_C[sup_C.size() - 1];
        sup_C.pop_back();

        check_almost_clique_has_no_forbidden_edges(g, C);

        if(is_C_almost_clique(g, C)) {
            return C;
        }
    }

    return {};
}


vector<int> find_almost_clique_heur(const WeightedGraph & g) {
    // std::cerr<<"find_almost_clique"<<std::endl;
    const auto & vs = g.all_vertices();

    if(vs.size() <= 1) return {};

    // for(int v : vs) {
    //     const auto & r = _find_almost_clique_heur(g, vs, v);
    //     if(r.size()) return r;
    // }

    // old heur init C
    int max_v=-1;
    int max_v_w=FORBIDDEN_WEIGHT;
    for(int v : vs) {
        int v_w = 0;
        for(int u : vs) {
            if(u==v) continue;
            if(g.get_edge_weight(u, v) == FORBIDDEN_WEIGHT) continue;
            v_w += std::abs(g.get_edge_weight(u, v));
        }
        if(max_v == -1 || v_w > max_v_w) {
            max_v_w = v_w;
            max_v = v;
        }
    }

    const auto & r = _find_almost_clique_heur(g, vs, max_v);
    if(r.size()) return r;

    return {};
}

vector<int> find_almost_clique(const WeightedGraph & g) {
    const auto & r1 = find_almost_clique_neighbor(g);
    if(r1.size()) return r1;
    const auto & r2 = find_almost_clique_heur(g);
    if(r2.size()) return r2;
    return {};
}


void AlmostCliqueReduction::apply ( WeightedGraph *& graph, Bound & bound ) {
    assert(almost_clique.size() >= 2);
    // std::cerr<<"bound.upper pre "<<bound.upper<<std::endl;
    int u = almost_clique[0];
    for(int v_i = 1; v_i < (int)almost_clique.size(); ++v_i) {
        int v = almost_clique[v_i];

        MergeReduction * merge = new MergeReduction(v,u);
        merge->apply(graph, bound);
        merges.push_back(merge);
    }
    // std::cerr<<"bound.upper post "<<bound.upper<<std::endl;
}

void AlmostCliqueReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    if ( solution ) {
        for(int i = ((int)merges.size())-1; i >= 0; --i){
            merges[i]->reconstruct_solution(solution);
        }
    }
}

AlmostCliqueReduction::~AlmostCliqueReduction(){
    for(MergeReduction * mr : merges){
        delete mr;
    }
}
