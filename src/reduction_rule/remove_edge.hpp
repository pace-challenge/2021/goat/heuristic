#pragma once

#include "reduction_rule.hpp"
#include "../graph/graph.hpp"

/*!
 * Removes an edge in an attempt to find the solution.
 */
class RemoveEdgeReduction : public ReductionRule<Graph> {
    public:

        RemoveEdgeReduction ( int from, int to );

        void apply ( Graph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

    private:
        int from, to; /*!< The edded edge endpoints */
};

