#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"

#include "merge.hpp"

#include <vector>
#include <utility>


vector<pair<int,int>> find_heavy_non_edges( const WeightedGraph & g) {
    vector<pair<int,int>> heavy_non_edges;
    const auto & vs = g.all_vertices();
    for(int i = 0; i < vs.size(); ++i) {
        for(int j = i+1; j < vs.size(); ++j) {
            int u = vs[i];
            int v = vs[j];
            if(g.get_edge_weight(u, v) >= 0 || g.get_edge_weight(u, v) == FORBIDDEN_WEIGHT) continue;

            int uw_w = 0;
            int vw_w = 0;

            for(int k = 0; k < vs.size(); ++k) {
                if(k==i || k==j) continue;
                int w = vs[k];
                if(g.get_edge_weight(u, w) >= 0) uw_w += g.get_edge_weight(u, w);
                if(g.get_edge_weight(v, w) >= 0) uw_w += g.get_edge_weight(v, w);
            }

            if(g.get_edge_weight(u, v) >= uw_w || g.get_edge_weight(u, v) >= vw_w) {
                heavy_non_edges.push_back({u, v});
            }
        }
    }
    return heavy_non_edges;
}
