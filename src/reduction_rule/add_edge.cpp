#include "add_edge.hpp"
#include <assert.h>

AddEdgeReduction::AddEdgeReduction ( int from, int to )
    : from ( from ), to ( to ) { }

void AddEdgeReduction::apply ( Graph *& graph, Bound & bound ) {
    bound.decrease_by( 1 );
    graph->add_edge( from, to );
}

void AddEdgeReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    (void) solution; // removing edge doesn't change the resulting components
}

AddWeightedEdgeReduction::AddWeightedEdgeReduction ( int from, int to, int weight )
    : from( from ), to( to ), weight( weight ) {}

void AddWeightedEdgeReduction::apply ( WeightedGraph *& graph, Bound & bound ) {
    this->original_weight = graph->get_edge_weight( from, to );
    bound.decrease_by( this->original_weight );
    assert( this->original_weight < 0 );
    graph->set_edge_weight( from, to, PERMANENT_WEIGHT );
}

void AddWeightedEdgeReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    (void) solution; // removing edge doesn't change the resulting components
}
