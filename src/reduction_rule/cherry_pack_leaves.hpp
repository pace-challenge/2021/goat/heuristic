#pragma once

#include "../solution/clique_solution.hpp"
#include "../node/node.hpp"
#include "../graph/weighted_graph.hpp"
#include "reduction_rule.hpp"

/*!
 * Reduction rule 6.9 from "Parameterizing Edge Modification Problems Above Lower Bounds"
 * written by van Bevern, Froese, and Komusiewicz.
 *
 * If $G$ contains a $P_3$ $uvw$ such that $N(u)\setminus\{u,v,w\} = N(v)\setminus\{u,v,w\}$ and $N(u)\cap N(w) = \{v\}$
 * then delete ${u,w}$ and decrease $k$ by one.
 */
class CherryPackSimilarLeaves : public ReductionRule<WeightedGraph> {
    public:
        virtual ~CherryPackSimilarLeaves() {};

        /*!
         * Applies reduction rule to an instance.
         *
         * \param[in,out] graph Instance to be modified.
         */
        void apply ( WeightedGraph *& graph, Bound &bound ) override;

        /*!
         * Modifies the solution from the modified to the original graph.
         *
         * \param[in,out] solution Modified instance solution which is to be changed.
         */
        void reconstruct_solution ( CliqueSolution * solution ) const override;

    private:
        bool is_unweighted_graph ( WeightedGraph *& graph ) const;
};
