#include "unaffordable_edges.hpp"
#include "../utils/induced_cost.hpp"
#include "../lower_bounds/greedy_disjoint_conflict_triples.hpp"
#include "../upper_bounds/meta_upper_bound.hpp"
#include "../utils/timer.hpp"

#include <algorithm>
#include <vector>
#include <iostream>
#include <unordered_set>

using namespace std;

long long _lb_counter = 0;


void UnaffordableEdgeReduction::apply ( WeightedGraph *& graph, Bound & bound ) {
    // std::cerr<<"unaff"<<std::endl;
    // Timer _lb_counter_ct;
    // _lb_counter_ct.start();

    GreedyDisjointConflictTriplesLowerBound lb_querry = GreedyDisjointConflictTriplesLowerBound(*graph);
    // applying the basic lowerboudns,
    int lowerbound = lb_querry.get_lower_bound();

    // _lb_counter += _lb_counter_ct.stop();
    // std::cerr<<"_lb_counter="<<_lb_counter<<std::endl;

    // int lowerbound = lb_querry.get_lower_bound();
    bound.lower = std::max(bound.lower, lowerbound);

    // std::cerr<<"bound.lower="<<bound.lower<<std::endl;
    // std::cerr<<"bound.upper="<<bound.upper<<std::endl;
    // std::cerr<<"bound.size="<<bound.size()<<std::endl;


    // lowerbound is higher than bound.upper, so no solution possible
    if(bound.empty()){
        return;
    }

    int max_v = 0;
    const auto & vs = graph->all_vertices();
    for(int v : vs)if(v >= max_v) max_v = v+1;

    vector<bool> merged(max_v, 0);

    // unordered_set<int> merged; // vertices that was merged so we skip them when we iterate over them
    for(int i = 0; i < (int)vs.size(); ++i) {
        int u = vs[i];
        if(merged[u]) {
            continue;
        }
        for(int j = i+1; j < (int)vs.size(); ++j) {
            int v = vs[j];
            if(merged[v]){
                continue;
            }
            if(graph->get_edge_weight(u, v) == FORBIDDEN_WEIGHT) continue;
            assert(graph->get_edge_weight(u, v) != PERMANENT_WEIGHT);

            int excluded_edge_lowerbound = lb_querry.get_masked_lower_bound(u,v);
            // int icp = induced_cost_permanent(graph, u, v) + excluded_edge_lowerbound;
            int icp = graph->get_icp(u, v) + excluded_edge_lowerbound;
            // int icf = induced_cost_forbidden(graph, u, v) + excluded_edge_lowerbound;
            int icf = graph->get_icf(u, v) + excluded_edge_lowerbound;
            // the instance cannot be solved with this budget

            // std::cerr<<"u="<<u<<", "<<
            // "v="<<v<<", "<<
            // "icp="<<icp<<", "<<
            // "icf="<<icf<<", "<<
            // "bound.lower="<<bound.lower<<", "<<
            // "bound.upper="<<bound.upper<<", "<<
            // std::endl;

            if((icp > bound.upper && icf > bound.upper)){
                bound.upper = -1;
                return;
            } else
            //cost of setting this edge permanent is too high it will be forbidden
            if( icp> bound.upper){
                if(graph->contains_edge(u,v)){
                    bound.decrease_by(graph->get_edge_weight(u,v));
                }
                lb_querry.update_lower_bound_edge_forbidden(u, v);
                graph->set_edge_forbidden(u,v);
               // std::cerr<<"FORB icp="<<icp<<" icf="<<icf<<" upper="<<bound.upper<<std::endl;
            }
            //cost setting it forbidden is too high it will be permanent
            else if( icf> bound.upper){
                lb_querry.update_lower_bound_edge_merge_u_into_v(v, u);
                // if(!graph->contains_edge(u,v)){
                //     bound.decrease_by(-graph->get_edge_weight(u, v));
                //     graph->set_edge_weight(u,v,PERMANENT_WEIGHT);
                // }
                //merge
                MergeReduction * merge = new MergeReduction(v,u);
                merge->apply(graph, bound);
                merges.push_back(merge);
                merged[v]=1;
                // std::cerr<<"PERM icp="<<icp<<" icf="<<icf<<" upper="<<bound.upper<<std::endl;
            }
        }
    }
}

void UnaffordableEdgeReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    if ( solution ) {
        for(int i = ((int)merges.size())-1; i >= 0; --i){
            merges[i]->reconstruct_solution(solution);
        }
    }
}

UnaffordableEdgeReduction::~UnaffordableEdgeReduction(){
    for(MergeReduction * mr : merges){
        delete mr;
    }
}
