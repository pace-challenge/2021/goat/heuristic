#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"

class ModifyEdgeWeightReduction : public ReductionRule<WeightedGraph> {
    public:

        ModifyEdgeWeightReduction ( int from, int to, int new_weight );

        void apply ( WeightedGraph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

    private:
        int from, to, new_weight, original_weight; /*!< The edded edge endpoints */
};

