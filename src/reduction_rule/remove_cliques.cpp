#include "remove_cliques.hpp"
#include "../utils/get_components.hpp"

void RemoveCliquesReduction::apply ( WeightedGraph *& graph, Bound & bound ) {
	vector<vector<int>> components = get_components( *graph );

	for ( const auto & C : components ) {
		bool is_clique = true;
		for ( const auto v : C ) {
			if ( graph->degree( v ) != (int)C.size() - 1 ) {
				is_clique = false; break;
			}
		}
		if ( is_clique ) {
			this->cliques.push_back(vector<RemoveVertexReduction>());
			for ( const auto v : C ) {
				this->cliques.back().emplace_back( v );
				this->cliques.back().back().apply( graph, bound );
			}
		}
	}
    (void) bound; // removing finished cliques doesn't require changing the edges
}

void RemoveCliquesReduction::reconstruct_solution ( CliqueSolution * solution ) const {
	for ( const auto & C : this->cliques ) {
		for ( const auto & red : C ) {
			red.reconstruct_solution( solution );
		}
	}
}
