#pragma once

#include "reduction_rule.hpp"
#include "remove_vertex.hpp"
#include <vector>

using std::vector;

class RemoveCliquesReduction : public ReductionRule<WeightedGraph> {
	public:
		void apply ( WeightedGraph *& graph, Bound & bound );

		void reconstruct_solution ( CliqueSolution * solution ) const;

	private:
		vector<vector<RemoveVertexReduction>> cliques;
};
