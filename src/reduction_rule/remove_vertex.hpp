#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"
#include <vector>
#include <utility>

using std::vector;
using std::pair;

/*!
 * Removes an edge in an attempt to find the solution.
 */
class RemoveVertexReduction : public ReductionRule<WeightedGraph> {
    public:

        RemoveVertexReduction ( int vertex );

        void apply ( WeightedGraph *& graph, Bound & bound );

        void reconstruct_solution ( CliqueSolution * solution ) const;

    private:
        int vertex; /*!< The removed vertex. */
};

