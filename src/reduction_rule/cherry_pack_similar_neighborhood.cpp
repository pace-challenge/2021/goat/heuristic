#include "cherry_pack_similar_neighborhood.hpp"
#include "../node/cherry.hpp"
#include <iostream>

void CherryPackSimilarNeighborhood::apply ( WeightedGraph *& graph, Bound & b ) {
    int u, v, w;
    assert( this->is_unweighted_graph( graph ) );

    auto cherries = find_cherries( graph );
    for ( const auto & cherry : cherries ) {
        std::tie(u,v,w) = cherry;

        if(!is_cherry(u, v, w, graph)) continue;
        vector<int> Nu = graph->neighbors( u, false, { u, v, w } ), 
                    Nv = graph->neighbors( v, false, { u, v, w } ), 
                    Nw = graph->neighbors( w, false, { u, v, w } );
        if ( Nu == Nv && Nv == Nw ) {
            int weight = graph->get_edge_weight( u, w );
            assert(weight == -1);
            graph->unsafe_set_edge_weight( u, w, 1 );
            b.decrease_by(1);
        }
    }
}

void CherryPackSimilarNeighborhood::reconstruct_solution ( CliqueSolution * solution ) const {
    (void) solution; // flipping u, w doesn't change connected components of the solution
}

bool CherryPackSimilarNeighborhood::is_unweighted_graph ( const WeightedGraph * graph ) const {
    vector<int> V = graph->all_vertices();
    for ( size_t i = 0; i < V.size(); ++i ) {
        for ( size_t j = i + 1; j < V.size(); ++j ) {
            if ( graph->get_edge_weight( V[ i ], V[ j ] ) != 1 && graph->get_edge_weight( V[ i ], V[ j ] ) != -1 )
                return false;
        } 
    }
    return true;
}
