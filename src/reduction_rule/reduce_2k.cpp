#include "reduce_2k.hpp"
#include "add_edge.hpp"
#include "modify_edge_weight.hpp"
#include <assert.h>
#include <iostream>

TwokReduction::TwokReduction ( int reducible_vertex ) : reducible_vertex ( reducible_vertex ) { }

pair<int,int> find_2k_reducible_vertex ( WeightedGraph * G ) {
    int delta, // soucet vah nehran v N[v],
    gamma, // soucet vah hran mezi N[v] a zbytkem grafu,
    rho,   // kolik by stalo udelat z N[v] cluster,
    z,     // pocet nulovych hran.
    Nv;    // |N[v]|

    vector<int> V = G->all_vertices();
    for ( int v : V ) {
        vector<int> N = G->neighbors( v ); N.push_back( v );
        // Is isolated vertex.
        if ( N.size() == 1 ) continue;

        Nv = N.size(), delta = gamma = z = 0;
        set<int> Ns( N.begin(), N.end() );

        for ( size_t i = 0; i < V.size(); ++i ) {
            for ( size_t j = i + 1; j < V.size(); ++j ) {
                int u = V[ i ], w = V[ j ];
                int weight = G->get_edge_weight( u, w );
                assert(weight == FORBIDDEN_WEIGHT || abs(weight - FORBIDDEN_WEIGHT) > 10000);
                if ( weight == FORBIDDEN_WEIGHT ) {
                    goto end;
                }
                if ( weight == 0 ) z += 1;
                else if ( weight < 0 ) delta += -weight;
            }
        }

        for ( int u : N ) {
            for ( int w : V ) {
                if ( !Ns.count( w ) && G->contains_edge( u, w ) )
                    gamma += G->get_edge_weight( u, w );
            }
        }
        // No second neighbors.
        if ( gamma == 0 ) continue;

        rho = 2*delta + gamma;
        if ( rho + z < Nv )
            return { 1, v };
end:;
    }

    return {-1, -1};
}

void TwokReduction::apply ( WeightedGraph *& g, Bound & bound ) {
    // Find closed neighborhood for vertex reducible_vertex.
    auto neighbors_vector = g->neighbors( reducible_vertex );

    std::cerr << "APPLY 2k: " << this->reducible_vertex << " (" << neighbors_vector.size() << ")" << std::endl;
    neighbors_vector.push_back( this->reducible_vertex );

    // create clique around reducible_vertex and its neighborhood
    this->apply_first_reduction( g, neighbors_vector, bound );

    // compare second-neighbors, if they are connected with cheaper 
    // edges than non-edges, remove them
    this->apply_second_reduction( g, neighbors_vector, bound );

    // contract the vector and its neighborhood
    this->apply_third_reduction( g, neighbors_vector, bound );
    /* std::cerr << ">>> changed " << reductions.size() << " and merged " << merge_reductions.size() << " edges" << std::endl; */
}

void TwokReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    for(auto it=reductions.crbegin(); it!=reductions.crend(); ++it) 
        (* it)->reconstruct_solution(solution);
}

void TwokReduction::apply_first_reduction ( WeightedGraph *& g, vector<int> & neighbors_vector, Bound &bound ) {
    /* std::cerr << "reduce! " << reducible_vertex << std::endl; */
    /* g->print(std::cerr); */
    /* std::cerr << reducible_vertex << " neighbors: "; */
    /* for(int n:neighbors_vector) std::cerr << n << ' '; */
    /* std::cerr<<std::endl; */
    for ( int i = 0; i < (int)neighbors_vector.size(); ++i ) {
        for ( int j = i + 1; j < (int)neighbors_vector.size(); ++j ) {
            int u = neighbors_vector[ i ], v = neighbors_vector[ j ];

            // Ondra noted that if there is an edge of weight 0,
            // then the reduction rule does not work.
            // if ( g->get_edge_weight( u, v ) == 0 )
            //     throw NotApplicableException();

            // Add edge to create clique out of N[reducible_vertex]
            if ( !g->contains_edge( u, v ) ) {
                reductions.emplace_back( new FlipEdgeReduction( u, v ) );
                reductions.back()->apply( g, bound );
                /* std::cerr << "flip (add) edge " << u << ' ' << v << std::endl; */
            }
        }
    }
}

void TwokReduction::apply_second_reduction ( WeightedGraph *& g, vector<int> & neighbors_vector, Bound &bound ) {
    set<int> second_neighbors = get_second_neighbors( g, neighbors_vector );

    for ( int x : second_neighbors ) {
        vector<pair<int,int>> edges_to_remove;
        int positive = 0, negative = 0;

        for ( int n : neighbors_vector ) {
            int w = g->get_edge_weight( x, n );
            if ( w > 0 )
                positive += w, edges_to_remove.emplace_back( x, n );
            else if ( w < 0 )
                negative -= w;
            /* else */
            /*     throw NotApplicableException(); */
        }

        if ( positive <= negative ) {
            for ( auto p : edges_to_remove ) {
                reductions.emplace_back( new FlipEdgeReduction( p.first, p.second ) );
                reductions.back()->apply( g, bound );
                /* std::cerr << "flip (rem) edge " << p.first << ' ' << p.second << std::endl; */
            }
        }
    }
}

void TwokReduction::apply_third_reduction ( WeightedGraph *& g, vector<int> & neighbors_vector, Bound &bound ) {
    set<int> second_neighbors = get_second_neighbors( g, neighbors_vector );
    size_t z = 0; // number of zero edges to N[N[v]]

    for ( int x : second_neighbors ) {
        bool is_connected = false;
        int positive = 0, negative = 0;

        for ( int y : neighbors_vector ) {
            int w = g->get_edge_weight( x, y );
            if ( w > 0 )
                positive += w, is_connected = true;
            else if ( w < 0 )
                negative += -w;
            else
                ++z;
        }

        if ( is_connected ) {
            // Merge N(v) into reducible_vertex
            for ( int y : neighbors_vector ) {
                if ( y == reducible_vertex ) continue;
                reductions.emplace_back( new MergeReduction( y, reducible_vertex ) );
                reductions.back()->apply( g, bound );
            }

            // Add an edge {x,reducible_vertex} with weight w(E(x,N(v)))-w(\notE(x,N(v)))
            assert( g->get_edge_weight( x, reducible_vertex ) == positive - negative );
            // reductions.emplace_back( new AddWeightedEdgeReduction( x, reducible_vertex, positive - negative ) );
            // reductions.back()->apply( g );

            // For all y != x in N(N[v]) add permanent edge
            if ( z == 0 ) {
                for ( int y : second_neighbors ) {
                    if ( y == x ) continue;
                    int w = g->get_edge_weight( y, reducible_vertex );
                    if ( w < 0 && w != FORBIDDEN_WEIGHT ) {
                        reductions.emplace_back( new ModifyEdgeWeightReduction( y, reducible_vertex, FORBIDDEN_WEIGHT ) );
                        reductions.back()->apply( g, bound );
                    }
                }
            }

            return;
        }
        /* std::cerr << "merge vertices " << n << " into " << reducible_vertex << std::endl; */
    }
}

set<int> TwokReduction::get_second_neighbors ( WeightedGraph *& g, const vector<int> & neighborhood ) const {
    set<int> second_neighbors;

    for ( int n : neighborhood ) 
        for ( int i : g->neighbors( n ) ) 
            second_neighbors.insert( i );

    for ( int n : neighborhood )
        second_neighbors.erase( n );

    return second_neighbors;
}
