#pragma once

#include "../solution/clique_solution.hpp"
#include "../node/node.hpp"

/*!
 * Pure virtual class representing single reduction rule.
 */
template < class GRAPH_TYPE >
class ReductionRule {
    public:
        virtual ~ReductionRule() {};

        /*!
         * Applies reduction rule to an instance.
         *
         * \param[in,out] graph Instance to be modified.
         */
        virtual void apply ( GRAPH_TYPE *& graph, Bound &bound ) = 0;

        /*!
         * Modifies the solution from the modified to the original graph.
         *
         * \param[in,out] solution Modified instance solution which is to be changed.
         */
        virtual void reconstruct_solution ( CliqueSolution * solution ) const = 0;

};
