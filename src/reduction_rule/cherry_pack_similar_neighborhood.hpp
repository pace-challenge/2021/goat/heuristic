#pragma once

#include "reduction_rule.hpp"
#include "../solution/clique_solution.hpp"
#include "../node/node.hpp"
#include "../graph/weighted_graph.hpp"

/*!
 * Reduction rule 6.8 from "Parameterizing Edge Modification Problems Above Lower Bounds"
 * written by van Bevern, Froese, and Komusiewicz.
 *
 * If $G$ contains a $P_3$ $uvw$ such that $N(u)\setminus\{u,v,w\} = N(v)\setminus\{u,v,w\} = N(w)\setminus\{u,v,w\}$, 
 * then insert ${u,w}$ and decrease $k$ by one.
 */
class CherryPackSimilarNeighborhood : public ReductionRule<WeightedGraph> {
    public:
        virtual ~CherryPackSimilarNeighborhood() {};

        /*!
         * Applies reduction rule to an instance.
         *
         * \param[in,out] graph Instance to be modified.
         */
        void apply ( WeightedGraph *& graph, Bound &bound ) override;

        /*!
         * Modifies the solution from the modified to the original graph.
         *
         * \param[in,out] solution Modified instance solution which is to be changed.
         */
        void reconstruct_solution ( CliqueSolution * solution ) const override;

    private:
        bool is_unweighted_graph ( const WeightedGraph * graph ) const;
};
