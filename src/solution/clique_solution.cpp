#include "clique_solution.hpp"
#include "../utils/get_components.hpp"

#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <cassert>

using std::swap;
using std::make_pair;
using std::endl;

long long  get_edge_idx(long long u, long long v) {
    if(u < v) return v * (v-1) / 2 + u;
    return u * (u-1) / 2 + v;
}


CliqueSolution::CliqueSolution(){}

CliqueSolution::CliqueSolution(const WeightedGraph &graph):CliqueSolution(get_components(graph)){
}

CliqueSolution::CliqueSolution(vector<vector<int>> components):components(components){
    for(int i=0; i<(int)components.size(); ++i){
        for(int n:components[i]) vertex_component[n] = i;
    }
}

void CliqueSolution::add_vertex_to_component_of ( int new_vertex, int components_vertex ){
    assert(vertex_component.count(new_vertex) == 0);
    assert(vertex_component.count(components_vertex) != 0);
    int comp_id = vertex_component[components_vertex];
    components[comp_id].push_back(new_vertex);
    vertex_component[new_vertex] = comp_id;
}

void CliqueSolution::add_vertex_as_a_component ( int new_vertex ){
    assert(vertex_component.count(new_vertex) == 0);
    int comp_id = components.size();
    components.push_back(vector<int>());
    components[comp_id].push_back(new_vertex);
    vertex_component[new_vertex] = comp_id;
}

void CliqueSolution::map_vertices ( const map<int,int> & mapping ){
    vector<vector<int>> new_components(components.size());
    map<int,int> new_vertex_component;
    for(int i=0; i<(int)components.size(); ++i){
        for(int n : components[i]) {
            int new_n;
            auto it = mapping.find(n);
            if(it!=mapping.end()) new_n = it->second; // is remapped
            else new_n = n; // stays the same as before
            new_components[i].push_back(new_n);
            new_vertex_component[new_n] = i;
        }
    }
    components = new_components;
    vertex_component = new_vertex_component;
}

void CliqueSolution::map_vertices ( const vector<int> & mapping ){
    map<int,int> m;
    for(int i=0; i<(int)mapping.size(); ++i) m[i]=mapping[i];
    return map_vertices(m);
}

void CliqueSolution::merge ( const CliqueSolution & other ){
    for(auto v : other.components){
        for(int n : v) {
            assert(vertex_component.count(n) == 0);
            vertex_component[n] = components.size();
        }
        components.push_back(v);
    }
}

Solution * CliqueSolution::get_solution ( const WeightedGraph * g ) const{
    assert(g != nullptr);
    set<pair<int,int>> solution_edges;
    for(auto &c : components){
        for(int i=0; i<(int)c.size(); ++i){
            for(int j=i+1; j<(int)c.size(); ++j){
                solution_edges.insert({c[i],c[j]});
                solution_edges.insert({c[j],c[i]});
            }
        }
    }
    Solution * res = new Solution();
    auto v = g->all_vertices();
    for(int i=0; i<(int)g->size(); ++i){
        for(int j=i+1; j<(int)g->size(); ++j){
            int g_sign = g->get_edge_sign(v[i], v[j]);
            bool s_has = solution_edges.count({v[i], v[j]});
            int weight = g->get_edge_weight(v[i], v[j]);
            if( g_sign != 0 && ((g_sign == 1) != s_has) ) res->add_edge(v[i], v[j], abs(weight));
        }
    }
    return res;
}

Solution * CliqueSolution::get_solution ( const NeighboursGraph * g ) const{
    int n = g->get_vertices_count();
    Solution * res = new Solution();
    std::unordered_map<int,int> cluster_map;
    for(int c_i = 0; c_i < components.size(); ++c_i) {
        for(int v : components[c_i]) cluster_map[v] = c_i;
    }
    std::unordered_set<long long> edges;
    for(int v = 0; v < n; ++v) {
        for(int u : g->get_adjacency_list(v)) {
            if(u<v) {
                edges.insert(get_edge_idx(u,v));
                if(cluster_map[u] != cluster_map[v]) res->add_edge(u, v, 1);
            }
        }
    }
    for(auto & cluster : components) {
        for(int i = 0; i < cluster.size(); ++i) {
            for(int j = i+1; j < cluster.size(); ++j) {
                int u=cluster[i];
                int v=cluster[j];
                if(edges.count(get_edge_idx(u,v))==0)res->add_edge(u, v, -1);
            }
        }
    }

    return res;
}

int CliqueSolution::vertex_count() const{
    return vertex_component.size();
}

ostream& operator<<(ostream &os, const CliqueSolution &c){
    for(int i=0; i<(int)c.components.size(); ++i){
        os << i << " : ";
        for(int n : c.components[i]) os << n << ' ';
        os << endl;
    }
    return os;
}

const vector<vector<int>> & CliqueSolution::_get_components() const {
    return components;
}

int CliqueSolution::component_of(int vertex) const{
    return this->vertex_component.at(vertex);
}

int CliqueSolution::get_cost( const WeightedGraph & g) const {
    const auto & vs = g.all_vertices();
    int cost = 0;
    for(int i = 0; i < (int)vs.size(); ++i) {
        for(int j = i+1; j < (int)vs.size(); ++j) {
            int u = vs[i];
            int v = vs[j];
            assert(u!=v);
            if(vertex_component.at(u) == vertex_component.at(v)) {
                assert(g.get_edge_weight(u, v) != FORBIDDEN_WEIGHT);
                cost += std::max(0, -g.get_edge_weight(u, v));
            } else {
                cost += std::max(0, g.get_edge_weight(u, v));
            }
        }
    }
    return cost;
}
