#pragma once
#include "../solution/clique_solution.hpp"
#include "../graph/neighbours_graph.hpp"

std::pair<int, CliqueSolution*> fast_lowmem_unweighted(const NeighboursGraph & g);
