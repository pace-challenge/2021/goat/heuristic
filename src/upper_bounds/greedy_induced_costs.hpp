#pragma once
#include "../solution/clique_solution.hpp"

std::pair<int, CliqueSolution*> greedy_induced_costs(const WeightedGraph & g);
