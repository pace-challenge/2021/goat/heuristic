#pragma once
#include "../solution/clique_solution.hpp"

std::pair<int, CliqueSolution*> improve_solution(const WeightedGraph & g, const CliqueSolution & cs);
