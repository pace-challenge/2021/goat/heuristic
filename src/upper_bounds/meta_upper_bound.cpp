#include "meta_upper_bound.hpp"
#include "greedy_induced_costs.hpp"
#include "improve_solution.hpp"
#include <cassert>
#include "../utils/clique_solution.hpp"

std::pair<int, CliqueSolution*> meta_upper_bound(const WeightedGraph & g) {
    auto r = greedy_induced_costs(g);
    assert(r.first != -1);
    auto r1 = improve_solution(g, *r.second);
    delete r.second;

    // check that nothing cheeky is going on
    check_clique_solution_valid(g, *r1.second);

    return r1;
}
