#pragma once
#include "../solution/clique_solution.hpp"

std::pair<int, CliqueSolution*> meta_upper_bound(const WeightedGraph & g);
