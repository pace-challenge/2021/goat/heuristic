
int cost_of_clusters(const WeightedGraph & wg, const vector<int> & cluster_vertex_map) {
    int cost = 0;
    vector<int> vs = wg.all_vertices();
    for(int i = 0; i < vs.size(); ++i) {
        for(int j = i+1; j < vs.size(); ++j) {
            int v_i = vs[i];
            int v_j = vs[j];
            if(cluster_vertex_map[v_i] == cluster_vertex_map[v_j]) {
                cost += std::max(0, -wg.get_edge_weight(v_i, v_j));
            } else {
                cost += std::max(0, wg.get_edge_weight(v_i, v_j));
            }
        }
    }
    return cost;
}

int diff_of_joining_clusters(const WeightedGraph & wg, const vector<int> & cluster_vertex_map, int c_a, int c_b) {
    assert(c_a != c_b);
    vector<int> vs = wg.all_vertices();

    vector<int> c_a_vs;
    vector<int> c_b_vs;

    for(int i = 0; i < vs.size(); ++i) {
        int v_i = vs[i];
        if(cluster_vertex_map[v_i] == c_a) {
            c_a_vs.push_back(v_i);
        }
        if(cluster_vertex_map[v_i] == c_b) {
            c_b_vs.push_back(v_i);
        }
    }

    int diff = 0;
    for(int v_c_a : c_a_vs) {
        for(int v_c_b : c_b_vs) {
            assert(v_c_a != v_c_b);
            diff -= std::max(0, wg.get_edge_weight(v_c_a, v_c_b));
            diff += std::max(0, -wg.get_edge_weight(v_c_a, v_c_b));
        }
    }

    return diff;
}

int greedy_clustering_upper_bound_v01(const WeightedGraph & wg) {
    vector<int> cluster_vertex_map;
    vector<int> clusters;
    vector<int> vs = wg.all_vertices();
    int max_v = 0;
    for(int i = 0; i < vs.size(); ++i) {
        max_v = std::max(max_v, vs[i]);
    }
    for(int i = 0; i < max_v; ++i) {
        cluster_vertex_map.push_back(-1);
    }
    for(int i = 0; i < vs.size(); ++i) {
        clusters.push_back(i);
        cluster_vertex_map[vs[i]] = i;
    }

    while(true) {
        int best_diff = 0;
        int best_c_i = 0;
        int best_c_j = 0;

        for(int c_i = 0; c_i < clusters.size(); ++c_i) {
            for(int c_j = c_i+1; c_j < clusters.size(); ++c_j) {
                int d = diff_of_joining_clusters(
                    wg, cluster_vertex_map, clusters[c_i], clusters[c_j]);
                if(d < best_diff) {
                    best_diff = d;
                    best_c_i = c_i;
                    best_c_j = c_j;
                }
            }
        }

        if(best_diff == 0) break;

        for(int v : vs) {
            if(cluster_vertex_map[v] == clusters[best_c_j]) {
                cluster_vertex_map[v] = clusters[best_c_i];
            }
        }
        clusters[best_c_j] = clusters[clusters.size()-1];
        clusters.pop_back();

        // std::cerr<<best_diff<<std::endl;
    }

    // for(int v : vs) {
    //     std::cerr<<v<<" -> "<<cluster_vertex_map[v]<<std::endl;
    // }

    // std::cerr<<"cc: "<<cost_of_clusters(wg, cluster_vertex_map) << std::endl;

    return cost_of_clusters(wg, cluster_vertex_map);
}
