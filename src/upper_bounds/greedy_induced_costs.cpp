#include "greedy_induced_costs.hpp"
#include "../utils/union_find.hpp"
#include "../utils/induced_costs.hpp"
#include "../lower_bounds/greedy_disjoint_conflict_triples.hpp"
#include <cassert>
#include "../utils/clique_solution.hpp"

using std::set;
using std::pair;
using std::tuple;

std::pair<int, CliqueSolution*> greedy_induced_costs(const WeightedGraph & g) {
    // std::cerr<<"greedy_induced_costs"<<std::endl;

    WeightedGraph * p_g_t = g.copy();
    WeightedGraph & g_t = *p_g_t;

    InducedCosts ic(g_t);
    GreedyDisjointConflictTriplesLowerBound gdct(g_t);
    UnionFind uf;
    for(int v : g_t.all_vertices()) uf.add(v);

    const auto & vs = g_t.all_vertices();

    set<tuple<int,int,int>> prio_q;
    for(int i = 0; i < (int)vs.size(); ++i) {
        for(int j = i+1; j < (int)vs.size(); ++j) {
            int u = vs[i];
            int v = vs[j];
            if(u>v)std::swap(u, v);

            if(g_t.get_edge_weight(u, v) == FORBIDDEN_WEIGHT) continue;
            assert(g_t.get_edge_weight(u, v) != PERMANENT_WEIGHT);

            int icp = ic.get_icp(u, v);
            int icf = ic.get_icf(u, v);
            int lb = gdct.get_masked_lower_bound(u, v);

            int cost = std::max(icp, icf) + lb;

            prio_q.insert({-cost, u, v});
        }
    }

    // fake - does not update the queue
    while(prio_q.size()) {
        auto pq = *prio_q.begin();
        // int cost = -std::get<0>(pq);
        int u = std::get<1>(pq);
        int v = std::get<2>(pq);
        prio_q.erase(prio_q.begin());

        if(!g_t.vertex_exists(u) || !g_t.vertex_exists(v) || g_t.get_edge_weight(u, v) == FORBIDDEN_WEIGHT) continue;

        int icp = ic.get_icp(u, v);
        int icf = ic.get_icf(u, v);
        // int lb = gdct.get_masked_lower_bound(u, v);

        // std::cerr<<cost<<' '<<u<<' '<<v<<std::endl;

        // merging is more expensive, lets do forbidden
        if(icp > icf) {
            ic.update_edge_forbidden_before(u, v, g_t);
            gdct.update_lower_bound_edge_forbidden(u, v);
            g_t.set_edge_forbidden(u, v);
            // std::cerr<<"  FORBIDDEN"<<std::endl;
        }
        // merging is less expensive, lets do it
        else {
            ic.update_edge_merge_u_to_v_before(u, v, g_t);
            gdct.update_lower_bound_edge_merge_u_into_v(u, v);
            g_t.merge(u, v);
            ic.update_edge_merge_u_to_v_after(u, v, g_t);
            uf.uni(u, v);
        }
    }

    // lets make sure that the result is valid => all edges must be forbidden and all vertices are in diffferent clusters
    // const auto & vs = g_t.all_vertices();

    for(int i = 0; i < (int)vs.size(); ++i) {
        if(!g_t.vertex_exists(vs[i]))continue;
        for(int j = i+1; j < (int)vs.size(); ++j) {
            if(!g_t.vertex_exists(vs[j]))continue;
            assert(g_t.get_edge_weight(vs[i], vs[j]) == FORBIDDEN_WEIGHT);
            assert(uf.find(vs[i]) != uf.find(vs[j]));
        }
    }

    CliqueSolution * cs = new CliqueSolution(uf.get_components());

    check_clique_solution_valid(g, *cs);

    int cs_cost = cs->get_cost(g);

    return {cs_cost, cs};







    // while(true) {
    //     int best_u = -1;
    //     int best_v = -1;
    //     int best_cost = -1;
    //     int best_icp = -1;
    //     int best_icf = -1;
    //     int best_lb = -1;

    //     for(int i = 0; i < vs.size(); ++i) {
    //         for(int j = i+1; j < vs.size(); ++j) {
    //             int u = vs[i];
    //             int v = vs[j];

    //             if(g_t.get_edge_weight(u, v) == FORBIDDEN_WEIGHT) continue;
    //             assert(g_t.get_edge_weight(u, v) != PERMANENT_WEIGHT);

    //             int icp = ic.get_icp(u, v);
    //             int icf = ic.get_icf(u, v);
    //             int lb = gdct.get_masked_lower_bound(u, v);

    //             int cost = std::min(icp, icf) + lb;
    //             if(cost > best_cost) {
    //                 best_cost = cost;
    //                 best_u = u;
    //                 best_v = v;
    //                 best_icp = icp;
    //                 best_icf = icf;
    //                 best_lb = lb;
    //             }
    //         }
    //     }

    //     if(best_cost == -1) {
    //         break;
    //     }

    //     // std::cerr
    //     // <<"best_u="<<best_u<<", "
    //     // <<"best_v="<<best_v<<", "
    //     // <<"best_cost="<<best_cost<<", "
    //     // <<"best_icp="<<best_icp<<", "
    //     // <<"best_icf="<<best_icf<<", "
    //     // <<"best_lb="<<best_lb<<", "
    //     // <<std::endl;

    //     // merging is more expensive, lets do forbidden
    //     if(best_icp > best_icf) {
    //         ic.update_edge_forbidden_before(best_u, best_v, g_t);
    //         gdct.update_lower_bound_edge_forbidden(best_u, best_v);
    //         g_t.set_edge_forbidden(best_u, best_v);
    //         // std::cerr<<"  FORBIDDEN"<<std::endl;
    //     }
    //     // merging is less expensive, lets do it
    //     else {
    //         ic.update_edge_merge_u_to_v_before(best_u, best_v, g_t);
    //         gdct.update_lower_bound_edge_merge_u_into_v(best_u, best_v);
    //         g_t.merge(best_u, best_v);
    //         ic.update_edge_merge_u_to_v_after(best_u, best_v, g_t);
    //         uf.uni(best_u, best_v);
    //         // std::cerr<<"  MERGE"<<std::endl;
    //     }
    //     // DEBUG
    //     // ic.check(g_t);
    // }

    // // lets make sure that the result is valid => all edges must be forbidden and all vertices are in diffferent clusters
    // // const auto & vs = g_t.all_vertices();
    // for(int i = 0; i < vs.size(); ++i) {
    //     for(int j = i+1; j < vs.size(); ++j) {
    //         assert(g_t.get_edge_weight(vs[i], vs[j]) == FORBIDDEN_WEIGHT);
    //         assert(uf.find(vs[i]) != uf.find(vs[j]));
    //     }
    // }

    // CliqueSolution * cs = new CliqueSolution(uf.get_components());

    // check_clique_solution_valid(g, *cs);

    // int cs_cost = cs->get_cost(g);


    // return {cs_cost, cs};
}
