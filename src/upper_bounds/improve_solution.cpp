#include "improve_solution.hpp"
#include "../utils/clique_solution.hpp"
#include <cassert>
#include <unordered_map>

int cost_of_clusters(const WeightedGraph & g, const vector<int> & cluster_vertex_map) {
    int cost = 0;
    vector<int> vs = g.all_vertices();
    for(int i = 0; i < (int)vs.size(); ++i) {
        for(int j = i+1; j < (int)vs.size(); ++j) {
            int v_i = vs[i];
            int v_j = vs[j];
            if(cluster_vertex_map[v_i] == cluster_vertex_map[v_j]) {
                cost += std::max(0, -g.get_edge_weight(v_i, v_j));
            } else {
                cost += std::max(0, g.get_edge_weight(v_i, v_j));
            }
        }
    }
    return cost;
}

int diff_of_joining_clusters(const WeightedGraph & g, const vector<int> & cluster_vertex_map, int c_a, int c_b) {
    assert(c_a != c_b);
    vector<int> vs = g.all_vertices();

    vector<int> c_a_vs;
    vector<int> c_b_vs;

    for(int i = 0; i < (int)vs.size(); ++i) {
        int v_i = vs[i];
        if(cluster_vertex_map[v_i] == c_a) {
            c_a_vs.push_back(v_i);
        }
        if(cluster_vertex_map[v_i] == c_b) {
            c_b_vs.push_back(v_i);
        }
    }

    int diff = 0;
    for(int v_c_a : c_a_vs) {
        for(int v_c_b : c_b_vs) {
            assert(v_c_a != v_c_b);
            if(g.get_edge_weight(v_c_a, v_c_b) == FORBIDDEN_WEIGHT) return INFINITY;
            assert(g.get_edge_weight(v_c_a, v_c_b) != PERMANENT_WEIGHT);

            diff -= std::max(0, g.get_edge_weight(v_c_a, v_c_b));
            diff += std::max(0, -g.get_edge_weight(v_c_a, v_c_b));
        }
    }

    return diff;
}

int diff_of_moving_v_to_cluster_c(const WeightedGraph & g, const vector<int> & cluster_vertex_map, int v, int c_c) {
    int c_v = cluster_vertex_map[v];
    assert(c_v != c_c);

    vector<int> vs = g.all_vertices();

    vector<int> c_c_vs;
    vector<int> c_v_vs;

    for(int i = 0; i < (int)vs.size(); ++i) {
        int v_i = vs[i];
        if(cluster_vertex_map[v_i] == c_c) {
            c_c_vs.push_back(v_i);
        }
        if(cluster_vertex_map[v_i] == c_v) {
            c_v_vs.push_back(v_i);
        }
    }

    int diff = 0;
    // from old cluster we must now pay for all true edges and get discont for nonedges
    for(int v_c_v : c_v_vs) {
        if(v_c_v == v) continue;
        assert(g.get_edge_weight(v_c_v, v) != FORBIDDEN_WEIGHT);
        assert(g.get_edge_weight(v_c_v, v) != PERMANENT_WEIGHT);
        // discount for nonedge
        diff -= std::max(0, -g.get_edge_weight(v_c_v, v));
        // pay for edge
        diff += std::max(0, g.get_edge_weight(v_c_v, v));
    }
    // in new cluster it is the same but the opposite
    for(int v_c_c : c_c_vs) {
        assert(v_c_c != v);
        if(g.get_edge_weight(v_c_c, v) == FORBIDDEN_WEIGHT) return INFINITY;
        assert(g.get_edge_weight(v_c_c, v) != PERMANENT_WEIGHT);
        // discount for edge
        diff -= std::max(0, g.get_edge_weight(v_c_c, v));
        // pay for nonedge
        diff += std::max(0, -g.get_edge_weight(v_c_c, v));
    }

    return diff;
}


std::pair<int, CliqueSolution*> improve_solution(const WeightedGraph & g, const CliqueSolution & cs) {
    vector<vector<int>> clusters = cs._get_components();
    const auto & vs = g.all_vertices();
    int n = -1;
    for(int v : vs) if(v>=n)n=v+1;
    vector<int> cluster_vertex_map(n, -1);
    for(int c = 0; c < (int)clusters.size(); ++c) {
        for(int v : clusters[c]) {
            cluster_vertex_map[v] = c;
        }
    }
    set<int> cluster_ids;
    for(int c : cluster_vertex_map) if(c!=-1)cluster_ids.insert(c);

    int cost = cost_of_clusters(g, cluster_vertex_map);

    while(true) {
        int best_diff = 0;
        int best_op = -1;
        int best_c1 = -1;
        int best_c2 = -1;
        int best_v = -1;
        int best_c = -1;

        // std::cerr
        // <<"before cost_of_clusters(g, cluster_vertex_map)="<<cost_of_clusters(g, cluster_vertex_map)<<", "
        // <<std::endl;


        // try joining clusters;
        for(int c1 : cluster_ids) {
            for(int c2 : cluster_ids) {
                if(c1 >= c2) continue;
                int diff = diff_of_joining_clusters(g, cluster_vertex_map, c1, c2);
                if(diff < best_diff) {
                    best_diff = diff;
                    best_op = 1;
                    best_c1 = c1;
                    best_c2 = c2;
                }
            }
        }

        // try moving vertex
        for(int v : vs) {
            int c_v = cluster_vertex_map[v];
            for(int c : cluster_ids) {
                if(c_v == c) continue;
                int diff = diff_of_moving_v_to_cluster_c(g, cluster_vertex_map, v, c);
                if(diff < best_diff) {
                    best_diff = diff;
                    best_op = 2;
                    best_v = v;
                    best_c = c;
                }
            }
        }

        // we merge clusters
        if(best_op == 1) {
            for(int v : vs) {
                if(cluster_vertex_map[v] == best_c1) cluster_vertex_map[v] = best_c2;
                cluster_ids.erase(best_c1);
            }
        }
        // we move vertex
        else if(best_op == 2) {
            cluster_vertex_map[best_v] = best_c;
        } else {
            break;
        }

        // std::cerr
        // <<"cost="<<cost<<", "
        // <<"best_diff="<<best_diff<<", "
        // <<"cost_of_clusters(g, cluster_vertex_map)="<<cost_of_clusters(g, cluster_vertex_map)<<", "
        // <<std::endl;

        assert(cost + best_diff == cost_of_clusters(g, cluster_vertex_map));
        cost += best_diff;
    }


    // reconstruct solution
    std::unordered_map<int, int> cluster_id_to_cluster_idx;
    for(int cid : cluster_ids) cluster_id_to_cluster_idx[cid] = cluster_id_to_cluster_idx.size();
    vector<vector<int>> new_clusters(cluster_ids.size());
    for(int v : vs) {
        new_clusters[
            cluster_id_to_cluster_idx[
                cluster_vertex_map[v]
            ]
        ].push_back(v);
    }

    CliqueSolution * new_cs = new CliqueSolution(new_clusters);

    check_clique_solution_valid(g, *new_cs);

    return {cost, new_cs};
}
