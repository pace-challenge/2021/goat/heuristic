#include "fast_lowmem_unweighted.hpp"
#include "greedy_induced_costs.hpp"
#include "../graph/materialized_weighted_graph.hpp"
#include "../utils/timer.hpp"
#include <cassert>
#include <algorithm>

using std::swap;
using std::tuple;

int compute_cost_of_clusters(const NeighboursGraph & g, const vector<vector<int>> & clusters) {
    int n = g.get_vertices_count();

    vector<int> cluster_map(n, -1);
    for(int c_i = 0; c_i < (int)clusters.size(); ++c_i) {
        for(int v : clusters[c_i]) cluster_map[v] = c_i;
    }
    for(int v = 0; v < n; ++v) {
        assert(cluster_map[v] != -1);
    }

    int cs_cost = 0;
    for(const auto & cluster : clusters) {
        int cs = cluster.size();
        if(cs==0) continue;
        cs_cost += cs * (cs - 1) / 2;
    }

    // std::cerr<<"ZZ cs_cost"<<cs_cost<<std::endl;

    for(int v = 0; v < n; ++v) {
        for(int u : g.get_adjacency_list(v)) {
            if(v>=u) continue;
            if(cluster_map[u] == cluster_map[v]) {
                cs_cost--;
            }
            else {
                cs_cost++;
            }
            // std::cerr<<u<<' '<<v<<" pre_cs_cost="<<pre_cs_cost<< "post cs_cost="<<cs_cost<<std::endl;
        }
    }

    // std::cerr<<"XX cs_cost"<<cs_cost<<std::endl;

    return cs_cost;
}

vector<vector<int>> meta_upper_bound_on_induced_of_g(const NeighboursGraph & g, const vector<int> & chosen_vertices) {
    vector<int> in_v_to_v_map;
    map<int, int> v_to_in_v_map;
    for(int v : chosen_vertices) {
        v_to_in_v_map[v] = in_v_to_v_map.size();
        in_v_to_v_map.push_back(v);
    }

    int n = in_v_to_v_map.size();
    MaterializedWeightedGraph mwg(n, -1);

    for(int v : chosen_vertices) {
        for(int u : g.get_adjacency_list(v)) {
            if(u>=v) continue;
            if(v_to_in_v_map.count(u) == 0) continue;
            mwg.unsafe_set_edge_weight(v_to_in_v_map[v], v_to_in_v_map[u], 1);
        }
    }
    mwg.recompute_structures();

    // std::cerr<<"greedy_induced_costs"<<std::endl;
    auto mub = greedy_induced_costs(mwg);
    vector<vector<int>> clusters;
    for(const auto & in_cluster : mub.second->_get_components()) {
        vector<int> cluster;
        for(int in_v : in_cluster) cluster.push_back(in_v_to_v_map[in_v]);
        clusters.push_back(std::move(cluster));
    }

    return clusters;
}


void check_clusters_with_v_to_cluster_id(const vector<vector<int>> & clusters, const vector<int> & v_to_cluster_id) {
    vector<bool> mask(v_to_cluster_id.size(), 0);
    for(auto cluster : clusters) {
        for(int v : cluster) {
            assert(mask[v]==false);
            mask[v]=true;
        }
    }

    for(int c_i = 0; c_i < (int)clusters.size(); ++c_i) {
        for(int v : clusters[c_i]) {
            assert(v_to_cluster_id[v] == c_i);
        }
    }
    for(int i = 0; i < (int)v_to_cluster_id.size(); ++i) {
        assert(v_to_cluster_id[i] != -1);
    }
}



bool try_join_clusters(
    const NeighboursGraph & g, vector<vector<int>> & clusters, vector<int> & v_to_cluster_id,
    int n, const vector<pair<int,int>> & edges, int & cs_cost) {
    map<pair<int,int>, int> cluster_id_pair_to_edges_between;
    for(auto e : edges) {
        int u = e.first, v = e.second;
        int c_u = v_to_cluster_id[u];
        int c_v = v_to_cluster_id[v];

        if(c_u != c_v) {
            if(c_u >= c_v) swap(c_u, c_v);
            cluster_id_pair_to_edges_between[{c_u, c_v}]++;
        }
    }

    vector<pair<int, pair<int,int>>> scores;
    for(auto p : cluster_id_pair_to_edges_between) {
        int edges_between = p.second;
        int c_u_id = p.first.first;
        int c_v_id = p.first.second;

        int c_u_size = clusters[c_u_id].size();
        int c_v_size = clusters[c_v_id].size();

        assert(c_u_size>0);
        assert(c_v_size>0);

        int non_edges_between = c_u_size * c_v_size - p.second;

        int diff = edges_between - non_edges_between;
        if(diff > 0) {
            scores.push_back({-diff, {c_u_id, c_v_id}});
        }
        if(diff > 0) {
            // std::cerr<<"c_u_id="<<c_u_id<<std::endl;
            // std::cerr<<"c_v_id="<<c_v_id<<std::endl;
            // std::cerr<<"edges_between="<<edges_between<<std::endl;
            // std::cerr<<"non_edges_between="<<non_edges_between<<std::endl;
            // std::cerr<<"diff="<<edges_between - non_edges_between<<std::endl;
            scores.push_back({-diff, {c_u_id, c_v_id}});
        }
    }
    sort(scores.begin(), scores.end());

    if(scores.size() == 0) {
        // std::cerr<<"r="<<r<<" break"<<std::endl;
        return false;
    }

    int cn = clusters.size();
    vector<bool> cluster_closed(cn, 0);

    for(auto ss : scores) {
        int diff = -ss.first;
        int c_u_id = ss.second.first;
        int c_v_id = ss.second.second;
        // std::cerr<<"c_u_id="<<c_u_id<<std::endl;
        // std::cerr<<"c_v_id="<<c_v_id<<std::endl;
        // std::cerr<<"diff="<<diff<<std::endl;

        if(cluster_closed[c_u_id] || cluster_closed[c_v_id]) continue;

        cluster_closed[c_u_id]=true;
        cluster_closed[c_v_id]=true;

        // c_u_id will be smaller cluster
        if(clusters[c_u_id].size() > clusters[c_v_id].size()) {
            swap(c_u_id, c_v_id);
        }

        // join c_u_id into c_v_id and modify
        for(int v : clusters[c_u_id]) {
            v_to_cluster_id[v] = c_v_id;
            clusters[c_v_id].push_back(v);
        }
        clusters[c_u_id].clear();
        cs_cost -= diff;
    }

    // std::cerr<<"cs_cost="<<cs_cost<<std::endl;
    return true;
}

bool try_move_vertex_between_clusters(
    const NeighboursGraph & g, vector<vector<int>> & clusters, vector<int> & v_to_cluster_id,
    int n, const vector<pair<int,int>> & edges, int & cs_cost) {
    // trying to move vertices between clusters
    map<int, int> v_to_cluster_deg;
    map<int, map<int, int>> v_to_cluster_id_to_deg;
    for(auto e : edges) {
        int u = e.first, v = e.second;
        int c_u = v_to_cluster_id[u];
        int c_v = v_to_cluster_id[v];

        if(c_u==c_v) {
            v_to_cluster_deg[u]++;
            v_to_cluster_deg[v]++;
        }
        else {
            v_to_cluster_id_to_deg[u][c_v]++;
            v_to_cluster_id_to_deg[v][c_u]++;
        }
    }

    vector<bool> moved(n, 0);

    vector<tuple<int,int,int>> should_move;
    for(auto p : v_to_cluster_deg) {
        int v = p.first;
        int c_v = v_to_cluster_id[v];

        int cluster_deg = p.second;
        int non_edge_cluster_deg = clusters[c_v].size() - 1 - p.second;

        for(auto pp : v_to_cluster_id_to_deg[v]) {
            int cluster_id = pp.first;
            int cluster_id_deg = pp.second;
            int non_edge_cluster_id_deg = clusters[cluster_id].size() - cluster_id_deg;

            int move_diff = non_edge_cluster_deg + cluster_id_deg - cluster_deg - non_edge_cluster_id_deg;
            // then it is cool to move v to cluster_id
            if(move_diff > 0 && moved[v] == false) {
                should_move.push_back({v, cluster_id, move_diff});
                moved[v]=1;
            }
        }
    }

    if(should_move.size()==0) {
        // std::cerr<<"r="<<r<<" break"<<std::endl;
        return false;
    }

    for(auto p : should_move) {
        int v, move_c_id, move_diff;
        std::tie(v, move_c_id, move_diff) = p;

        int c_v = v_to_cluster_id[v];
        int c_v_cluster_deg = v_to_cluster_deg[v];
        int c_v_non_edge_deg = clusters[c_v].size() - 1 - c_v_cluster_deg;

        int move_c_id_deg = v_to_cluster_id_to_deg[v][move_c_id];
        int move_c_id_non_edge_deg = clusters[move_c_id].size() - move_c_id_deg;

        int real_diff = c_v_non_edge_deg + move_c_id_deg - c_v_cluster_deg - move_c_id_non_edge_deg;
        if(real_diff <= 0) continue;
        // std::cerr<<"real_diff="<<real_diff<<std::endl;
        cs_cost -= real_diff;

        assert(move_c_id != c_v);

        int prev_v_c_id_deg = v_to_cluster_deg[v];
        int prev_v_c_id_move_deg = v_to_cluster_id_to_deg[v][move_c_id];

        for(int u : g.get_adjacency_list(v)) {
            assert(u!=v);
            int c_u = v_to_cluster_id[u];
            if(c_v == c_u) {
                v_to_cluster_deg[v]--;
                v_to_cluster_deg[u]--;

                v_to_cluster_id_to_deg[v][c_v]++;
                v_to_cluster_id_to_deg[u][move_c_id]++;
            }
            else if(move_c_id == c_u) {
                v_to_cluster_deg[v]++;
                v_to_cluster_deg[u]++;

                v_to_cluster_id_to_deg[v][move_c_id]--;
                v_to_cluster_id_to_deg[u][c_v]--;
            }
            else {
                v_to_cluster_id_to_deg[u][c_v]--;
                v_to_cluster_id_to_deg[u][move_c_id]++;
            }
        }

        assert(v_to_cluster_id_to_deg[v][move_c_id]==0);
        assert(v_to_cluster_id_to_deg[v][c_v]==prev_v_c_id_deg);
        assert(v_to_cluster_deg[v]==prev_v_c_id_move_deg);


        v_to_cluster_id[v] = move_c_id;
        clusters[move_c_id].push_back(v);
        for(int i = 0; i < (int)clusters[c_v].size(); ++i) {
            if(clusters[c_v][i] == v) {
                clusters[c_v][i] = clusters[c_v][clusters[c_v].size() - 1];
                clusters[c_v].pop_back();
                break;
            }
        }

        // if(cs_cost != compute_cost_of_clusters(g, clusters)) {
        //     std::cerr<<"ERR"<<cs_cost<<" "<<compute_cost_of_clusters(g, clusters)<<std::endl;
        //     std::cerr<<"post clusters:"<<std::endl;
        //     for(int c_i = 0; c_i < clusters.size(); ++c_i) {
        //         std::cerr<<c_i<<" =>";
        //         for(int v : clusters[c_i]) std::cerr<<" "<<v;
        //         std::cerr<<std::endl;
        //     }
        // }
        // assert(cs_cost == compute_cost_of_clusters(g, clusters));
        // std::cerr<<"real_diff="<<real_diff<<" cs_cost="<<cs_cost<<std::endl;

    }


    // check_clusters_with_v_to_cluster_id(clusters, v_to_cluster_id);
    // assert(cs_cost == compute_cost_of_clusters(g, clusters));
    // std::cerr<<"cs_cost="<<cs_cost<<std::endl;

    return true;
}


bool try_explode_clusters(
    const NeighboursGraph & g, vector<vector<int>> & clusters, vector<int> & v_to_cluster_id,
    int n, const vector<pair<int,int>> & edges, int & cs_cost) {

    vector<vector<pair<int,int>>> cluster_id_to_edges(clusters.size());
    for(auto e : edges) {
        int u = e.first;
        int v = e.second;
        if(v_to_cluster_id[u] == v_to_cluster_id[v]) {
            cluster_id_to_edges[v_to_cluster_id[u]].push_back(e);
        }
    }

    vector<int> cluster_ids_to_explode;

    for(int cluster_id = 0; cluster_id < clusters.size(); ++cluster_id) {
        auto & cluster = clusters[cluster_id];
        auto & cluster_edges = cluster_id_to_edges[cluster_id];
        if(cluster.size() == 0) continue;

        int all_edges_cnt = cluster.size() * (cluster.size()-1) / 2;
        int edges_cnt = cluster_edges.size();;
        int non_edges_cnt = all_edges_cnt - edges_cnt;

        if(non_edges_cnt > edges_cnt) {
            cluster_ids_to_explode.push_back(cluster_id);
        }
    }

    if(cluster_ids_to_explode.size() == 0) {
        return false;
    }

    assert(cs_cost == compute_cost_of_clusters(g, clusters));
    for(int cluster_id : cluster_ids_to_explode) {
        // std::cerr<<"EXPLODE"<<std::endl;
        auto & cluster = clusters[cluster_id];
        auto & cluster_edges = cluster_id_to_edges[cluster_id];
        int all_edges_cnt = cluster.size() * (cluster.size()-1) / 2;
        int edges_cnt = cluster_edges.size();;
        int non_edges_cnt = all_edges_cnt - edges_cnt;
        assert(non_edges_cnt > edges_cnt);
        cs_cost -= non_edges_cnt - edges_cnt;

        for(int v : clusters[cluster_id]) {
            int new_cluster_id = clusters.size();
            clusters.push_back({v});
            v_to_cluster_id[v] = new_cluster_id;
        }
        clusters[cluster_id].clear();

        assert(cs_cost == compute_cost_of_clusters(g, clusters));
    }

    return true;
}


void try_improve_clusters(const NeighboursGraph & g, vector<vector<int>> & clusters, long long PHASE_TRY_IMPROVE_CLUSTERS_DUR_MICROS) {
    Timer timer;
    timer.start();

    // we want to do some greedy linear magic to try to improve our bad clusters
    int n = g.get_vertices_count();
    vector<pair<int,int>> edges;
    for(int v = 0; v < n; ++v) {
        for(int u : g.get_adjacency_list(v)) {
            if(u<v){edges.push_back({u, v});}
        }
    }

    int cs_cost = compute_cost_of_clusters(g, clusters);

    vector<int> v_to_cluster_id(n, -1);
    for(int c_i = 0; c_i < (int)clusters.size(); ++c_i) {
        for(int v : clusters[c_i]) v_to_cluster_id[v] = c_i;
    }

    while(1) {
        bool something_changed = false;
        // std::cerr<<"cs_cost="<<cs_cost<<std::endl;

        // trying to join the clusters
        something_changed |= try_join_clusters(g, clusters, v_to_cluster_id, n, edges, cs_cost);
        something_changed |= try_move_vertex_between_clusters(g, clusters, v_to_cluster_id, n, edges, cs_cost);
        something_changed |= try_explode_clusters(g, clusters, v_to_cluster_id, n, edges, cs_cost);

        // check_clusters_with_v_to_cluster_id(clusters, v_to_cluster_id);
        // assert(cs_cost == compute_cost_of_clusters(g, clusters));



        if(something_changed==false){
            break;
        }
        if(timer.stop() > PHASE_TRY_IMPROVE_CLUSTERS_DUR_MICROS) {
            // std::cerr<<"try_improve_clusters is out of time"<<std::endl;
            break;
        }
    }

    // throw away empty clusters
    for(int c_i = 0; c_i < (int)clusters.size(); ++c_i) {
        if(clusters[c_i].size() == 0) {
            swap(clusters[clusters.size() - 1], clusters[c_i]);
            clusters.pop_back();
            c_i--;
            continue;
        }
    }


}



int estimate_initial_meta_cluster_size(const NeighboursGraph & g, long long PHASE_META_CLUSTERS_DUR_MICROS) {
    int n = g.get_vertices_count();

    vector<vector<int>> meta_clusters;
    int CLUSTER_SIZE = n;
    if(n > 1200) {
        // we estimate the cluster size based on how much time it will take to compute
        CLUSTER_SIZE = 1;
        for(int cs = 10; cs < 60; ++cs) {
            double cs_micros = 1. * cs*cs*cs/10 * 0.9;
            double cs_tot_micros =  cs_micros * n / cs;
            // std::cerr<<"cs="<<cs<<" cs_ms="<<cs_ms<<" cs_tot="<<cs_tot<<std::endl;
            CLUSTER_SIZE = cs;
            if(cs_tot_micros > PHASE_META_CLUSTERS_DUR_MICROS) {
                break;
            }
        }
    }
    return CLUSTER_SIZE;
}

vector<vector<int>> try_init_reasonable_clusters(const NeighboursGraph & g, long long PHASE_META_CLUSTERS_DUR_MICROS) {
    Timer timer;
    timer.start();

    int n = g.get_vertices_count();

    int meta_cluster_size = estimate_initial_meta_cluster_size(g, PHASE_META_CLUSTERS_DUR_MICROS);
    // std::cerr<<"meta_cluster_size="<<meta_cluster_size<<std::endl;
    vector<vector<int>> clusters;

    Timer iter_timer;
    int iter_total_micros = 0;
    bool circuit_break = false;
    int fast_counter = 0;
    int slow_counter = 0;

    double avg_meta_cluster_size = meta_cluster_size;

    int v = 0;
    vector<int> meta_cluster;
    while(v < n) {
        iter_timer.start();
        meta_cluster.push_back(v);
        v++;
        if((int)meta_cluster.size() < meta_cluster_size) continue;

        // std::cerr<<"meta_cluster="<<meta_cluster.size()<<std::endl;

        auto mub_clusters = meta_upper_bound_on_induced_of_g(g, meta_cluster);
        for(auto & mub_cluster : mub_clusters) clusters.push_back(std::move(mub_cluster));

        meta_cluster.clear();
        if(timer.stop() > PHASE_META_CLUSTERS_DUR_MICROS) {
            // std::cerr<<"try_init_reasonable_clusters is out of time"<<std::endl;
            break;
        }

        int iter_dur_micros = iter_timer.stop();
        iter_total_micros += iter_dur_micros;
        double estimate_eta_total_micros = iter_total_micros + (1. * iter_total_micros / v) * (n-v);
        // double estimate_eta_total_micros = iter_total_micros + (1. * iter_dur_micros) * (n-v);

        // std::cerr<<"n-v="<<n-v<<std::endl;
        // std::cerr<<"iter_dur_micros="<<iter_dur_micros<<std::endl;
        // std::cerr<<"estimate_eta_total_micros="<<estimate_eta_total_micros<<std::endl;


        if(estimate_eta_total_micros < PHASE_META_CLUSTERS_DUR_MICROS) {
            fast_counter++;
            slow_counter=0;
            if(fast_counter >= 3) {
                fast_counter = 0;
                meta_cluster_size++;
            }
        }
        else {
            fast_counter=0;
            slow_counter++;

            double how_much_we_over = estimate_eta_total_micros / PHASE_META_CLUSTERS_DUR_MICROS;
            if(circuit_break==false&&how_much_we_over > 1.1) {
                circuit_break = true;
                meta_cluster_size = 60;
            }

            if(slow_counter>=3) {
                slow_counter=0;
                meta_cluster_size--;
                if(meta_cluster_size < 1) meta_cluster_size = 1;
            }

            if(v > n*0.1 && (meta_cluster_size < avg_meta_cluster_size * 0.9 || meta_cluster_size > avg_meta_cluster_size * 1.1)) {
                meta_cluster_size = avg_meta_cluster_size;
            }
            if(circuit_break&&meta_cluster_size>60)meta_cluster_size=60;
        }

        avg_meta_cluster_size = (avg_meta_cluster_size * v + meta_cluster_size)/(v+1);
        // std::cerr<<"avg_meta_cluster_size="<<avg_meta_cluster_size<<std::endl;

    }

    // if we were out of time, just initialize the rest of vertices a singleton clusters
    for(int v : meta_cluster) {
        clusters.push_back({v});
    }
    while(v < n) {
        clusters.push_back({v});
        v++;
    }

    #ifndef NDEBUG
    vector<bool> mask(n, false);
    for(auto & cluster : clusters) {
        for(int v : cluster) {
            assert(mask[v]==false);
            mask[v]=true;
        }
    }
    for(int v = 0; v < n; ++v) assert(mask[v]);
    #endif

    return clusters;
}


const long long TOTAL_MAX_DUR_MICROS = 600 * 1000 * 1000;
const long long TOTAL_DEADLINE_DUR_MICROS = TOTAL_MAX_DUR_MICROS + 30 * 1000 * 1000;
const long long PHASE_META_CLUSTERS_DUR_MICROS = 250 * 1000 * 1000;

std::pair<int, CliqueSolution*> fast_lowmem_unweighted(const NeighboursGraph & g) {
    int n = g.get_vertices_count();

    Timer s;
    s.start();
    vector<vector<int>> clusters = try_init_reasonable_clusters(g, PHASE_META_CLUSTERS_DUR_MICROS);

    long long micros_remaining = TOTAL_MAX_DUR_MICROS - s.stop();
    try_improve_clusters(g, clusters, micros_remaining);

    int cs_cost = compute_cost_of_clusters(g, clusters);

    if(s.stop() > TOTAL_DEADLINE_DUR_MICROS) {
        throw 1;
    }

    return {cs_cost, new CliqueSolution(clusters)};
}
