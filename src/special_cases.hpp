#include "solution/clique_solution.hpp"
#include "graph/weighted_graph.hpp"

#include <vector>
#include <utility>

using namespace std;

/*!
 * Few utility self-explanatory methods. They shouldn't fail, just return true/false.
 */
bool is_connected(const WeightedGraph & graph);
bool is_path(const WeightedGraph & graph);
bool is_cycle(const WeightedGraph & graph);

/*!
* Calculate how much it will cost to make chosen vertices a clique only on those vertices. Vertices outside chosen vertices are not used.
*
* \param[in] graph Weighted graph according which weights it will be counted.
* \param[in] chosen_vertices Vertices which we want to make a clique from.
*/
int calculate_cluster_cost(const WeightedGraph & graph, vector<int> chosen_vertices);

/*!
* Finds if graph forms a path and if so returns it as a sequence of vertices.
*
* \param[in] graph Weighted graph representing a weighted path with at least 2 vertices.
*/
vector<int> get_path(const WeightedGraph & graph);

/*!
* Finds cheapest way to modify path into cluster graph.
*
* \param[in] graph Weighted graph representing a weighted path with at least 2 vertices.
*/
pair<int, CliqueSolution*> solve_path(const WeightedGraph & graph);

/*!
* Finds cheapest way to modify cycle into cluster graph.
*
* \param[in] graph Weighted graph representing a weighted cycle.
*/
pair<int, CliqueSolution*> solve_cycle(const WeightedGraph & graph);

