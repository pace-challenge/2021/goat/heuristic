#include "induced_cost.hpp"

std::pair<int, int> induced_cost_permanent_zero_edges_count(const WeightedGraph * g, int u, int v){
    int zero_edges_count = 0;
    if(g->get_edge_weight(u, v) == 0){
        --zero_edges_count;
    } 
    // cost of setting edge to permanent
    int cost = std::max(0, -g->get_edge_weight(u, v));

    for(int w : g->all_vertices()){
        // we have to count edges that are both zero so we dont skip those
        if(u == w || v == w || (g->get_edge_sign(u,w) == g->get_edge_sign(v,w) && g->get_edge_sign(u,w) != 0)){
            continue;
        }
        cost += std::min(abs(g->get_edge_weight(v,w)), abs(g->get_edge_weight(u,w)));
        // counting added and removed zeroes
        if(g->get_edge_weight(u,w) == 0){
            --zero_edges_count;
        }
        if(g->get_edge_weight(v,w) == 0){
            --zero_edges_count;
        }
        if(g->get_edge_weight(u,w) == -g->get_edge_weight(v,w)){
            ++zero_edges_count; 
        }
    }
    return {cost, zero_edges_count};
}

int induced_cost_permanent(const WeightedGraph * g, int u, int v){
    return induced_cost_permanent_zero_edges_count(g,u,v).first;
}

int induced_cost_forbidden(const WeightedGraph * g, int u, int v){
    // cost of setting edge to forbidden
    int cost = std::max(0, g->get_edge_weight(u, v));

    // cost for vertices neighbor to v but not u
    for(int w : g->neighbors(v)){
        if(u == w || !g->contains_edge(u,w)){
            continue;
        }
        cost += std::min(g->get_edge_weight(v,w), g->get_edge_weight(u,w));
    }
    return cost;
}
