#pragma once

#include "../graph/weighted_graph.hpp"

#include <utility>


// Data reduction - rule 2
// cost of setting edge permanent
int induced_cost_permanent(const WeightedGraph * g, int u, int v);

// similar as induced_cost_permanent, but it also returns number of zero edges created
// returns pair<induced cost, number of zero edges created>
std::pair<int, int> induced_cost_permanent_zero_edges_count(const WeightedGraph * g, int u, int v);

// cost of setting edge forbidden
int induced_cost_forbidden(const WeightedGraph * g, int u, int v);
