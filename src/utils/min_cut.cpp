#include "min_cut.hpp"
#include <queue>
#include <cassert>

namespace mimino{
    struct MaxFlow {
        //mimino
        int N;
        struct Edge { int from, to, residue; };
        vector<Edge> edges;
        vector<vector<int>> graph; // graph[node] = indices of outgoing edges

        void add_edge(int from, int to, int capacity) {
            // std::cerr<<from<<' '<<to<<' '<<capacity<<std::endl;
            graph[from].push_back(edges.size());
            edges.push_back(Edge{from, to, capacity});
            graph[to].push_back(edges.size());
            edges.push_back(Edge{to, from, 0});
        }

        vector<int> back;
        bool bfs(int source, int sink) {
            back=vector<int>(N,-1);
            back[source] = -2;
            std::queue<int> q;
            q.push(source);
            while (!q.empty() && back[sink] == -1) {
                int node = q.front();
                q.pop();
                for(int e:graph[node]){
                    Edge & edge = edges[e];
                    if (edge.residue && back[edge.to] == -1) {
                        back[edge.to] = e;
                        q.push(edge.to);
                    }
                }
            }
            return back[sink] != -1;
        }

        int maxflow(int source, int sink) {
            int total_flow = 0;
            while (bfs(source, sink)) {
                // find size of the flow
                int flow = 1<<30, node = sink;
                while (back[node] != -2) {
                    Edge & edge = edges[back[node]];
                    flow = std::min(flow, edge.residue);
                    node = edge.from;
                }
                // push the flow
                node = sink;
                while (back[node] != -2) {
                    Edge & edge = edges[back[node]],
                         & edge2 = edges[back[node]^1];
                    edge.residue -= flow;
                    edge2.residue += flow;
                    node = edge.from;
                }
                total_flow += flow;
            }
            return total_flow;
        }

        vector<pair<int,int>> mincut(int source) {
            vector<bool> reachable(N, 0);
            std::queue<int> q;
            q.push(source);
            reachable[source]=1;
            while (!q.empty()) {
                int node = q.front();
                q.pop();
                for(int e:graph[node]){
                    Edge & edge = edges[e];
                    if (edge.residue && !reachable[edge.to]) {
                        reachable[edge.to]=1;
                        q.push(edge.to);
                    }
                }
            }

            // std::cerr<<"reachable:";
            // for(int i = 0; i < N; ++i) {
            //     if(reachable[i])
            //         std::cerr<<" "<<i;
            // }
            // std::cerr<<std::endl;


            vector<pair<int,int>> mincut;

            for(int e = 0; e < (int)edges.size(); e+=2) {
                Edge & e1 = edges[e];
                Edge & e2 = edges[e+1];

                int u = e1.from;
                int v = e1.to;
                if(u>v)continue;
                // on the edge
                if(reachable[u] + reachable[v] == 1) {
                    assert(e1.residue == 0 || e2.residue == 0);
                    mincut.push_back({u, v});
                }
            }

            return mincut;
        }
    };
    ///mimino
}

// the copy is necessary
MinCut _max_flow_call(mimino::MaxFlow mf, int s, int t) {
    // std::cerr<<s<<' '<<t<<std::endl;
    int f = mf.maxflow(s, t);
    // std::cerr<<f<<std::endl;
    return MinCut(f, mf.mincut(s));
}

MinCut min_cut_of_induced_graph(const WeightedGraph & g, const vector<int> & vs) {
    if(vs.size() <= 1) {
        return MinCut(0, {});
    }

    // remapping is needed
    int max_v = 0;
    for(int v : vs) {
        if(v >= max_v) max_v = v + 1;
    }
    vector<int> v_to_in_v(max_v, -1);
    vector<int> in_v_to_v;
    for(int v : vs) {
        assert(v>=0);
        assert(v<max_v);
        v_to_in_v[v] = in_v_to_v.size();
        in_v_to_v.push_back(v);
    }

    // initialize MaxFlow network structure - this will serve as ready blueprint
    int n = in_v_to_v.size();
    assert(n>=2);
    mimino::MaxFlow mf;
    mf.N = n;
    mf.graph.resize(n);
    for(int i = 0; i < (int)vs.size(); ++i) {
        for(int j = i+1; j < (int)vs.size(); ++j) {
            int u = vs[i];
            int v = vs[j];
            int c = g.get_edge_weight(u, v);
            if(c <= 0) continue;
            mf.add_edge(v_to_in_v[u], v_to_in_v[v], c);
            mf.add_edge(v_to_in_v[v], v_to_in_v[u], c);
        }
    }

    MinCut min_mc = _max_flow_call(mf, 0, 1);

    // perform the computation
    for(int t = 2; t < n; ++t) {
        MinCut mc = _max_flow_call(mf, 0, t);
        if(mc.value < min_mc.value) min_mc = mc;
    }

    MinCut r_mc(min_mc.value, {});
    for(auto e : min_mc.edges) {
        int u = in_v_to_v[e.first];
        int v = in_v_to_v[e.second];
        if(u>v)std::swap(u,v);
        r_mc.edges.push_back({u, v});
    }


    // check
    int r_mc_sum = 0;
    for(auto e : r_mc.edges) r_mc_sum += g.get_edge_weight(e.first, e.second);
    assert(r_mc_sum == r_mc.value);

    return r_mc;
}
