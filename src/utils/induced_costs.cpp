#include "induced_costs.hpp"
#include <cassert>

InducedCosts::InducedCosts(const WeightedGraph & g) {
    const auto & vs = g.all_vertices();

    int max_v = 0;
    for(int v : vs) {
        if(v>=max_v)max_v = v+1;
    }

    // for(int v : vs) {
    //     valid_vertices.insert(v);
    // }
    icp.resize(max_v*(max_v+1)/2);
    icf.resize(max_v*(max_v+1)/2);

    for(int i = 0; i < (int)vs.size(); ++i) {
        for(int j = i+1; j < (int)vs.size(); ++j) {
            _set_icp(vs[i], vs[j], compute_icp(vs[i], vs[j], g));
            _set_icf(vs[i], vs[j], compute_icf(vs[i], vs[j], g));
        }
    }
}

int InducedCosts::get_icp(int u, int v) const {
    return icp.at(_get_edge_idx(u, v));
}

int InducedCosts::get_icf(int u, int v) const {
    return icf.at(_get_edge_idx(u, v));
}
///////////////////////////////////////////////////////////////////////

void InducedCosts::_set_icp(int u, int v, int val) {
    icp[_get_edge_idx(u, v)] = val;
}

void InducedCosts::_set_icf(int u, int v, int val) {
    // std::cerr<<"_set_icf "<<u<<' '<<v<<", pre icf="<<icf[_get_edge_idx(u, v)]<<", val="<<val<<std::endl;
    icf[_get_edge_idx(u, v)] = val;

}

///////////////////////////////////////////////////////////////////////

int InducedCosts::_get_edge_idx(int u, int v) const {
    if(u < v) std::swap(u, v);
    assert(u>v);
    return u * (u-1) / 2 + v;
}

int InducedCosts::_add(int a, int b) const {
    if(a==INFINITY || a==-INFINITY) {
        assert(a!=-b);
        return a;
    }
    if(b==INFINITY || b==-INFINITY) {
        assert(a!=-b);
        return b;
    }
    return a+b;
}

///////////////////////////////////////////////////////////////////////

int InducedCosts::compute_icp(int u, int v, const WeightedGraph & g) const {
    // cannot set forbidden edge to permanent
    if(g.get_edge_weight(u, v) == FORBIDDEN_WEIGHT) {
        return INFINITY;
    }

    int icp = std::max(0, -g.get_edge_weight(u, v));

    for(int w : g.all_vertices()) {
        if(w==u || w==v) continue;

        int e_uw = g.get_edge_weight(u, w);
        int e_vw = g.get_edge_weight(v, w);

        // symmetric difference neighborhood
        if((e_uw > 0 && e_vw < 0) || (e_uw < 0 && e_vw > 0)) {
            icp = _add(icp, std::min(std::abs(e_uw), std::abs(e_vw)));
            if(icp == INFINITY) return INFINITY;
        }
    }

    assert(icp>=0 && icp < INFINITY);
    return icp;
}

int InducedCosts::compute_icf(int u, int v, const WeightedGraph & g) const {
    // cannot set permanent edge to forbidden
    if(g.get_edge_weight(u, v) == PERMANENT_WEIGHT) {
        return INFINITY;
    }
    if(g.get_edge_weight(u, v) == FORBIDDEN_WEIGHT) {
        return 0;
    }

    int icf = std::max(0, g.get_edge_weight(u, v));

    for(int w : g.all_vertices()) {
        if(w==u || w==v) continue;

        int e_uw = g.get_edge_weight(u, w);
        int e_vw = g.get_edge_weight(v, w);

        // true neighborhood
        if(e_uw > 0 && e_vw > 0) {
            icf = _add(icf, std::min(e_uw, e_vw));
            if(icf == INFINITY) return INFINITY;
        }
    }

    assert(icf>=0 && icf < INFINITY);
    return icf;
}

///////////////////////////////////////////////////////////////////////

// this must be called before the edge was set to forbidden in the graph!
void InducedCosts::update_edge_forbidden_before(int u, int v, const WeightedGraph & g) {
    _set_icf(u, v, 0);
    _set_icp(u, v, INFINITY);

    _update_edge_forbidden_icf(u, v, g);
    _update_edge_forbidden_icp_ux(u, v, g);
    _update_edge_forbidden_icp_vx(u, v, g);
}

void InducedCosts::_update_edge_forbidden_icf(int u, int v, const WeightedGraph & g) {
    const auto & vs = g.all_vertices();
    int uv_weight = g.get_edge_weight(u, v);

    // icf looks for true neighborhood - if v was not a neighbor of both before, it will also not be after update
    // if the edge did not exits before, nothing cahnges for icf
    if(uv_weight < 0) return;

    // update the icf for ux
    for(int x : vs) {
        if(x == u || x == v) continue;
        if(g.get_edge_weight(v, x) < 0) {
            continue;
        }
        if(g.get_edge_weight(u, x) == FORBIDDEN_WEIGHT) {
            continue;
        }
        // now we know that v was common neighbor of u, x before and it would not be, because we set uv to forbidden
        int vx_weight = g.get_edge_weight(v, x);
        int new_val = get_icf(u, x) - std::min(uv_weight, vx_weight);

        // std::cerr
        // <<"u="<<u<<", "
        // <<"x="<<x<<", "
        // <<"get_icf(u, x)="<<get_icf(u, x)<<", "
        // <<"uv_weight="<<uv_weight<<", "
        // <<"vx_weight="<<vx_weight<<", "
        // <<"new_val="<<new_val<<", "
        // <<std::endl;

        _set_icf(u, x, new_val);
    }
    // update the icf for vx
    for(int x : vs) {
        if(x == u || x == v) continue;
        if(g.get_edge_weight(u, x) < 0) {
            continue;
        }
        if(g.get_edge_weight(v, x) == FORBIDDEN_WEIGHT) {
            continue;
        }
        // now we know that v was common neighbor of u, x before and it would not be, because we set uv to forbidden
        int ux_weight = g.get_edge_weight(u, x);
        int new_val = get_icf(v, x) - std::min(uv_weight, ux_weight);

        // std::cerr
        // <<"v="<<v<<", "
        // <<"x="<<x<<", "
        // <<"get_icf(v, x)="<<get_icf(v, x)<<", "
        // <<"uv_weight="<<uv_weight<<", "
        // <<"ux_weight="<<ux_weight<<", "
        // <<"new_val="<<new_val<<", "
        // <<std::endl;

        _set_icf(v, x, new_val);
    }
}

void InducedCosts::_update_edge_forbidden_icp_ux(int u, int v, const WeightedGraph & g) {
    const auto & vs = g.all_vertices();
    int uv_weight = g.get_edge_weight(u, v);

    // icp looks for symdiff neighborhood - if v was not a neighbor of both before, it will also not be after update
    for(int x : vs) {
        if(x == u || x == v) continue;
        int vx_weight = g.get_edge_weight(v, x);
        int d = std::min(std::abs(uv_weight), std::abs(vx_weight));
        assert(d!=INFINITY);
        assert(vx_weight!=PERMANENT_WEIGHT);

        int new_val = get_icp(u, x);
        if(uv_weight > 0 && vx_weight > 0) {
            new_val = _add(new_val, vx_weight);
        }
        else if (uv_weight <= 0 && vx_weight > 0) {
            new_val = _add(new_val, - d + vx_weight);
        }
        else if (uv_weight > 0 && vx_weight <= 0) {
            new_val = _add(new_val, - d);
        }
        else if (uv_weight <= 0 && vx_weight <= 0) {
            // no update
        }

        _set_icp(u, x, new_val);
    }
}

void InducedCosts::_update_edge_forbidden_icp_vx(int u, int v, const WeightedGraph & g) {
    const auto & vs = g.all_vertices();
    int uv_weight = g.get_edge_weight(u, v);

    // icp looks for symdiff neighborhood - if v was not a neighbor of both before, it will also not be after update
    for(int x : vs) {
        if(x == u || x == v) continue;
        int ux_weight = g.get_edge_weight(u, x);
        int d = std::min(std::abs(uv_weight), std::abs(ux_weight));
        assert(d!=INFINITY);
        assert(ux_weight!=PERMANENT_WEIGHT);

        int new_val = get_icp(v, x);
        if(uv_weight > 0 && ux_weight > 0) {
            new_val = _add(new_val, ux_weight);
        }
        else if (uv_weight <= 0 && ux_weight > 0) {
            new_val = _add(new_val, - d + ux_weight);
        }
        else if (uv_weight > 0 && ux_weight <= 0) {
            new_val = _add(new_val, - d);
        }
        else if (uv_weight <= 0 && ux_weight <= 0) {
            // no update
        }

        _set_icp(v, x, new_val);
    }
}

///////////////////////////////////////////////////////////////////////

// this must be called before the edge is merged to v!
void InducedCosts::update_edge_merge_u_to_v_before(int u, int v, const WeightedGraph & g) {
    const auto & vs = g.all_vertices();

    for(int i = 0; i < (int)vs.size(); ++i) {
        int w = vs[i];
        if(w==u||w==v) continue;

        for(int j = i+1; j < (int)vs.size(); ++j) {
            int x = vs[j];
            if(x==u||x==v||x==w) continue;
                _update_edge_merge_u_to_v_icf_uvwx(u, v, w, x, g);
                _update_edge_merge_u_to_v_icp_uvwx(u, v, w, x, g);
        }
    }
}

void InducedCosts::_update_edge_merge_u_to_v_icf_uvwx(int u, int v, int w, int x, const WeightedGraph & g) {
    int icf = get_icf(w, x);

    if(g.get_edge_weight(w, x) == FORBIDDEN_WEIGHT) return;

    int e_xu = g.get_edge_weight(x, u);
    int e_wu = g.get_edge_weight(w, u);

    int e_xv = g.get_edge_weight(x, v);
    int e_wv = g.get_edge_weight(w, v);


    // std::cerr
    // <<"u="<<u<<", "
    // <<"v="<<v<<", "
    // <<"w="<<w<<", "
    // <<"x="<<x<<", "
    // <<"icf="<<icf<<", "
    // <<"e_xu="<<e_xu<<", "
    // <<"e_wu="<<e_wu<<", "
    // <<"e_xv="<<e_xv<<", "
    // <<"e_wv="<<e_wv<<", "
    // <<std::endl;

    // std::cerr<<"compute_icf(w, x)="<<compute_icf(w, x)<<std::endl;
    // assert(compute_icf(w,x) == icf);

    if(e_xu > 0 && e_wu > 0) {
        assert(e_xu != PERMANENT_WEIGHT);
        assert(e_wu != PERMANENT_WEIGHT);
        icf -= std::min(e_xu, e_wu);
    }

    if(e_xv > 0 && e_wv > 0) {
        assert(e_xv != PERMANENT_WEIGHT);
        assert(e_wv != PERMANENT_WEIGHT);
        icf -= std::min(e_xv, e_wv);
    }

    int e_xu_prime = _add(e_xu, e_xv);
    int e_wu_prime = _add(e_wu, e_wv);

    // std::cerr
    // <<"e_xu_prime="<<e_xu_prime<<", "
    // <<"e_wu_prime="<<e_wu_prime<<", "
    // <<std::endl;

    if(e_xu_prime > 0 && e_wu_prime > 0) {
        icf += std::min(e_xu_prime, e_wu_prime);
    }

    // std::cerr<<"icf="<<icf<<std::endl;

    assert(icf>=0);
    _set_icf(w, x, icf);
}

void InducedCosts::_update_edge_merge_u_to_v_icp_uvwx(int u, int v, int w, int x, const WeightedGraph & g) {
    int icp = get_icp(w, x);

    if(g.get_edge_weight(w, x) == FORBIDDEN_WEIGHT) return;

    int e_xu = g.get_edge_weight(x, u);
    int e_wu = g.get_edge_weight(w, u);

    int e_xv = g.get_edge_weight(x, v);
    int e_wv = g.get_edge_weight(w, v);


    // std::cerr
    // <<"u="<<u<<", "
    // <<"v="<<v<<", "
    // <<"w="<<w<<", "
    // <<"x="<<x<<", "
    // <<"icp="<<icp<<", "
    // <<"e_xu="<<e_xu<<", "
    // <<"e_wu="<<e_wu<<", "
    // <<"e_xv="<<e_xv<<", "
    // <<"e_wv="<<e_wv<<", "
    // <<std::endl;

    if((e_xu > 0 && e_wu < 0) || (e_xu < 0 && e_wu > 0)) {
        assert(e_xu != PERMANENT_WEIGHT);
        assert(e_wu != PERMANENT_WEIGHT);
        icp -= std::min(std::abs(e_xu), std::abs(e_wu));
    }

    if((e_xv > 0 && e_wv < 0) || (e_xv < 0 && e_wv > 0)) {
        assert(e_xv != PERMANENT_WEIGHT);
        assert(e_wv != PERMANENT_WEIGHT);
        icp -= std::min(std::abs(e_xv), std::abs(e_wv));
    }

    int e_xu_prime = _add(e_xu, e_xv);
    int e_wu_prime = _add(e_wu, e_wv);

    // std::cerr
    // <<"icp="<<icp<<", "
    // <<"e_xu_prime="<<e_xu_prime<<", "
    // <<"e_wu_prime="<<e_wu_prime<<", "
    // <<std::endl;

    if((e_xu_prime > 0 && e_wu_prime < 0) || (e_xu_prime < 0 && e_wu_prime > 0)) {
        icp += std::min(std::abs(e_xu_prime), std::abs(e_wu_prime));
    }

    assert(icp>=0);
    _set_icp(w, x, icp);
}

void InducedCosts::update_edge_merge_u_to_v_after(int u, int v, const WeightedGraph & g) {
    // u is no longer valid in the graph, because it was merged into v
    for(int w : g.all_vertices()) {
        assert(w!=u);
        if(w==v) continue;
        _set_icp(v, w, compute_icp(v, w, g));
        _set_icf(v, w, compute_icf(v, w, g));
    }
}

void InducedCosts::check(const WeightedGraph & g) const {
    const auto & vs = g.all_vertices();
    for(int i = 0; i < (int)vs.size(); ++i) {
        for(int j = i+1; j < (int)vs.size(); ++j) {
            if(get_icf(vs[i], vs[j]) != compute_icf(vs[i], vs[j], g)) {
                std::cerr<<"icf mismatch = "<<vs[i]<<" "<<vs[j]<<", "
                <<"get_icf("<<vs[i]<<", "<<vs[j]<<")="<<get_icf(vs[i], vs[j])<<", "
                <<"compute_icf("<<vs[i]<<", "<<vs[j]<<")="<<compute_icf(vs[i], vs[j], g)<<", "
                <<std::endl;

                assert(0);
            }
            if(get_icp(vs[i], vs[j]) != compute_icp(vs[i], vs[j], g)) {
                std::cerr<<"icp mismatch = "<<vs[i]<<" "<<vs[j]<<", "
                <<"get_icp("<<vs[i]<<", "<<vs[j]<<")="<<get_icp(vs[i], vs[j])<<", "
                <<"compute_icp("<<vs[i]<<", "<<vs[j]<<")="<<compute_icp(vs[i], vs[j], g)<<", "
                <<std::endl;
                assert(0);
            }
        }
    }
}


void perform_induced_costs_graph_edge_forbidden(int u, int v, WeightedGraph & g, InducedCosts & ic) {
    ic.update_edge_forbidden_before(u, v,g );
    g.set_edge_weight(u, v, FORBIDDEN_WEIGHT);
}

void perform_induced_costs_graph_edge_merge_u_to_v(int u, int v, WeightedGraph & g, InducedCosts & ic) {
    ic.update_edge_merge_u_to_v_before(u, v, g);
    g.merge(u, v);
    ic.update_edge_merge_u_to_v_after(u, v, g);
}

