#include "get_components.hpp"

#include <stack>

void find_component_rec ( const WeightedGraph & graph, int v, vector<int> & c, map<int,bool> &found ) {
	if ( found[ v ] )
		return;

	found[ v ] = true;
	c.push_back( v );

	for ( auto u : graph.neighbors( v ) )
		find_component_rec( graph, u, c, found );
}

vector<vector<int>> get_components ( const WeightedGraph & graph ) {
	map<int,bool> found;
	vector<int> vertices = graph.all_vertices();
	vector<vector<int>> components;

	for ( auto v : vertices ) 
		found[ v ] = false;

	for ( auto v : vertices ) {
		if ( !found[ v ] ) {
			components.push_back(vector<int>());
			find_component_rec( graph, v, components.back(), found );
		}
	}

	return components;
}



void find_component ( const NeighboursGraph & graph, int v, vector<int> & c, map<int,bool> &found ) {
	if ( found[ v ] )
		return;

	found[ v ] = true;
	c.push_back( v );

	std::stack<int> s;
	s.push(v);
	while(!s.empty()){
		int current = s.top();
		s.pop();
		for ( auto u : graph.get_adjacency_list( current ) ){
			if(!found[u]){
				s.push(u);
				c.push_back(u);
				found[u] = true;
			}
		}
	}
}

vector<vector<int>> get_components ( const NeighboursGraph & graph ) {
	map<int,bool> found;
	vector<vector<int>> components;

	for(int v = 0; v < (int)graph.get_vertices_count(); ++v){
		found[ v ] = false;

	}

	for(int v = 0; v < (int)graph.get_vertices_count(); ++v){
		if ( !found[ v ] ) {
			components.push_back(vector<int>());
			find_component( graph, v, components.back(), found );
		}
	}

	return components;
}

