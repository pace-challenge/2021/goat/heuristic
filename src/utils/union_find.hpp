#pragma once

#include <vector>
#include <unordered_map>
#include <cassert>

using std::unordered_map;
using std::vector;

struct UnionFind {
    void add(int u) {
        assert(u>=0);
        assert(p.count(u) == 0);
        p[u] = -1;
        h[u] = 0;
    }

    void uni(int u, int v) {
        assert(p.count(u) == 1);
        assert(p.count(v) == 1);
        u = find(u);
        v = find(v);
        if(u!=v) {
            int u_h = h[u];
            int v_h = h[v];
            if(u_h < v_h) {
                p[u] = v;
            }
            else if(u_h > v_h) {
                p[v] = u;
            }
            else {
                p[u] = v;
                h[v]++;
            }
        }
    };

    int find(int u) {
        assert(p.count(u) == 1);
        if(p[u] == -1) return u;
        int v = find(p[u]);
        p[u] = v;
        return v;
    };

    vector<vector<int>> get_components() {
        std::unordered_map<int, vector<int>> cc_map;

        for(auto x : p) {
            cc_map[find(x.first)].push_back(x.first);
        }

        vector<vector<int>> cc;
        for(auto & x : cc_map) {
            cc.push_back(std::move(x.second));
        }
        return cc;
    }

    std::unordered_map<int, int> p;
    std::unordered_map<int, int> h;
};