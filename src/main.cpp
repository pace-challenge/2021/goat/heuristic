#include "computation/eval.hpp"
#include "node/node.hpp"
#include "strategy/priority_strategy.hpp"
#include "computation/tree.hpp"
#include "graph/conversion.hpp"
#include "graph/weighted_graph.hpp"
#include "graph/dumb_weighted_graph.hpp"
#include "graph/old_materialized_graph.hpp"
#include "io/input_reader.hpp"
#include "solution.hpp"
#include "node/cherry.hpp"
#include "node/iter.hpp"

#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;

#define INF (1<<30)

#include "reduction_rule/merge.hpp"
using std::vector;


int priority(const Node *n){
    (void)n;
    return 1;
}

int main ( void ) {
    InputReader in;

    auto graphFactory = [](int SZ){ return new OldMaterializedGraph(SZ); };
    Graph *g = in.read( cin );
    WeightedGraph * graph = convert_graph_to_weighted_graph(*g, graphFactory);
    delete g;

    PriorityStrategy *strategy = new PriorityStrategy(priority);
    ComputationTree ct(strategy);
    Node * root = new IterNode(graph->copy(), Bound(0, INF), [](WeightedGraph *g, Bound b){return new GreedyCherryNode(g, b.upper);});
    Solution *solution = ct.evaluate(root);
    cerr << "root: " << root << endl;
    if(solution){
        cerr << "=== solution ===\n";
        cout << (*solution);
        cerr << "=== solution ===\n";
    }
    else cerr << "no solution found!\n";
    delete graph;
    delete solution;
    return 0;
}
