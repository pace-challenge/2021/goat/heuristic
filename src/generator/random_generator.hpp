#pragma once

#include <random>

#include "../graph/graph.hpp"
/*!
 * Class for generating random graphs.
 */
class RandomGenerator
{
public:
    RandomGenerator():gen(std::random_device()()){}
    RandomGenerator(int seed):gen(std::random_device()()){
        gen.seed(seed);
    }
    ~RandomGenerator() = default;

    /*!
    * Generates random graph.
    *
    * \param[in] n Number of vertices.
    * \param[in] m Number of edges.
    * \param[in] k Number of components.
    */
    Graph generate_graph(int n, int m, int k);
private:
    std::mt19937 gen;

    /*!
    * Creates graph on n vertices with k fully connected components.
    *
    * \param[in] n Number of vertices.
    * \param[in] k Number of components.
    */
    Graph divide_to_components(int n, int k);

    /*!
    * Creates random permutation of vertices from 0 to n-1.
    *
    * \param[in] n Number of vertices.
    */
    std::vector<int> random_ordered_vertices(int n);

    /*!
    * Adds clique to graph on vertices provided by component.
    *
    * \param[inout] graph Graph to which clique will be added.
    * \param[in] component Vertices of a component.
    */
    void add_clique(Graph & graph, std::vector<int> const & component) const;
};
