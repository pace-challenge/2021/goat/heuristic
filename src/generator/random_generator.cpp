#include "random_generator.hpp"

#include <algorithm>
#include <utility>
#include <cassert>

// starting with all edges, shuffling them and then take m
Graph RandomGenerator::generate_graph(int n, int m, int k){
    assert(n >= 0 && m >= 0 && k > 0 && m <= n*(n-1)/2 && k <= n);
    std::vector<std::pair<int, int>> edges{};
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < i; ++j){
            edges.push_back(std::make_pair(i, j));
        }
    }
    shuffle(edges.begin(), edges.end(), this->gen);
    edges.resize(m);
    Graph result = divide_to_components(n, k);
    for(std::pair<int, int> & edge : edges){
        result.flip_edge(edge.first, edge.second);
    }
    return result;
}


Graph RandomGenerator::divide_to_components(int n, int k){
    std::vector<int> delimiters = random_ordered_vertices(n-1); //-1 because it is delimiter between n and n+1
    delimiters.resize(k-1); // -1
    std::sort(delimiters.begin(), delimiters.end());
    std::vector<std::vector<int>> splits = std::vector<std::vector<int>>(k);
    std::vector<int> vertices = random_ordered_vertices(n);
    int current_component = 0;
    for(int i = 0; i < n; ++i){
        splits[current_component].push_back(vertices[i]);
        if(current_component < k -1 && delimiters[current_component] == i){
            ++current_component;
        }
    }
    Graph graph(n, 0);
    for(const auto & split : splits){
        add_clique(graph, split);
    }
    return graph;
}

std::vector<int> RandomGenerator::random_ordered_vertices(int n){
    std::vector<int> vertices = std::vector<int>(n);
    for (int i = 0; i < n; ++i){
        vertices[i] = i;
    }
    shuffle(vertices.begin(), vertices.end(), this->gen);
    return vertices;
}

void RandomGenerator::add_clique(Graph & graph, std::vector<int> const & component) const {
    for(size_t i = 0; i < component.size(); i++){
        for(size_t j = 0; j < i; j++){
            graph.add_edge(component[i], component[j]);
        }
    }
}
