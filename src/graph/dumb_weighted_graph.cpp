#include "dumb_weighted_graph.hpp"
#include "weighted_graph.hpp"
#include <iomanip>
#include <algorithm>

using std::swap;
using std::count_if;

DumbWeightedGraph::DumbWeightedGraph(int N):mx_vertex_id(0){
    for(int i=0; i<N; ++i) add_vertex();
}

DumbWeightedGraph::DumbWeightedGraph(const vector<int> &vertices, const unordered_map<int,int> &edges, int mx)
    :vertices(vertices.begin(),vertices.end()), edges(edges), mx_vertex_id(mx) { }

DumbWeightedGraph::DumbWeightedGraph(const DumbWeightedGraph & db)
    :vertices(db.vertices.begin(),db.vertices.end()), edges(db.edges), mx_vertex_id(db.mx_vertex_id) { }

bool DumbWeightedGraph::contains_edge(int u, int v) const{
    return get_edge_weight(u, v) > 0;
}

int DumbWeightedGraph::get_edge_idx(int u, int v) const {
    if(u < v) return v * (v-1) / 2 + u;
    return u * (u-1) / 2 + v;
}

void DumbWeightedGraph::flip_edge(int u, int v){
    if(u>v)swap(u,v);
    edges[get_edge_idx(u,v)] = -edges[get_edge_idx(u,v)];
}

int DumbWeightedGraph::get_edge_weight(int u, int v) const {
    if(u>v)swap(u,v);
    const auto it = edges.find(get_edge_idx(u,v));
    return it->second;
}

void DumbWeightedGraph::set_edge_weight(int u, int v, int val){
    if(u>v)swap(u,v);
    edges[get_edge_idx(u,v)] = val;
}

int DumbWeightedGraph::get_edge_sign(int u, int v) const{
    int val = get_edge_weight(u, v);
    return (0 < val) - (val < 0);
}

vector<int> DumbWeightedGraph::all_vertices() const{
    return vector<int>(vertices.begin(),vertices.end());
}

int DumbWeightedGraph::size() const{
    return vertices.size();
}

int DumbWeightedGraph::add_vertex(){
    return add_vertex(mx_vertex_id);
}

int DumbWeightedGraph::add_vertex( int new_vertex_id ){
    vertices.insert(new_vertex_id);
    for(int n : vertices) if(n != new_vertex_id) set_edge_weight(new_vertex_id, n, -1);
    mx_vertex_id = std::max(mx_vertex_id, new_vertex_id) + 1;
    return new_vertex_id;
}

void DumbWeightedGraph::delete_vertex(int v){
    vertices.erase(v);
    for(int n:vertices){
        if(n==v)continue;
        int nn=n;
        int vv=v;
        if(nn>vv)swap(nn,vv);
        edges.erase(get_edge_idx(nn,vv));
    }
}

bool DumbWeightedGraph::vertex_exists(int v) const {
    return vertices.count(v);
}

vector<int> DumbWeightedGraph::neighbors(int u) const{
    vector<int> res;
    for(int n:vertices) if(n!=u){
        if(contains_edge(u, n)) res.push_back(n);
    }
    return res;
}

void DumbWeightedGraph::merge(int what, int into){
    for(int n:vertices){
        if(n==what||n==into)continue;
        int wu = get_edge_weight(what,n);
        int wv = get_edge_weight(into,n);
        int new_weight = wu+wv;
        if(wu==FORBIDDEN_WEIGHT) new_weight = FORBIDDEN_WEIGHT;
        if(wv==FORBIDDEN_WEIGHT) new_weight = FORBIDDEN_WEIGHT;
        set_edge_weight(into,n,new_weight);
    }
    delete_vertex(what);
}

WeightedGraph* DumbWeightedGraph::copy() const{
    return new DumbWeightedGraph(*this);
}

WeightedGraph* DumbWeightedGraph::new_empty_graph() const{
    return new DumbWeightedGraph();
}

void DumbWeightedGraph::print(ostream &os) const{
    os << vertices.size() << std::endl;
    for(int i : vertices){
        os << i << ":";
        for(int j : vertices){
            if(i!=j)os << std::setw(3) << get_edge_weight(i, j);
            else os << std::setw(3) << "x";
        }
        os << std::endl;
    }
}
