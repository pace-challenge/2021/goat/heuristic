#include "graph.hpp"
#include <cassert>

#pragma GCC diagnostic ignored "-Wunused-parameter"
Graph::Graph( int n, int m )
    : n( n ), m( 0 ), amatrix( n , vector<bool>( n , false ) ), alist( n , list<int>() ) {}

size_t Graph::get_vertices_count ( void ) const {
    return this->n;
}

size_t Graph::get_edges_count ( void ) const {
    return this->m;
}

bool Graph::contains_edge ( int u, int v ) const {
    return this->amatrix[ u ][ v ] == true;
}

const list<int> & Graph::get_adjacency_list ( int v ) const {
    assert(0 <= v);
    assert(v < (int)n);
    return alist[v];
}

void Graph::add_edge ( int u, int v ) {
    if ( this->amatrix[ u ][ v ] == false ){
        this->amatrix[ u ][ v ] = this->amatrix[ v ][ u ] = true;
        this->alist[ u ].push_back( v );
        this->alist[ v ].push_back( u );
        ++m;
    }
}

void Graph::remove_edge ( int u, int v ) {
    if ( this->amatrix[ u ][ v ] == true ){
        this->amatrix[ u ][ v ] = this->amatrix[ v ][ u ] = false;
        this->alist[ u ].remove( v ); // potentially slow
        this->alist[ v ].remove( u ); // potentially slow
        --m;
    }
}

void Graph::flip_edge( int u, int v) {
    if( this->contains_edge(u, v) ){
        this->remove_edge(u, v);
    } else {
        this->add_edge(u, v);
    }
}

void Graph::print(ostream & out) const{
    out << "p cep: " << this->n << " " << this->m << std::endl;
    for( size_t i = 0; i < this->n; ++i ){
        for( size_t j : this->alist[i] ){
            if(i < j){
                out << i+1 << " " << j+1 << std::endl;
            }
        }
    }
}
