#pragma once

#include "weighted_graph.hpp"

#include <unordered_map>
#include <iostream>
#include <map>
#include <set>
#include <vector>
using std::map;
using std::unordered_map;
using std::ostream;
using std::set;
using std::vector;
using std::pair;


/*!
 * Really dumb weighted graph implementation.
 *
 * Uses set for vertices, and map<lower,higher> for edge weights. All operations should be quite trivial to implement, but slow.
 */
class DumbWeightedGraph : public WeightedGraph {

public:

    DumbWeightedGraph(int N=0);

    DumbWeightedGraph(const vector<int> &vertices, const unordered_map<int,int> &edges, int mx);

    DumbWeightedGraph(const DumbWeightedGraph &db);

    virtual bool contains_edge(int u, int v) const;

    virtual void flip_edge(int u, int v);

    virtual int get_edge_weight(int u, int v) const;

    virtual void set_edge_weight(int u, int v, int val);

    virtual int get_edge_sign(int u, int v) const;

    virtual vector<int> all_vertices() const;

    virtual int size() const;

    virtual int add_vertex();

    virtual int add_vertex( int new_vertex_id );

    virtual void delete_vertex(int v);

    virtual bool vertex_exists( int v ) const;

    virtual vector<int> neighbors(int u) const;

    virtual void merge(int what, int into);

    virtual WeightedGraph* copy() const;

    WeightedGraph* new_empty_graph() const;

    virtual void print(ostream &os) const;

    virtual std::string to_json() const {return "{}";};


private:
    int get_edge_idx(int u, int v) const;

    set<int> vertices;
    unordered_map<int,int> edges;
    int mx_vertex_id;
};
