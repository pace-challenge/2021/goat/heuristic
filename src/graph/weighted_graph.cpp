#include "weighted_graph.hpp"

#include <set>
#include <cassert>
#include <algorithm>

/*!
 * returns true if the edge u,v has positive weight, false otherwise
 */
bool WeightedGraph::contains_edge(int u, int v) const {
    return get_edge_weight(u, v) > 0;
}

/////////////////////////////////////////////////////////////////////////

/*!
 * sets the weight of edge u,v to forbidden
 */
void WeightedGraph::unsafe_set_edge_weight(int u, int v, int val) {
    set_edge_weight(u, v, val);

}

/*!
 * sets the weight of edge u,v to forbidden
 */
void WeightedGraph::set_edge_forbidden(int u, int v) {
    set_edge_weight(u, v, FORBIDDEN_WEIGHT);
}

/*!
 * flips the sign of the weight of the edge u,v
 */
void WeightedGraph::flip_edge(int u, int v) {
    set_edge_weight(u, v, -get_edge_weight(u, v));
}

/////////////////////////////////////////////////////////////////////////

/*!
 * gets the sign of the weight of the edge u,v
 * returns +1 if the weight is positive, -1 if the weight is negative, 0 if the weight is 0
 */
int WeightedGraph::get_edge_sign(int u, int v) const {
    int val = get_edge_weight(u, v);
    return (0 < val) - (val < 0);
}

/////////////////////////////////////////////////////////////////////////

/*!
 * adds a new vertex v to the graph such that v is not already in the graph
 * for each u in the graph, sets edge u,v to -1
 * returns v
 */
int WeightedGraph::add_vertex() {
    const auto & vs = all_vertices();
    if(!vs.size()) return add_vertex(0);
    else {
        int mv=vs[0];
        for(int v : vs) {
            mv = std::max(mv, v);
        }
        return add_vertex(mv+1);
    }
}

/////////////////////////////////////////////////////////////////////////

/*!
 * returns the actual number of vertices in the graph
 */
int WeightedGraph::size() const {
    return all_vertices().size();
}

/////////////////////////////////////////////////////////////////////////

/*!
 * returns the set N of vertices such that for each v in N, we have that the weight of u,v edge is positive
 */
vector<int> WeightedGraph::neighbors( int u ) const {
    vector<int> N;
    for(int v : all_vertices()) {
        if(u==v) continue;
        if(get_edge_weight(u, v) > 0) N.push_back(v);
    }

    return N;
}

vector<int> WeightedGraph::neighbors( int u, bool closed, const vector<int> & excluded ) const {
    vector<int> N;
    for(int v : all_vertices()) {
        if(u==v) continue;
        if(get_edge_weight(u, v) > 0) N.push_back(v);
    }

    if ( closed == true ) {
        N.emplace_back( u );
    }

    if ( excluded.size() > 0 ) {
        std::sort( N.begin(), N.end() );
        vector<int> res;
        std::set_difference( N.begin(), N.end(), excluded.begin(), excluded.end(),
                       std::back_inserter( res ) );
        return res;
    }
    return N;
}

/////////////////////////////////////////////////////////////////////////

/*!
 * returns the degree of u
 */
int WeightedGraph::degree(int u) const {
    return neighbors(u).size();
}

/////////////////////////////////////////////////////////////////////////

WeightedGraph* WeightedGraph::induced_subgraph(const vector<int> & chosen_vertices) const{
    WeightedGraph *induced_subgraph = new_empty_graph();
    auto vs = all_vertices();

    // this graph must contain the list of chosen_vertices
    std::set<int> vs_set(vs.begin(), vs.end());
    for(int u : chosen_vertices) assert(vs_set.count(u));

    for(int u : chosen_vertices) induced_subgraph->add_vertex(u);
    for(int u : chosen_vertices) for(int v : chosen_vertices) if(u < v){
        induced_subgraph->set_edge_weight(u, v, this->get_edge_weight(u, v));
    }
    return induced_subgraph;
}

/////////////////////////////////////////////////////////////////////////

bool WeightedGraph::equals(const WeightedGraph * graph) const{
    if(size() != graph->size()) return false;
    int N=size();
    vector<int> va = all_vertices();
    vector<int> vb = graph->all_vertices();
    for(int i=0; i<N; ++i) if(va[i] != vb[i]) return false;
    for(int i=0; i<N; ++i){
        for(int j=i+1; j<N; ++j){
            int ea = get_edge_weight(va[i], va[j]);
            int eb = graph->get_edge_weight(va[i], va[j]);
            if(ea != eb) return false;
        }
    }
    return true;
}

