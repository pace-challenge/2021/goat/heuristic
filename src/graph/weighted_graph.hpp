#pragma once

#include <iostream>
#include <vector>
using std::vector;
using std::ostream;

const int INFINITY = 1<<24;
const int FORBIDDEN_WEIGHT = -INFINITY;
const int PERMANENT_WEIGHT = INFINITY;

/*!
 * Graph representation with weighted edges.
 *
 * All edges are always 'present', however actual edges have always positive weight -- this is considered when searching for neighbors, counting degree, etc. Changes to this graph DOESN'T change indices of represented vertices -- these always stay valid, unless you delete or merge them.
 */
class WeightedGraph {

public:
    virtual ~WeightedGraph(){};

    /*!
     * returns the weight of edge u,v
     */
    virtual int get_edge_weight(int u, int v) const = 0;

    /*!
     * sets the weight of edge u,v to val
     */
    virtual void set_edge_weight(int u, int v, int val) = 0;

    /*!
     * helper - directly sets the weight of edge u,v
     */
    virtual void unsafe_set_edge_weight(int u, int v, int val);

    /*!
     * sets the weight of edge u,v to forbidden
     */
    virtual void set_edge_forbidden(int u, int v);

    /*!
     * returns true if the edge u,v has positive weight, false otherwise
     */
    virtual bool contains_edge(int u, int v) const;

    /*!
     * flips the sign of the weight of the edge u,v
     */
    virtual void flip_edge(int u, int v);

    /*!
     * gets the sign of the weight of the edge u,v
     * returns +1 if the weight is positive, -1 if the weight is negative, 0 if the weight is 0
     */
    virtual int get_edge_sign(int u, int v) const;

    /*!
     * add v to the graph if it does not exist, otherwise does nothing
     * if v was added, for each u in the graph, sets edge u,v to -1
     * returns v
     */
    virtual int add_vertex( int v ) = 0;

    /*!
     * adds a new vertex v to the graph such that v is not already in the graph
     * for each u in the graph, sets edge u,v to -1
     * returns v
     */
    virtual int add_vertex();

    /*!
     * deletes vertex v from the graph if it exists, otherwise does nothing
     */
    virtual void delete_vertex(int v) = 0;

	/*!
	 * Checks whether there is a vertex with specified id.
	 */
    virtual bool vertex_exists(int v) const = 0;

	/*!
     * Merges v_what vertex into v_into vertex and v_what vertex is then removed.
     */
    virtual void merge(int v_what, int v_into) = 0;

    /*!
     * returns the actual set of vertices of the graph
     * the vertices may not be numbered from 0 to size()-1, but the vertices must be in ascending order
     */
    virtual vector<int> all_vertices() const = 0;

    /*!
     * returns the actual number of vertices in the graph
     */
    virtual int size() const;

    /*!
     * returns the set N of vertices such that for each v in N, we have that the weight of u,v edge is positive
	 *
	 * When `closed` is set to `true`, even a vertex `u` is part of the neighborhood.
	 * Vector of excluded vertices must be sorted!
     */
    virtual vector<int> neighbors(int u) const;
    virtual vector<int> neighbors(int u, bool closed, const vector<int> & excluded) const;

    /*!
     * returns the degree of u
     */
    virtual int degree(int u) const;

    /*!
     * returns induced subgraph on chosen_vertices
     * the resulting graph must have the same set of vertices as chosen_vertices
     * chosen_vertices must a subset of the vertices in the graph
     */
    virtual WeightedGraph* induced_subgraph(const vector<int> & chosen_vertices) const;

    /*!
     * returns the copy of this graph
     */
    virtual WeightedGraph* copy() const = 0;

    /*!
     * returns new empty graph
     */
    virtual WeightedGraph* new_empty_graph() const = 0;

    /*!
     * prints human readable representation
     */
    virtual void print(ostream &os) const = 0;

    friend ostream& operator<<(ostream &os, const WeightedGraph *w){
        w->print(os);
        return os;
    }

    /*!
     * checks if the representation of this graph is the same as g
     */
    virtual bool equals(const WeightedGraph* g) const;

    /*!
     * returns machine readable representation in one line of json
     */
    virtual std::string to_json() const = 0;


    virtual void recompute_structures() {}
    virtual void free_structures() {}


    // virtual int get_lower_bound() const {
    //     return 0;
    // }
    // virtual int get_masked_lower_bound(int u, int v) const {
    //     return 0;
    // }

    virtual int get_icp(int u, int v) const {
        (void)u;
        (void)v;
        throw 666;
    }
    virtual int get_icf(int u, int v) const {
        (void)u;
        (void)v;
        throw 42;
    }

};
