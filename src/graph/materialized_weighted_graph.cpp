#include "materialized_weighted_graph.hpp"
#include <cassert>
#include <iomanip>


int MaterializedWeightedGraph::get_edge_idx(int u, int v) const {
    if(u < v) return v * (v-1) / 2 + u;
    return u * (u-1) / 2 + v;
}

/////////////////////////////////////////////////////////////////////////

int MaterializedWeightedGraph::get_edge_weight(int u, int v) const {
    assert(vertex_exists(u));
    assert(vertex_exists(v));
    assert(u!=v);
    return edge_weights[get_edge_idx(u, v)];
}

/////////////////////////////////////////////////////////////////////////

void MaterializedWeightedGraph::set_edge_weight(int u, int v, int val) {
    assert(0);
    assert(vertex_exists(u));
    assert(vertex_exists(v));
    assert(u!=v);
    edge_weights[get_edge_idx(u, v)] = val;
}

void MaterializedWeightedGraph::unsafe_set_edge_weight(int u, int v, int val) {
    assert(vertex_exists(u));
    assert(vertex_exists(v));
    assert(u!=v);

    structures_valid = false;
    // std::cerr<<"unsafe_set_edge_weight"<<std::endl;

    edge_weights[get_edge_idx(u, v)] = val;
}

void MaterializedWeightedGraph::set_edge_forbidden(int u, int v) {
    // lb_structures->update_lower_bound_edge_forbidden(u, v, *this);
    ic_structures->update_edge_forbidden_before(u, v, *this);
    edge_weights[get_edge_idx(u, v)] = FORBIDDEN_WEIGHT;
}


/////////////////////////////////////////////////////////////////////////

vector<int> MaterializedWeightedGraph::all_vertices() const {
    vector<int> vs;
    for(int i = 0; i < n; ++i) {
        if(vertices_validity_mask[i]) vs.push_back(i);
    }
    return vs;
}

/////////////////////////////////////////////////////////////////////////

int MaterializedWeightedGraph::size() const {
    return _size;
}

/////////////////////////////////////////////////////////////////////////

MaterializedWeightedGraph::MaterializedWeightedGraph(const MaterializedWeightedGraph &wg) :
    n(wg.n),
    _size(wg._size),
    edge_weights(wg.edge_weights),
    vertices_validity_mask(wg.vertices_validity_mask),
    structures_valid(wg.structures_valid),
    // lb_structures(wg.lb_structures == nullptr ? nullptr : new GreedyDisjointConflictTriplesLowerBound(*wg.lb_structures)),
    ic_structures(wg.ic_structures == nullptr ? nullptr : new InducedCosts(*wg.ic_structures))
    {}

/////////////////////////////////////////////////////////////////////////

WeightedGraph* MaterializedWeightedGraph::copy() const {
    return new MaterializedWeightedGraph(*this);
}

/////////////////////////////////////////////////////////////////////////

MaterializedWeightedGraph::MaterializedWeightedGraph(int n, int val)
    : n(n), _size(n), edge_weights(n*(n-1)/2, val), vertices_validity_mask(n, true),
    structures_valid(false), ic_structures(nullptr) {}

MaterializedWeightedGraph::MaterializedWeightedGraph(int n)
    : MaterializedWeightedGraph(n, -1) {}

/////////////////////////////////////////////////////////////////////////

void MaterializedWeightedGraph::extend_representation()  {
    // add n edges
    for(int i = 0; i < n; ++i) edge_weights.push_back(-1);
    // then increment n and vertices_validity_mask
    n++;
    vertices_validity_mask.push_back(false);
}

/////////////////////////////////////////////////////////////////////////

int MaterializedWeightedGraph::add_vertex(int v) {
    assert(v>=0);
    while(v >= n) extend_representation();
    if(vertices_validity_mask[v] == true) return v;

    vertices_validity_mask[v] = true;
    _size++;
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        if(u == v) continue;
        set_edge_weight(u, v, -1);
    }

    structures_valid = false;
    // std::cerr<<"add_vertex"<<std::endl;

    return v;
}

/////////////////////////////////////////////////////////////////////////

void MaterializedWeightedGraph::delete_vertex(int v) {
    // std::cerr<<"delete_vertex"<<std::endl;
    structures_valid = false;
    _delete_vertex(v);
}

void MaterializedWeightedGraph::_delete_vertex(int v) {
    if(vertices_validity_mask[v] == false) return;
    vertices_validity_mask[v] = false;
    _size--;
}

/////////////////////////////////////////////////////////////////////////

bool MaterializedWeightedGraph::vertex_exists(int u) const {
    if(u<0 || u>=n) return false;
    return vertices_validity_mask[u];
}

/////////////////////////////////////////////////////////////////////////

WeightedGraph* MaterializedWeightedGraph::new_empty_graph() const {
    return new MaterializedWeightedGraph(0, -1);
}

/////////////////////////////////////////////////////////////////////////

void MaterializedWeightedGraph::merge(int what, int into) {
    assert(vertex_exists(what));
    assert(vertex_exists(into));
    assert(what != into);

    // lb_structures->update_lower_bound_edge_merge_u_into_v(what, into, *this);
    ic_structures->update_edge_merge_u_to_v_before(what, into, *this);

    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        if(u == what) continue;
        if(u == into) continue;

        int what_u = get_edge_weight(what, u);
        int into_u = get_edge_weight(into, u);
        int new_weight = what_u + into_u;

        // does not work with PERMANENT weights
        assert(what_u != PERMANENT_WEIGHT);
        assert(into_u != PERMANENT_WEIGHT);
        if(what_u == FORBIDDEN_WEIGHT) new_weight = FORBIDDEN_WEIGHT;
        if(into_u == FORBIDDEN_WEIGHT) new_weight = FORBIDDEN_WEIGHT;
        edge_weights[get_edge_idx(into, u)] = new_weight;
    }
    _delete_vertex(what);

    ic_structures->update_edge_merge_u_to_v_after(what, into, *this);
}


/////////////////////////////////////////////////////////////////////////

void MaterializedWeightedGraph::print(ostream &os) const {
    os << "vertices size: " << _size << std::endl;
    os << "vertices: ";
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        os << " " << u;
    }
    os << std::endl;

    os << "edges: \n";
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        for(int v = 0; v < u; ++v) {
            if(!vertices_validity_mask[v]) continue;
            os << std::setw(3) << u << "," << v << " -> " << get_edge_weight(u, v) << std::endl;
        }
    }
}

/////////////////////////////////////////////////////////////////////////

std::string MaterializedWeightedGraph::to_json() const {
    std::string j_vertices = "";
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;

        if(j_vertices.size()) j_vertices+=",";
        j_vertices += std::to_string(u);
    }
    j_vertices = "[" + j_vertices + "]";

    std::string j_edges = "";
    for(int u = 0; u < n; ++u) {
        if(!vertices_validity_mask[u]) continue;
        for(int v = 0; v < u; ++v) {
            if(!vertices_validity_mask[v]) continue;

            if(j_edges.size()) j_edges+=",";
            j_edges += "[" + std::to_string(u) + "," + std::to_string(v) + "," + std::to_string(get_edge_weight(u, v)) + "]";
        }
    }
    j_edges = "[" + j_edges + "]";

    std::string j = std::string("{") + "\"vertices\":" + j_vertices + "," + "\"edges\":" + j_edges + "}";
    return j;
}

WeightedGraph* MaterializedWeightedGraph::induced_subgraph(const vector<int> & chosen_vertices) const {
    // std::cerr<<"induced_subgraph"<<std::endl;

    MaterializedWeightedGraph * g_is = (MaterializedWeightedGraph*)copy();
    std::set<int> to_delete;
    for(int v : all_vertices()) {to_delete.insert(v);}
    for(int v : chosen_vertices) {to_delete.erase(v);}
    for(int v : to_delete) g_is->delete_vertex(v);
    g_is->recompute_structures();
    return g_is;
}



void MaterializedWeightedGraph::recompute_structures() {
    // delete lb_structures;
    // lb_structures = new GreedyDisjointConflictTriplesLowerBound(*this);

    delete ic_structures;
    ic_structures = new InducedCosts(*this);

    structures_valid = true;
    // std::cerr<<"recompute_structures"<<std::endl;
}

// int MaterializedWeightedGraph::get_lower_bound() const {
//     assert(structures_valid);
//     return lb_structures->get_lower_bound();
// }

// int MaterializedWeightedGraph::get_masked_lower_bound(int u, int v) const {
//     assert(structures_valid);
//     return lb_structures->get_masked_lower_bound(u, v);
// }

void MaterializedWeightedGraph::free_structures() {
    delete ic_structures;
    ic_structures = nullptr;
    structures_valid = false;
}

int MaterializedWeightedGraph::get_icp(int u, int v) const {
    assert(structures_valid);
    return ic_structures->get_icp(u, v);
}

int MaterializedWeightedGraph::get_icf(int u, int v) const {
    assert(structures_valid);
    return ic_structures->get_icf(u, v);
}

MaterializedWeightedGraph::~MaterializedWeightedGraph() {
    // delete lb_structures;
    delete ic_structures;
}
