#include "../special_cases.hpp"

#include "../graph/dumb_weighted_graph.hpp"
#include "tester.hpp"

#include <iostream>

using namespace std;

int main(void){
    Tester t;
    t.check("calculating cost for path on 5 vertices");
    WeightedGraph * path5 = new DumbWeightedGraph(5);
    path5->set_edge_weight(0,1, 1);
    path5->set_edge_weight(1,2, 1);
    path5->set_edge_weight(2,3, 1);
    path5->set_edge_weight(3,4, 1);


    t.is_true(calculate_cluster_cost(*path5, {0}) == 0);
    t.is_true(calculate_cluster_cost(*path5, {0, 1}) == 0);
    t.is_true(calculate_cluster_cost(*path5, {2, 1}) == 0);
    t.is_true(calculate_cluster_cost(*path5, {0, 1, 2}) == 1);
    t.is_true(calculate_cluster_cost(*path5, {1, 2, 3}) == 1);
    t.is_true(calculate_cluster_cost(*path5, {0, 1, 2, 3}) == 3);
    t.is_true(calculate_cluster_cost(*path5, {0, 1, 2, 3, 4}) == 6);

    t.is_true(get_path(*path5).size() == 5);
    t.is_true(solve_path(*path5).first == 2);
    t.ok();

    delete path5;

    t.check("calculating cost for path on 3 vertices");
    WeightedGraph * path3 = new DumbWeightedGraph(3);

    path3->set_edge_weight(0,1, 3);
    path3->set_edge_weight(1,2, 4);
    path3->set_edge_weight(0,2, -9);

    t.is_true(calculate_cluster_cost(*path3, {0}) == 0);
    t.is_true(calculate_cluster_cost(*path3, {1}) == 0);
    t.is_true(calculate_cluster_cost(*path3, {0, 1}) == 0);
    t.is_true(calculate_cluster_cost(*path3, {2, 1}) == 0);
    t.is_true(calculate_cluster_cost(*path3, {0, 1, 2}) == 9);


    t.is_true(get_path(*path3).size() == 3);
    pair<int, CliqueSolution*> path3_clique = solve_path(*path3);
    t.is_true(path3_clique.first == 3);
    Solution *path3_solution = path3_clique.second->get_solution(path3);
    t.is_true(path3_solution->size() == 1);
    t.is_true(path3_solution->weight() == 3);
    t.is_true(path3_solution->isInSolution(0,1));
    delete path3_solution;
    t.ok();
    
    delete path3;

    t.check("calculating cost for path on 4 vertices");
    WeightedGraph * path4 = new DumbWeightedGraph(4);
    // 1 -> 3 -> 0 -> 2 
    path4->set_edge_weight(1,3, 2);
    path4->set_edge_weight(3,0, 5);
    path4->set_edge_weight(0,2, 7);
    path4->set_edge_weight(0,1, -3);
    path4->set_edge_weight(1,2, -4);
    path4->set_edge_weight(2,3, -1);


    t.is_true(calculate_cluster_cost(*path4, {0, 1, 3}) == 3);
    t.is_true(calculate_cluster_cost(*path4, {0, 1, 2,  3}) == 8);
    t.is_true(calculate_cluster_cost(*path4, {0, 2, 3}) == 1);

    t.is_true(get_path(*path4).size() == 4);

    pair<int, CliqueSolution*> path4_clique = solve_path(*path4);
    t.is_true(path4_clique.first == 3);
    Solution *path4_solution = path4_clique.second->get_solution(path4);
    t.is_true(path4_solution->size() == 2);
    t.is_true(path4_solution->weight() == 3);
    t.is_true(path4_solution->isInSolution(1,3));
    t.is_true(path4_solution->isInSolution(2,3));
    delete path4_solution;
    t.ok();

    delete path4;

    t.check("calculating cost for path on 4 vertices");
    WeightedGraph * path4_2 = new DumbWeightedGraph(4);
    // 0 -> 2 -> 3 -> 1
    path4_2->set_edge_weight(0,2, 9);
    path4_2->set_edge_weight(2,3, 4);
    path4_2->set_edge_weight(3,1, 7);
    path4_2->set_edge_weight(0,1, -5);
    path4_2->set_edge_weight(1,2, -6);
    path4_2->set_edge_weight(0,3, -8);


    t.is_true(calculate_cluster_cost(*path4_2, {0, 2, 3}) == 8);
    t.is_true(calculate_cluster_cost(*path4_2, {0, 1, 2,  3}) == 19);
    t.is_true(calculate_cluster_cost(*path4_2, {1, 2, 3}) == 6);

    t.is_true(get_path(*path4_2).size() == 4);

    pair<int, CliqueSolution*> path4_2_clique = solve_path(*path4_2);
    t.is_true(path4_2_clique.first == 4);
    Solution *path4_2_solution = path4_2_clique.second->get_solution(path4_2);
    t.is_true(path4_2_solution->size() == 1);
    t.is_true(path4_2_solution->weight() == 4);
    t.is_true(path4_2_solution->isInSolution(2,3));
    delete path4_2_solution;
    t.ok();

    delete path4_2;

    t.check("calculating cost for cycle on 4 vertices");
    WeightedGraph * cycle4 = new DumbWeightedGraph(4);
    cycle4->set_edge_weight(0,1, 1);
    cycle4->set_edge_weight(1,2, 1);
    cycle4->set_edge_weight(2,3, 1);
    cycle4->set_edge_weight(3,0, 1);

    pair<int, CliqueSolution*> cycle4_clique = solve_cycle(*cycle4);
    t.is_true(cycle4_clique.first == 2);
    t.ok();

    delete cycle4;

    t.check("calculating cost for another cycle on 4 vertices");
    WeightedGraph * cycle42 = new DumbWeightedGraph(4);
    // cycle: 0 -> 2 -> 3 -> 1 -> 0
    cycle42->set_edge_weight(0,2, 6);
    cycle42->set_edge_weight(2,3, 3);
    cycle42->set_edge_weight(3,1, 7);
    cycle42->set_edge_weight(1,0, 2);
    cycle42->set_edge_weight(1,2, -10);
    cycle42->set_edge_weight(3,0, -8);

    pair<int, CliqueSolution*> cycle42_clique = solve_cycle(*cycle42);
    t.is_true(cycle42_clique.first == 5);
    Solution *cycle42_solution = cycle42_clique.second->get_solution(cycle42);
    t.is_true(cycle42_solution->size() == 2);
    t.is_true(cycle42_solution->weight() == 5);
    t.is_true(cycle42_solution->contains_edge(2,3));
    t.is_true(cycle42_solution->contains_edge(1,0));
    delete cycle42_solution;
    t.ok();

    delete cycle42;

    t.check("calculating cost for cycle on 5 vertices");
    WeightedGraph * cycle5 = new DumbWeightedGraph(5);
    // cycle: 0 -> 2 -> 1 -> 4 -> 3 -> 0 
    cycle5->set_edge_weight(0,2, 8);
    cycle5->set_edge_weight(2,1, 3);
    cycle5->set_edge_weight(1,4, 11);
    cycle5->set_edge_weight(4,3, 2);
    cycle5->set_edge_weight(3,0, 16);
    cycle5->set_edge_weight(0,1, -8);
    cycle5->set_edge_weight(0,4, -12);
    cycle5->set_edge_weight(2,4, -9);
    cycle5->set_edge_weight(2,3, -4);
    cycle5->set_edge_weight(1,3, -7);

    pair<int, CliqueSolution*> cycle5_clique = solve_cycle(*cycle5);
    t.is_true(cycle5_clique.first == 9);
    Solution *cycle5_solution = cycle5_clique.second->get_solution(cycle5);
    t.is_true(cycle5_solution->size() == 3);
    t.is_true(cycle5_solution->weight() == 9);
    t.is_true(cycle5_solution->contains_edge(2,1));
    t.is_true(cycle5_solution->contains_edge(3,4));
    t.is_true(cycle5_solution->contains_edge(2,3));
    delete cycle5_solution;
    t.ok();

    delete cycle5;

    return 0;
}
