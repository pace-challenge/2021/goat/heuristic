#include <cassert>

#include "../generator/metrics.hpp"
#include "tester.hpp"

int main(void){
    Tester t;
    t.check("connectivity");
    Graph cherry(3, 0);
    cherry.add_edge(0,1);
    cherry.add_edge(0,2);
    t.is_true(calculate_edge_connectivity(cherry) == 1);

    Graph butterfly(5, 0);
    butterfly.add_edge(0, 1);
    butterfly.add_edge(0, 2);
    butterfly.add_edge(0, 3);
    butterfly.add_edge(0, 4);
    butterfly.add_edge(1, 2);
    butterfly.add_edge(3, 4);
    t.is_true(calculate_edge_connectivity(butterfly) == 2);


    Graph c4(4, 0);
    c4.add_edge(0, 1);
    c4.add_edge(1, 2);
    c4.add_edge(2, 3);
    c4.add_edge(3, 0);
    t.is_true(calculate_edge_connectivity(c4) == 2);

    Graph c3_plus_k1(4, 0);
    c3_plus_k1.add_edge(0, 1);
    c3_plus_k1.add_edge(1, 2);
    c3_plus_k1.add_edge(2, 0);
    t.is_true(calculate_edge_connectivity(c3_plus_k1) == 0);

    Graph k5(5, 0);
    k5.add_edge(0, 1);
    k5.add_edge(0, 2);
    k5.add_edge(0, 3);
    k5.add_edge(0, 4);
    k5.add_edge(1, 2);
    k5.add_edge(1, 3);
    k5.add_edge(1, 4);
    k5.add_edge(2, 3);
    k5.add_edge(2, 4);
    k5.add_edge(3, 4);
    t.is_true(calculate_edge_connectivity(k5) == 4);

    t.ok();

    t.check("degrees");
    Graph nice_degrees(4, 0);
    nice_degrees.add_edge(0, 1);
    nice_degrees.add_edge(1, 2);
    nice_degrees.add_edge(1, 3);
    nice_degrees.add_edge(2, 3);
    const auto [min, max, median] = calculate_min_max_median_degree(nice_degrees);
    t.is_true(min == 1);
    t.is_true(max == 3);
    t.is_true(median == 2);
    t.ok();
}
