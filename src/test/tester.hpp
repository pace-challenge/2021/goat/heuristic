#pragma once

#include <cassert>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

class Tester{
    public:
        Tester():checking(false){}

        void check(string s){
            if(checking){
                cout << "cannot check two things at once\n";
                exit(1);
            }
            cout << "Testing " << s << " ";
            checking = true;
        }

        void ok(){
            cout << ' ' << "\033[32m" << "OK" << "\033[0m" << endl;
            checking = false;
        }

#define is_true(q) check_true(#q,(q))
        void check_true(string s, bool val){
            if(!val){
                cout << ' ';
                cout << "\033[31m";
                cout << "test '" << s << "' failed!" << endl;
                cout << "\033[0m";
                exit(1);
            }else{
                cout << '.';
                cout.flush();
            }
        }

    private:
        bool checking;
};
