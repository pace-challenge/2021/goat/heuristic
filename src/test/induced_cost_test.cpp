#include <iostream>

#include "../utils/induced_cost.hpp"
#include "../graph/graph.hpp"
#include "../graph/dumb_weighted_graph.hpp"
#include "tester.hpp"



using namespace std;


int main(void){
    Tester t;
    t.check("induced cost cherry");
    WeightedGraph * cherry = new DumbWeightedGraph(3);
    cherry->set_edge_weight(0,1, 1);
    cherry->set_edge_weight(0,2, 2);
    cherry->set_edge_weight(1,2, -2);

    t.is_true(induced_cost_permanent(cherry, 0,1) == 2);
    t.is_true(induced_cost_permanent(cherry, 0,2) == 1);
    t.is_true(induced_cost_permanent(cherry, 1,2) == 2);
    t.is_true(induced_cost_forbidden(cherry, 0,1) == 1);
    t.is_true(induced_cost_forbidden(cherry, 0,2) == 2);
    t.is_true(induced_cost_forbidden(cherry, 1,2) == 1);

    
    t.is_true(induced_cost_permanent_zero_edges_count(cherry, 0,1).second == 1);
    t.is_true(induced_cost_permanent_zero_edges_count(cherry, 0,2).second == 0);
    t.is_true(induced_cost_permanent_zero_edges_count(cherry, 1,2).second == 0);
    delete cherry;
    t.ok();

    t.check("induced cost k4-e");
    WeightedGraph * k4e = new DumbWeightedGraph(4);
    k4e->set_edge_weight(0,1, 1);
    k4e->set_edge_weight(0,2, 2);
    k4e->set_edge_weight(0,3, 3);
    k4e->set_edge_weight(1,2, 2);
    k4e->set_edge_weight(1,3, 1);
    k4e->set_edge_weight(2,3, -2);

    t.is_true(induced_cost_permanent(k4e, 0,1) == 0);
    t.is_true(induced_cost_permanent(k4e, 0,2) == 2);
    t.is_true(induced_cost_permanent(k4e, 0,3) == 2);
    t.is_true(induced_cost_permanent(k4e, 1,2) == 1);
    t.is_true(induced_cost_permanent(k4e, 1,3) == 2);
    t.is_true(induced_cost_permanent(k4e, 2,3) == 2);
    

    t.is_true(induced_cost_forbidden(k4e, 0,1) == 2+1+1);
    t.is_true(induced_cost_forbidden(k4e, 0,2) == 1+2);
    t.is_true(induced_cost_forbidden(k4e, 0,3) == 1+3);
    t.is_true(induced_cost_forbidden(k4e, 1,2) == 1+2);
    t.is_true(induced_cost_forbidden(k4e, 1,3) == 1+1);
    t.is_true(induced_cost_forbidden(k4e, 2,3) == 2+1);


    t.is_true(induced_cost_permanent_zero_edges_count(k4e, 1,3).second == 1);
    delete k4e;
    t.ok();

    t.check("induced cost of k5 minus some edges");
    WeightedGraph * k5 = new DumbWeightedGraph(5);
    k5->set_edge_weight(0,1, 1);
    k5->set_edge_weight(0,2, -1);
    k5->set_edge_weight(0,3, 0);
    k5->set_edge_weight(0,4, 0);
    k5->set_edge_weight(1,2, -1);
    k5->set_edge_weight(1,3, 1);
    k5->set_edge_weight(1,4, 1);
    k5->set_edge_weight(2,3, 0);
    k5->set_edge_weight(2,4, 2);
    k5->set_edge_weight(3,4, -2);


    t.is_true(induced_cost_permanent_zero_edges_count(k5, 0,1).second == -2);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 0,2).second == -1);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 0,3).second == -3);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 0,4).second == -2);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 1,2).second == 0);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 1,3).second == -2);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 1,4).second == -1);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 2,3).second ==  0);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 2,4).second ==  -1);
    t.is_true(induced_cost_permanent_zero_edges_count(k5, 3,4).second ==  -2);
    delete k5;
    t.ok();

    return 0;
}