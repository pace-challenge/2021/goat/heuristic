#include <iostream>

#include "../node/edge_branch.hpp"
#include "../graph/graph.hpp"
#include "../graph/dumb_weighted_graph.hpp"
#include "../graph/materialized_weighted_graph.hpp"
#include "../graph/conversion.hpp"

#include "../node/cherry.hpp"
#include "tester.hpp"

using namespace std;

WeightedGraph * to_weighted(Graph &g){
    auto graphFactory = [](int SZ){ return new MaterializedWeightedGraph(SZ); };
    auto res = convert_graph_to_weighted_graph(g, graphFactory);
    res->recompute_structures();
    return res;
}

void test_branching_number_calculation(){    
    
    assert(compute_branching_number(1,1) >= 2);
    assert(compute_branching_number(1,1) <= 2.01);
    
    assert(compute_branching_number(1,3) >= 1.46);
    assert(compute_branching_number(1,3) <= 1.48);
}

int main(void){
    Tester t;
    t.check("Evaluation of star");

    Graph star3 = Graph(4,0);
    star3.add_edge(0,1);
    star3.add_edge(2,1);
    star3.add_edge(3,1);
    WeightedGraph * wstar3 = to_weighted(star3);

    EdgeBranchNode node = EdgeBranchNode(wstar3, Bound(2,2));
    EvalResult res = node.evaluate();
    vector<Node *> childs = res.new_nodes;
    t.is_true(childs.size() == 2);
    t.is_true(childs[0]->evaluate().result_state == EvalState::SUCCESS);
    delete wstar3;

    t.ok();


    t.check("Evaluation of k4-e");
    WeightedGraph * wg = new DumbWeightedGraph(4);
    wg->set_edge_weight(0,1,2);
    wg->set_edge_weight(0,2,2);
    wg->set_edge_weight(0,3,0);
    wg->set_edge_weight(1,2,1);
    wg->set_edge_weight(1,3,-1);
    wg->set_edge_weight(2,3,1);
    auto verry = find_cherry(wg);
    t.is_true(verry.size() == 3);
    
    t.ok();


    t.check("Counting branching numerb");
    test_branching_number_calculation();
    t.ok();
    return 0;
}
