#include <cassert>
#include <iostream>

#include "../utils/get_components.hpp"
#include "../graph/dumb_weighted_graph.hpp"
#include "tester.hpp"
using namespace std;


int main(void){
    Tester t;
    t.check("components for one simple case");
    WeightedGraph * graph = new DumbWeightedGraph(3);
    graph->set_edge_weight(0,1, 1);
    graph->set_edge_weight(0,2, -1);
    graph->set_edge_weight(1,2, -1);

    vector<vector<int>> v = get_components(*graph);
    t.is_true(v.size() == 2);
    int two_pos = v[0][0] == 2 ? 0 : 1;
    t.is_true(v[two_pos][0] == 2);
    int one_pos = v[1-two_pos][0] == 1 ? 0 : 1;
    t.is_true(v[1-two_pos][one_pos] == 1);
    t.is_true(v[1-two_pos][1-one_pos] == 0);
    t.ok();

    return 0;
}
