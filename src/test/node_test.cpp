#include "../graph/weighted_graph.hpp"
#include "../graph/dumb_weighted_graph.hpp"
#include "../node/cherry.hpp"
#include <cassert>
#include <iostream>
#include "tester.hpp"

#define MAX_V 10
#define DEFAULT_E 5

using std::cout;
using std::endl;

int main ( void ) {
    Tester t;
    t.check("graph construction");

    {
        // when using a variable, it is nulled
        WeightedGraph *g = new DumbWeightedGraph(3);
        auto e = new GreedyCherryNode(g, 5);
        t.is_true(g == nullptr);
        t.is_true(e != nullptr);
        delete e;
    }

    {
        // when using a copy, it is still valid
        WeightedGraph *g = new DumbWeightedGraph(3);
        auto e = new GreedyCherryNode(g->copy(), 5);
        t.is_true(g != nullptr);
        t.is_true(e != nullptr);
        delete e;
        delete g;
    }

    {
        // we can also use a new value directly
        auto e = new GreedyCherryNode(new DumbWeightedGraph(3), 5);
        t.is_true(e != nullptr);
        delete e;
    }
    t.ok();

    return 0;
}
