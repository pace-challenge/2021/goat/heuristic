#include "../graph/graph.hpp"
#include <cassert>
#include <iostream>
#include "tester.hpp"

#define MAX_V 10
#define DEFAULT_E 5

using std::cout, std::endl;

int main ( void ) {
    Tester t;
    t.check("graph construction");
    Graph * G = new Graph( MAX_V, DEFAULT_E );

    t.is_true( G->get_vertices_count() == MAX_V );
    t.is_true( G->get_edges_count() == 0 );

    t.ok();

    t.check("edges adding");
    for ( int i = 0; i < MAX_V; ++i ) {
        for ( int j = i + 1; j < MAX_V; ++ j ) {
            G->add_edge( i, j );
            t.is_true( G->contains_edge( j, i ) );
            t.is_true( G->contains_edge( i, j ) );
        }
    }
    t.ok();

    t.check("edges removal");
    for ( int i = 0; i < MAX_V; ++i ) {
        for ( int j = i + 1; j < MAX_V; ++ j ) {
            G->remove_edge( i, j );
            t.is_true( ! G->contains_edge( j, i ) );
            t.is_true( ! G->contains_edge( i, j ) );
        }
    }
    t.ok();

    delete G;
    return 0;
}
