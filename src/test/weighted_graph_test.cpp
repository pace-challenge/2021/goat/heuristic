#include "../graph/dumb_weighted_graph.hpp"
#include <cassert>
#include <iostream>
#include "tester.hpp"
#include "../utils/get_components.hpp"

using std::cout;
using std::endl;

int main ( void ) {
    Tester t;
    t.check("graph construction");
    WeightedGraph * G = new DumbWeightedGraph(11);

    t.is_true( G->size() == 11 );

    G->set_edge_weight(6, 7, 1);
    G->set_edge_weight(6, 8, 1);
    G->set_edge_weight(5, 6, 1);
    G->set_edge_weight(5, 7, 1);
    G->set_edge_weight(6, 10, 1);
    G->set_edge_weight(8, 10, 1);
    G->set_edge_weight(8, 9, 1);
    G->set_edge_weight(9, 10, 1);
    G->set_edge_weight(2, 3, 1);
    G->set_edge_weight(7, 8, 1);
    G->set_edge_weight(5, 8, 1);

    t.ok();

    t.check("getting components");
    vector<vector<int>> components = get_components(*G);
    
    t.is_true(components.size() == 5);
    t.is_true(components[0].size() == 1);
    t.is_true(components[0][0] == 0);
    t.is_true(components[1].size() == 1);
    t.is_true(components[1][0] == 1);
    t.is_true(components[2].size() == 2);
    t.is_true(components[3].size() == 1);
    t.is_true(components[3][0] == 4);
    t.is_true(components[4].size() == 6);

    t.ok();

    t.check("inducing subgraphs");

    WeightedGraph* child_graph_0 = G->induced_subgraph(components[0]);
    t.is_true(child_graph_0->size() == 1);
    t.is_true(child_graph_0->all_vertices()[0] == 0);

    WeightedGraph* child_graph_1 = G->induced_subgraph(components[1]);
    t.is_true(child_graph_1->size() == 1);
    t.is_true(child_graph_1->all_vertices()[0] == 1);


    WeightedGraph* child_graph = G->induced_subgraph(components[2]);
    t.is_true(child_graph->size() == 2);
    t.is_true(child_graph->all_vertices()[0] == 2);
    t.is_true(child_graph->all_vertices()[1] == 3);

    t.ok();

    delete G;
}
