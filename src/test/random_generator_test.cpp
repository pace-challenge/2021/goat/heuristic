#include "../generator/random_generator.hpp"

#include "tester.hpp"


int main(void){
    Tester t;
    t.check("generate_graph");
    RandomGenerator generator;
    Graph graph1 = generator.generate_graph(10, 0, 1);
    t.is_true(graph1.get_edges_count() == 10*9/2);

    Graph graph2 = generator.generate_graph(10, 20, 10);
    t.is_true(graph2.get_edges_count() == 20);

    Graph graph3 = generator.generate_graph(10, 20, 1);
    t.is_true(graph3.get_edges_count() == 10*9/2 - 20);

    Graph graph4 = generator.generate_graph(120, 120*119/2, 1);
    t.is_true(graph4.get_edges_count() == 0);

    Graph graph5 = generator.generate_graph(120, 120*119/2, 120);
    t.is_true(graph5.get_edges_count() == 120*119/2);

    t.ok();

    return 0;
}
