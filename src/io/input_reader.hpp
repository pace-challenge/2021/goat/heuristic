#pragma once

#include <iostream>
#include "../graph/graph.hpp"
#include "../graph/neighbours_graph.hpp"
#include "../solution.hpp"

using std::istream;

/*!
 * Helper class for reading graph representation.
 */
struct InputReader {

    /*!
     * Reads data from input stream and transforms it to corresponding graph instance.
     *
     * \param[in] input Input stream to be read from.
     *
     * \return Concrete instance of the graph read from the stream.
     */
    static Graph * read ( istream & input );

    /*!
     * Reads data from input stream and transforms it to corresponding NeighboursGraph instance.
     *
     * \param[in] input Input stream to be read from.
     *
     * \return Concrete instance of the graph read from the stream.
     */
    static NeighboursGraph * read_neighbours_graph ( istream & input );

    /*!
     * Reads data from input stream and transforms it to corresponding solution instance.
     *
     * \param[in] input Input stream to be read from.
     *
     * \return Concrete instance of the graph representing the solution.
     */
    static Solution * readSol ( istream & input );

};
