#include "naive_solutions.hpp"
#include <algorithm>
using std::sort;

CliqueSolution * remove_all_edges_solution(const WeightedGraph *graph){
    CliqueSolution *res = new CliqueSolution();
    for(int n : graph->all_vertices()) res->add_vertex_as_a_component(n);
    return res;
}

CliqueSolution * add_non_edges_solution(const WeightedGraph *graph){
    return new CliqueSolution(vector<vector<int>>(1, graph->all_vertices()));
}

CliqueSolution * greedy_solution(const WeightedGraph *graph, vector<int> ordered_vertices){
    WeightedGraph *copy = graph->copy();
    CliqueSolution *res = new CliqueSolution();
    auto vs = copy->all_vertices();
    set<int> remaining_vertices(vs.begin(),vs.end());
    for(int v : ordered_vertices){
        if(remaining_vertices.count(v) != 0){
            auto neighbors = copy->neighbors(v);
            res->add_vertex_as_a_component(v);
            copy->delete_vertex(v);
            remaining_vertices.erase(v);
            for(int n : neighbors) {
                res->add_vertex_to_component_of(n, v);
                copy->delete_vertex(n);
                remaining_vertices.erase(n);
            }
        }
    }
    delete copy;
    return res;
}

CliqueSolution * greedy_solution_low_to_high(const WeightedGraph *graph){
    auto vertices = graph->all_vertices();
    sort(vertices.begin(), vertices.end()); // take vertices sorted by name in INCREASING order
    return greedy_solution(graph, vertices);
}

CliqueSolution * greedy_solution_high_to_low(const WeightedGraph *graph){
    auto vertices = graph->all_vertices();
    sort(vertices.rbegin(), vertices.rend()); // take vertices sorted by name in DECREASING order
    return greedy_solution(graph, vertices);
}

vector<CliqueSolution*> get_solutions(const WeightedGraph * graph){
    vector<CliqueSolution*> res;
    res.push_back(remove_all_edges_solution(graph));
    res.push_back(add_non_edges_solution(graph));
    res.push_back(greedy_solution_low_to_high(graph));
    res.push_back(greedy_solution_high_to_low(graph));
    return res;
}

