#include "utils.hpp"
#include "../node/cherry.hpp"

vector<int> is_solution(WeightedGraph *graph, Solution *sol){
    WeightedGraph *copy = graph->copy();
    for(auto edge : sol->getSolution()) copy->flip_edge(edge.first, edge.second);
    auto v = find_cherry(copy);
    delete copy;
    return v;
}
