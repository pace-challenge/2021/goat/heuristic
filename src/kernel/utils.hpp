#pragma once

#include <vector>
#include "../graph/weighted_graph.hpp"
#include "../solution.hpp"

using std::vector;

vector<int> is_solution(WeightedGraph *graph, Solution *sol);

