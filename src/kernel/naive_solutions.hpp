#include <vector>
#include "../solution/clique_solution.hpp"
#include "../graph/weighted_graph.hpp"

using std::vector;

CliqueSolution * remove_all_edges_solution(const WeightedGraph *graph);
CliqueSolution * add_non_edges_solution(const WeightedGraph *graph);
CliqueSolution * greedy_solution(const WeightedGraph *graph, vector<int> ordered_vertices);
CliqueSolution * greedy_solution_low_to_high(const WeightedGraph *graph);
CliqueSolution * greedy_solution_high_to_low(const WeightedGraph *graph);
vector<CliqueSolution*> get_solutions(const WeightedGraph * graph);
