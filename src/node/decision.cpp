#include "decision.hpp"
#include "../lower_bounds/greedy_disjoint_conflict_triples.hpp"
#include "../computation/tree.hpp"
#include "../strategy/dumb_strategy.hpp"


#include "../reduction_rule/unaffordable_edges.hpp"
#include "../reduction_rule/remove_cliques.hpp"
#include "../reduction_rule/almost_clique.hpp"
#include "edge_branch.hpp"
#include "iter.hpp"
#include "../reduction_rule/reduce_2k.hpp"
#include "component.hpp"
#include "../utils/get_components.hpp"
#include "../special_cases.hpp"
#include "../upper_bounds/meta_upper_bound.hpp"
#include "../utils/timer.hpp"

#include <functional>
#include <iostream>
#include <chrono>

using std::function;

DecisionNode::DecisionNode(WeightedGraph *& graph, Bound bound, bool is_cheeky):Node(graph, bound, "decision"), is_cheeky(is_cheeky){ }
DecisionNode::DecisionNode(WeightedGraph *&& graph, Bound bound, bool is_cheeky):DecisionNode(graph, bound, is_cheeky){}

DecisionNode::~DecisionNode(){
    for(auto &r : reductionRules) delete r;
}

EvalResult DecisionNode::evaluate() {

    if(bound.empty()) return eval_fail();
    if(bound.upper < 0) {
        return eval_fail();
    }
    WeightedGraph *copy = graph->copy();

    int ub;
    do{
        ub = bound.upper;
        // handles also the lowerbound since it is more effective to calculate the lowerbound just once
        UnaffordableEdgeReduction* unaffordable_edges = new UnaffordableEdgeReduction();
        reductionRules.push_back(unaffordable_edges);
        reductionRules.back()->apply(copy, bound);
        if(bound.empty()){
            delete copy;
            return eval_fail();
        }
        
        // currently does not apply in any case
        // while(true){
        //     pair<int, int> result_2k = find_2k_reducible_vertex(copy);
        //     if(result_2k.first == 1) {
        //         reductionRules.push_back(new TwokReduction(result_2k.second));
        //         reductionRules.back()->apply(copy, bound);
        //     }else{
        //         break;
        //     }
        // }
        // if(bound.empty()){
        //     delete copy;
        //     return eval_fail();
        // }
    }while(ub > 1.1*bound.upper);

    vector<Node*> new_nodes;
    auto new_dec_node_lambda = [this](WeightedGraph *g, Bound b){ return new DecisionNode(g, b, this->is_cheeky);};
    // try to decompose into multiple connected components

    auto components = get_components(*copy);


    if(components.size() > 1){
        new_nodes.push_back(new ComponentNode(copy->copy(), bound, new_dec_node_lambda));
    }else if(bound.size() == 1){
        // paths and cycles are not common
        // if(copy->size() > 4){
        //     if(is_path(*copy)){
        //         auto p = solve_path(*copy);
        //         bound.decrease_by(p.first);
        //         reconstruct(p.second);
        //         return bound.empty() ? eval_fail() : eval_success(p.second);
        //     }
        //     if(is_cycle(*copy)){
        //         auto p = solve_cycle(*copy);
        //         bound.decrease_by(p.first);
        //         reconstruct(p.second);
        //         return bound.empty() ? eval_fail() : eval_success(p.second);
        //     }
        // }
        new_nodes.push_back(new EdgeBranchNode(copy->copy(), bound, new_dec_node_lambda));
    }else{
        // init
        new_nodes.push_back(new IterNode(copy->copy(), bound, new_dec_node_lambda));
    }

    delete copy;

    return eval_expand(new_nodes);
}

EvalResult DecisionNode::reconstruct(CliqueSolution *solution){
    if(solution != nullptr){
        for(auto it = this->reductionRules.rbegin(); it != this->reductionRules.rend(); ++it){
            (*it)->reconstruct_solution(solution);
        }
        return eval_success(solution);
    }
    return eval_void();
}

EvalResult DecisionNode::child_finished(Node *child) {
    return reconstruct(child->solution);
}

EvalResult DecisionNode::all_children_finished() {
    return eval_fail();
}

string DecisionNode::get_name() const{
    return Node::get_name() + ", reds:" + to_string(reductionRules.size());
}

