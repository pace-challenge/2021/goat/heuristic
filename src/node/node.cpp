#include "node.hpp"
#include <iostream>
#include <cassert>
#include <string>

Bound::Bound(int lower, int upper): lower(lower), upper(upper){
}

void greedy_node_bounds_check(const Bound & bound, string node_name){
    if(bound.lower < bound.upper){
        std::cerr << node_name << " is a greedy node, and so cannot accept bounds which are not equal\n";
        assert(false);
    }
}

int Node::id_generator = 0;

Node::Node(WeightedGraph*& graph, Bound bound, string name)
    : graph(graph), bound(bound), original_bound(bound), solution(nullptr),
      parent(nullptr), state(NEW), remaining_children(0), depth(0), name(name)
{
    if(graph == nullptr){
        std::cerr << "Node constructor supplied with a nullptr graph. It might have been cleared when passed into a different node before -- this is forbidden. Use graph->copy() give a copy instance into the node." << std::endl;
        assert(graph != nullptr);
    }
    graph = nullptr;
    id = id_generator++;
}

Node::Node(WeightedGraph *& graph, int lower, int upper, string name) : Node(graph, Bound(lower, upper), name){}

Node::~Node(){
    free_graph();
    for(Node *child : children) delete child;
    /* std::cerr << "deleting " << this << '\n'; */
}

EvalResult Node::child_finished(Node *child) { (void)child; return eval_void(); }

EvalResult Node::all_children_finished() { return eval_void(); }

void Node::notify_parent_about_bound_change(Node *child, Bound new_bounds) {
    (void) child;
    (void) new_bounds;
}

/*!
 * Parent told this vertex its bounds changed. It is up to the parent to
 * recompute the bounds, so the parameter should be valid for this node.
 */
void Node::notify_child_about_bound_change(Bound new_bounds) {
    bound = new_bounds;
}


Node* Node::get_parent() const { return parent; }

vector<Node*> Node::get_children() const { return children; }

set<Node*> Node::get_children_except(Node *omit_child) const{
    const auto ch = get_children();
    set<Node*> set_children(ch.begin(), ch.end());
    if(omit_child != nullptr) set_children.erase(omit_child);
    return set_children;
}

EvalState Node::get_state() const { return state; }

string Node::get_name() const{
    return name + "_" + std::to_string(id);
}

void Node::free_graph() {
    if(graph != nullptr){
        delete graph;
        graph = nullptr;
    }
}
