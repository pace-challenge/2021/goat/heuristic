#pragma once

#include "../computation/eval.hpp"

#include <cassert>
#include <vector>
#include <set>
#include <string>
#include <iostream>
#include <functional>

using std::vector;
using std::set;
using std::string;

class ComputationTree;

struct Bound {
    Bound(int lower, int upper);
    int lower, upper;

    int size(){ return upper-lower+1; }

    int empty(){ return lower > upper; }

    void decrease_by(int value){
        assert(value >= 0);
        lower -= value;
        upper -= value;
        if(lower < 0) lower = 0;
    }

    friend ostream & operator<<(ostream &os, const Bound &bound){
        os << '(' << bound.lower << "," << bound.upper << ')';
        return os;
    }
};

void greedy_node_bounds_check(const Bound & bound, string node_name);

class Node {
    public:

        using node_factory = std::function<Node*(WeightedGraph*, Bound)>;
        /*!
         * Creates the desired node. Graph is MOVED into the node, so it must not
         * be used afterwards -- this is indicated by clearing the passed variable
         * to nullptr which forces the subsequent uses the variable to fail.
         */
        Node(WeightedGraph *& graph, Bound bound, string name="undefined");
        Node(WeightedGraph *& graph, int lower, int upper, string name="undefined");

        /*!
         * Clears all children of the nodes and removes the graph, if it was not
         * removed yet.
         */
        virtual ~Node();

        /*!
         * Free the graph if it was not freed before. Prefer this method before
         * freeing it yourself as this method can be called multiple times without
         * failing.
         */
        void free_graph();

        /*!
         * Is called when we want to know the result of this node.
         * It may create new children, or give success or fail straight away.
         */
        virtual EvalResult evaluate() = 0;

        /*!
         * Is called whenever child status changed to either success or fail.
         * As a response, the node may create new children, or give success or fail.
         */
        virtual EvalResult child_finished(Node *child);

        /*!
         * Is called when no other unresolved children remain. The resolved child
         * will also invoke child_updated BEFORE this method is called.
         * As a response, the node may create new children, or give success or fail.
         */
        virtual EvalResult all_children_finished();

        /*!
         * Child notified this node that its bounds changed. It is up to this vertex
         * to recompute the bounds.
         */
        virtual void notify_parent_about_bound_change(Node *child, Bound new_bounds);

        /*!
         * Parent told this vertex its bounds changed. It is up to the parent to
         * recompute the bounds, so the parameter should be valid for this node.
         */
        virtual void notify_child_about_bound_change(Bound new_bounds);


        Node* get_parent() const;

        vector<Node*> get_children() const;

        set<Node*> get_children_except(Node *omit_child) const;

        EvalState get_state() const;

        virtual string get_name() const;


        WeightedGraph *graph;
        Bound bound;
        Bound original_bound;
        CliqueSolution *solution;

        int id;
    private:
        static int id_generator;
        Node *parent;
        vector<Node*> children;
        EvalState state;
        int remaining_children;

        int depth;
        string name;

    friend ComputationTree;
};
