#pragma once

#include <functional>
#include <vector>

#include "../graph/weighted_graph.hpp"
#include "node.hpp"
#include "../reduction_rule/reduction_rule.hpp"

using std::vector;

struct DecisionNode : public Node {

    DecisionNode(WeightedGraph *& graph, Bound bound, bool is_cheeky=false);
    DecisionNode(WeightedGraph *&& graph, Bound bound, bool is_cheeky=false);
    virtual ~DecisionNode();

    virtual EvalResult evaluate();

    virtual EvalResult child_finished(Node *child);
    virtual EvalResult all_children_finished();

    virtual string get_name() const;

    private:
        vector<ReductionRule<WeightedGraph> *> reductionRules;

        EvalResult reconstruct(CliqueSolution *solution);
        bool is_cheeky;
};
