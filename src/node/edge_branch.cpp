#include "edge_branch.hpp"
#include "cherry.hpp"
#include "../utils/induced_cost.hpp"

#include <algorithm>
#include <iostream>
#include <cassert>
#include <cmath>
#include <functional>

using std::function;


EdgeBranchNode::EdgeBranchNode(WeightedGraph *& graph, Bound bound): Node(graph, bound, "edge branch"), mergeRule(nullptr),
    new_node_factory([](WeightedGraph * g, Bound b){return new EdgeBranchNode(g, b);}) {
    greedy_node_bounds_check(bound, "edge branch");
}
EdgeBranchNode::EdgeBranchNode(WeightedGraph *&& graph, Bound bound): EdgeBranchNode(graph, bound){}


EdgeBranchNode::EdgeBranchNode(WeightedGraph *& graph, Bound bound, node_factory new_node_factory):
    Node(graph, bound, "edge branch"), mergeRule(nullptr), new_node_factory(new_node_factory){
    greedy_node_bounds_check(bound, "edge branch");
}
EdgeBranchNode::EdgeBranchNode(WeightedGraph *&& graph, Bound bound, node_factory new_node_factory):EdgeBranchNode(graph, bound, new_node_factory){}


// we have equality x^n = x^(n-a) + x^(n-b)
// 1 = x^(-a) + x^(-b)
// we want smallest x for which it holds
double compute_branching_number(double a, double b){
    assert(a >= 0);
    assert(b >= 0);
    double xlb = 1.00;
    double xub = 3.00;
    for(int i = 0; i < 10; ++i){
        double xmid = (xlb + xub)/2;
        if(pow(xmid, -a) + pow(xmid, -b) <= 1){
            xub = xmid;
        } else{
            xlb = xmid;
        }
    }
    return xub;
}

double compute_edge_branching_number(const WeightedGraph * g, int u, int v){
    // pair<int, int> icp_zero_edges = induced_cost_permanent_zero_edges_count(g,u,v);
    // double icp = std::max(icp_zero_edges.first - 0.35*icp_zero_edges.second, 0.0);
    double icp = g->get_icp(u, v);
    double forbidding_cost = g->get_edge_weight(u,v);
    return compute_branching_number(icp, forbidding_cost);
}

vector<int> compute_best_branching_edge(const WeightedGraph * g){
    double best_branching_number = 9999;
    vector<int> best_edge;
    for(int v : g->all_vertices()){
        for(int u : g->all_vertices()){
            if(u == v || !g->contains_edge(u,v)){
                continue;
            }
            double uv_branching_number = compute_edge_branching_number(g, u, v);
            if(uv_branching_number < best_branching_number){
                best_branching_number = uv_branching_number;
                best_edge = {u,v};
            }
        }
    }
    return best_edge;
}

WeightedGraph * EdgeBranchNode::copy_then_merge(const WeightedGraph * source, int u, int v, Bound &bound){
    WeightedGraph * copy = source->copy();
    mergeRule = new MergeReduction(u,v);
    mergeRule->apply(copy, bound);
    return copy;
}

WeightedGraph * copy_then_forbid(const WeightedGraph * source, int u, int v){
    WeightedGraph * copy = source->copy();
    copy->set_edge_forbidden(u, v);
    return copy;
}

EvalResult EdgeBranchNode::evaluate() {
    if(bound.upper < 0) {
        return eval_fail();
    }
    vector<int> branching_edge = compute_best_branching_edge(graph);
    if(branching_edge.size()){
        vector<Node*> new_nodes;
        this->u = branching_edge[0];
        this->v = branching_edge[1];
        assert(graph->get_edge_sign(u,v) == 1);
        // merge
        int merge_cost = graph->get_icp(u, v);
        if(merge_cost <= bound.upper){
            Bound new_bound = bound;
            WeightedGraph * new_graph = copy_then_merge(graph, u, v, new_bound);
            new_nodes.push_back(new_node_factory(new_graph, new_bound));
        }
        // forbid
        int forbid_cost = graph->get_edge_weight(u,v); // it is an edge so I don't have to check
        if(forbid_cost <= bound.upper){
            Bound new_bound = bound;
            new_bound.decrease_by(forbid_cost);
            WeightedGraph * new_graph = copy_then_forbid(graph, u, v);
            new_nodes.push_back(new_node_factory(new_graph, new_bound));
        }

        return new_nodes.size() > 0 ? eval_expand(new_nodes) : eval_fail();
    }else{
        return eval_success(new CliqueSolution(*(this->graph)));
    }
}


EvalResult EdgeBranchNode::child_finished(Node *child) {
    CliqueSolution *solution = child->solution;
    if(solution != nullptr){
        const auto &children = get_children();
        assert(children.size() > 0);
        int idx = find(children.begin(), children.end(), child) - children.begin();
        if(idx == 0 && mergeRule != nullptr){
            //merge case
            mergeRule->reconstruct_solution(solution);
        }else {
            //forbidden edge case
        }
        return eval_success(solution);
    }else{
        return eval_void();
    }
}

EvalResult EdgeBranchNode::all_children_finished() {
    return eval_fail();
}

EdgeBranchNode::~EdgeBranchNode(){
    if(mergeRule != nullptr) delete mergeRule;
}
