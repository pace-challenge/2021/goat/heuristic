#pragma once

#include "node.hpp"
#include "../computation/eval.hpp"
#include "../graph/weighted_graph.hpp"

#include <functional>

using std::function;


struct IterNode : public Node {

    IterNode(WeightedGraph *& graph, Bound bound, Node::node_factory new_node_fun);
    IterNode(WeightedGraph *&& graph, Bound bound, Node::node_factory new_node_fun);

    virtual EvalResult evaluate();

    virtual EvalResult child_finished(Node *child);

    private:
        Node::node_factory new_node_fun;
};

