#include "component.hpp"

#include <algorithm>
#include "../solution/clique_solution.hpp"
#include "../graph/dumb_weighted_graph.hpp"



ComponentNode::ComponentNode(WeightedGraph *& graph, Bound bound, Node::node_factory children_factory)
    :Node(graph, bound, "component"), children_factory(children_factory){}

ComponentNode::ComponentNode(WeightedGraph *&& graph, Bound bound, Node::node_factory children_factory):ComponentNode(graph, bound, children_factory){}

/*!
 * Split the graph into induced subgraphs made of connected components.
 * Set the bounds to 0-upper as we do not know anything else.
 */
EvalResult ComponentNode::evaluate() {
    auto components = get_components(*graph);
    vector<Node *> children;
    for(auto c : components) {
        WeightedGraph* child_graph = graph->induced_subgraph(c);
        Bound child_bound(0, bound.upper);
        Node *n = children_factory(child_graph, child_bound);
        children.push_back(n);
    }
    return eval_expand(children);
}

/*!
 * Child must return success for this node to succeed.
 */
EvalResult ComponentNode::child_finished(Node *child){
    this->notify_parent_about_bound_change(child, child->bound);
    EvalState s = child->get_state();
    return (is_resolved(s) && s != SUCCESS) ? eval_fail() : eval_void();
}

/*!
 * As all children succeeded we just merge the solutions.
 */
EvalResult ComponentNode::all_children_finished() {
    CliqueSolution *sol = new CliqueSolution();
    for(Node *n : get_children()) sol->merge(*n->solution);
    Solution *s = sol->get_solution(graph);
    return ((int)s->weight() <= bound.upper) ? eval_success(sol) : eval_fail();
}

void ComponentNode::recompute_bounds(Node *bounded_child){
    int upper_sum = 0;
    int lower_sum = 0;
    for(auto &child : get_children()) {
        upper_sum += child->bound.upper;
        lower_sum += child->bound.lower;
    }
    Node *parent = get_parent();
    // better upper bound from child -> send upper bound to parent
    if(parent && upper_sum < parent->bound.upper){
        parent->notify_parent_about_bound_change(this, Bound(parent->bound.lower, upper_sum));
    }
    auto other_children = get_children_except(bounded_child);
    // better lower bound from child or parent -> send to other children as changed upper bound
    // if(bound.upper != INF)
    for(auto &child : other_children){
        int child_upper_bound = bound.upper - (lower_sum - child->bound.lower);
        if(child_upper_bound < child->bound.upper){
            child->notify_child_about_bound_change(Bound(child->bound.lower, child_upper_bound));
        }
    }
}

void ComponentNode::notify_parent_about_bound_change(Node *bounded_child, Bound new_bounds){
    /* std::cerr << "new bounds from child " << new_bounds.lower << ' ' << new_bounds.upper << std::endl; */
    recompute_bounds(bounded_child);
    (void)new_bounds;
}

void ComponentNode::notify_child_about_bound_change(Bound new_bounds){
    /* std::cerr << "new bounds from parent " << new_bounds.lower << ' ' << new_bounds.upper << std::endl; */
    recompute_bounds();
    (void)new_bounds;
}
