#include "iter.hpp"

IterNode::IterNode(WeightedGraph *& graph, Bound bound, Node::node_factory new_node_fun)
    :Node(graph, bound, "iter"), new_node_fun(new_node_fun){}
IterNode::IterNode(WeightedGraph *&& graph, Bound bound, Node::node_factory new_node_fun)
    :IterNode(graph, bound, new_node_fun){}

EvalResult IterNode::evaluate() {
    /* cerr << "NEW ITERAITON\n"; */
    return eval_expand(new_node_fun(graph->copy(), Bound(bound.lower, bound.lower)));
}

EvalResult IterNode::child_finished(Node *child) {
    if(child->get_state() == SUCCESS){
        /* cerr << "ITER PARENT SUCCESS\n"; */
        bound.upper = bound.lower;
        return eval_success(child->solution);
    } else{
        bound.lower += 1;
        if(bound.lower > bound.upper){
            return eval_fail();
        }
        if(get_parent()) get_parent()->notify_parent_about_bound_change(this, bound);
        //std::cerr << "ITER PARENT NEXT K: " << bound.lower << '\n';
        return evaluate();
    }
}

