#include "solution.hpp"

#include <iostream>
#include <cassert>

using std::swap;
using std::make_pair;
using std::endl;

void Solution::add_edge ( int from, int to, int w ) {
    if(from > to) swap(from, to);
    /* std::cerr << ">> SOL added " << from << ' ' << to << std::endl; */
    auto new_edge = make_pair(from, to);
    assert(!solutionEdges.count(new_edge));
    solutionEdges.insert(new_edge);
    weights[new_edge] = abs(w);
}

void Solution::remove_edge ( int from, int to ) {
    if(from > to) swap(from, to);
    /* std::cerr << ">> SOL remove " << from << ' ' << to << std::endl; */
    auto edge = make_pair(from, to);
    solutionEdges.erase(edge);
    weights.erase(edge);
}
bool Solution::contains_edge ( int from, int to ){
    if(from > to) swap(from, to);
    return solutionEdges.count(make_pair(from, to));
}

int optional_map ( int value, const map<int,int> & mapping ) {
    auto it = mapping.find( value );
    return ( it != mapping.end() ) ? it->second : value;
}

void Solution::map_vertices ( const map<int,int> & mapping ) {
    set<pair<int,int>> newSolutionEdges;
    map<pair<int,int>, int> newWeight;
    for ( const pair<int,int> & p : solutionEdges ) {
        int from = optional_map( p.first, mapping );
        int to = optional_map( p.second, mapping );
        auto mapped_edge = make_pair ( from, to );
        newSolutionEdges.insert ( mapped_edge );
        newWeight[mapped_edge] = weights[p];
    }
    solutionEdges = newSolutionEdges;
    weights = newWeight;
}

void Solution::map_vertices ( const vector<int> & mapping ) {
    set<pair<int,int>> newSolutionEdges;
    map<pair<int,int>, int> newWeight;
    for ( const pair<int,int> & p : solutionEdges ) {
        int from = mapping[p.first];
        int to = mapping[p.second];
        auto mapped_edge = make_pair ( from, to );
        newSolutionEdges.insert ( mapped_edge );
        newWeight[mapped_edge] = weights[p];
    }
    solutionEdges = newSolutionEdges;
    weights = newWeight;
}

void Solution::merge(const Solution & other){
    for(pair<int, int> p : other.solutionEdges){
        int w = other.weights.find(p)->second;
        this->add_edge(p.first, p.second, w);
    }
}

int Solution::weight() const{
    int w = 0;
    for(auto &e : solutionEdges) w += this->weights.find(e)->second;
    return w;
}

size_t Solution::size() const{
    return this->solutionEdges.size();
}

set<pair<int,int>> Solution::getSolution() const{
    return solutionEdges;
}

bool Solution::isInSolution( int from, int to ) const{
    if(from > to) swap(from, to);
    return solutionEdges.count(make_pair(from, to));
}

ostream & operator<< ( ostream & os, const Solution & s ) {
    #ifdef kernel
    os << s.solutionEdges.size() << endl;
    #endif
    for ( const pair<int,int> & p : s.solutionEdges ) {
        os << p.first+1 << ' ' << p.second+1 << endl;
    }
    return os;
}
