#include "computation/eval.hpp"
#include "node/node.hpp"
#include "strategy/priority_strategy.hpp"
#include "strategy/dumb_strategy.hpp"
#include "computation/tree.hpp"
#include "graph/conversion.hpp"
#include "graph/weighted_graph.hpp"
#include "io/input_reader.hpp"
#include "solution.hpp"
#include "node/edge_branch.hpp"
#include "node/decision.hpp"
#include "node/iter.hpp"
#include "node/component.hpp"
#include "graph/materialized_weighted_graph.hpp"
#include "graph/dumb_weighted_graph.hpp"
#include "upper_bounds/meta_upper_bound.hpp"
#include "node/component.hpp"
#include "lower_bounds/greedy_disjoint_conflict_triples.hpp"
#include "utils/bitset.hpp"
#include "utils/min_cut.hpp"
#include "reduction_rule/almost_clique.hpp"


#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;

#define INF (1<<30)

int priority(const Node *n){
    return -n->graph->size();
}
int count;

// its a proper function so it can refernce itself

int main ( void ) {
    InputReader in;
    auto graphFactory = [](int SZ){ return new MaterializedWeightedGraph(SZ); };
    //auto graphFactory = [](int SZ){ return new DumbWeightedGraph(SZ); };
    Graph *g = in.read( cin );
    WeightedGraph * graph = convert_graph_to_weighted_graph(*g, graphFactory);
    delete g;

    // graph = new MaterializedWeightedGraph(4, -1);
    // graph->unsafe_set_edge_weight(0, 1, 1);
    // graph->unsafe_set_edge_weight(1, 2, 2);
    // graph->unsafe_set_edge_weight(2, 3, 2);
    // graph->unsafe_set_edge_weight(1, 3, 1);

    // std::cerr<<"xxxx"<<std::endl;
    // auto mc = min_cut_of_induced_graph(*graph, {1,2,3});

    // std::cerr<<"mc.value="<<mc.value<<std::endl;
    // for(auto e : mc.edges) {
    //     std::cerr<<e.first<<' '<<e.second<<std::endl;
    // }
    // std::cerr<<"xxxx"<<std::endl;

    // std::cerr<<"is_C_almost_clique: "<<is_C_almost_clique(*graph, {1,2,3})<<std::endl;
    // auto C = find_almost_clique(*graph);
    // std::cerr<<"find_almost_clique: "<<C.size()<<" =>";
    // for(int v : C)std::cerr<<" "<<v;
    // std::cerr<<std::endl;

    // return 0;



    // int r_greedy_disjoint_conflict_triples_v03 = greedy_disjoint_conflict_triples_v03(*graph);
    // int r_greedy_disjoint_conflict_triples_v02 = greedy_disjoint_conflict_triples_v02(*graph);
    // std::cerr<<"lb="<<r_greedy_disjoint_conflict_triples_v03<<std::endl;
    // std::cerr<<"greedy_disjoint_conflict_triples_v02="<<r_greedy_disjoint_conflict_triples_v02<<std::endl;
    // return 0;
    // itn

    graph->recompute_structures();

    PriorityStrategy *strategy = new PriorityStrategy(priority);

    auto ub = meta_upper_bound(*graph);
    // std::cerr<<"lb="<<r_greedy_disjoint_conflict_triples_v03<<",ub="<<ub.first<<",gap="<<ub.first-r_greedy_disjoint_conflict_triples_v03<<std::endl;

    ComputationTree ct(strategy);
    Node * root = new DecisionNode(graph, Bound(0, ub.first), true);
    Solution *solution = ct.evaluate(root);
    if(solution){
        cerr << "=== solution ===\n";
        cout << (*solution);
        cerr << "=== solution ===\n";
    }
    else cerr << "no solution found!\n";
    delete solution;
    return 0;
}


