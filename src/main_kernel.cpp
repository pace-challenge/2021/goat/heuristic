#include "node/node.hpp"
#include "utils/get_components.hpp"
#include "graph/conversion.hpp"
#include "graph/weighted_graph.hpp"
#include "graph/materialized_weighted_graph.hpp"
#include "reduction_rule/reduction_rule.hpp"
#include "reduction_rule/reduce_2k.hpp"
#include "reduction_rule/unaffordable_edges.hpp"
#include "reduction_rule/forbid_edge.hpp"
#include "reduction_rule/almost_clique.hpp"
#include "reduction_rule/cherry_pack_similar_neighborhood.hpp"
#include "reduction_rule/cherry_pack_leaves.hpp"
#include "io/input_reader.hpp"
#include "solution.hpp"
#include "kernel/naive_solutions.hpp"
#include "kernel/utils.hpp"
#include "upper_bounds/meta_upper_bound.hpp"

#include "computation/tree.hpp"
#include "computation/eval.hpp"
#include "strategy/priority_strategy.hpp"
#include "node/decision.hpp"

#include <iostream>
#include <vector>
#include <csignal>


using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::get;
using std::tuple;
using std::make_tuple;

void convert(const WeightedGraph *original, WeightedGraph *empty){
    auto vs = original->all_vertices();
    for(int n : vs) empty->add_vertex(n);
    for(int i=0; i<(int)vs.size(); ++i){
        int a = vs[i];
        for(int j=0; j<(int)vs.size(); ++j){
            int b = vs[j];
            if(a != b){
                int weight = original->get_edge_weight(a, b);
                empty->unsafe_set_edge_weight(a, b, weight);
            }
        }
    }
    empty->recompute_structures();
}

void assert_unweighted(const WeightedGraph *graph){
    auto vs = graph->all_vertices();
    for(int a : vs){
        for(int b : vs){
            if(a >= b) continue;
            int w = graph->get_edge_weight(a, b);
            assert(w == -1 || w == 1);
        }
    }
}

vector<pair<int,int>> get_edges(const WeightedGraph *graph){
    assert_unweighted(graph);
    vector<pair<int,int>> res;
    auto vs = graph->all_vertices();
    for(int a : vs){
        for(int b : vs){
            if(a >= b) continue;
            int w = graph->get_edge_weight(a, b);
            if(w == 1) res.push_back({a, b});
        }
    }
    return res;
}


// Priority for exact solver

int priority(const Node *n){
    return -n->graph->size();
}
Solution* compute_exactly(const WeightedGraph * graph, int ub, const int * end_immediately){
    WeightedGraph *copy_exact = graph->copy();
    PriorityStrategy *strategy = new PriorityStrategy(priority);

    ComputationTree ct(strategy, end_immediately);
    Node * root = new DecisionNode(copy_exact, Bound(0, ub), true);
    Solution *solution = ct.evaluate(root);
    if(solution){
        // cout << solution->size() << endl;
        // cout << "p cep " << 0 << " " << 0 << endl;
        // for(int i = 0; i < 4; ++i){
            // cout << *solution;
        // }
        return solution;
    }
    return nullptr;
}

vector<tuple<int,int>> changed_edges(WeightedGraph *original, WeightedGraph *reduced_weighted, vector<ReductionRule<WeightedGraph>*> &reductions){
    // decontract
    CliqueSolution * isolated_sol = remove_all_edges_solution(reduced_weighted);
    for(auto it = reductions.rbegin(); it!=reductions.rend(); ++it){
        ReductionRule<WeightedGraph> *rule = *it;
        rule->reconstruct_solution(isolated_sol);
    }
    auto components = isolated_sol->_get_components();
    vector<tuple<int,int>> flipped_edges;
    // for each merged clique, fill in all edges in the original graph
    for(auto &c : components) for(int a : c) for(int b : c) if(a < b){
        if(original->get_edge_weight(a, b) == -1){
            // original->unsafe_set_edge_weight(a, b, 1);
            flipped_edges.push_back(make_tuple(a, b));
        }
    }
    // for every pair with FORBIDDEN edge among them in the reduced_weighted copy,
    // remove all edges between respective groups in the original graph
    auto reduced_vertices = reduced_weighted->all_vertices();
    for(int ca : reduced_vertices) for(int cb : reduced_vertices) if(ca < cb){
        if(reduced_weighted->get_edge_weight(ca, cb) == FORBIDDEN_WEIGHT){
            for(int a : components[isolated_sol->component_of(ca)]){
                for(int b : components[isolated_sol->component_of(cb)]){
                    if(original->get_edge_weight(a, b) == 1){
                        // original->unsafe_set_edge_weight(a, b, -1);
                        flipped_edges.push_back(make_tuple(a, b));
                    }
                }
            }
        }
    }
    return flipped_edges;
}

int change_graph(WeightedGraph *original, WeightedGraph *reduced_weighted, vector<ReductionRule<WeightedGraph>*> &reductions){
    // decontract
    CliqueSolution * isolated_sol = remove_all_edges_solution(reduced_weighted);

    for(auto it = reductions.rbegin(); it!=reductions.rend(); ++it){
        ReductionRule<WeightedGraph> *rule = *it;
        rule->reconstruct_solution(isolated_sol);
    }
    auto components = isolated_sol->_get_components();

    int flipped_edges = 0;
    // for each merged clique, fill in all edges in the original graph
    for(auto &c : components) for(int a : c) for(int b : c) if(a < b){
        if(original->get_edge_weight(a, b) == -1){
            original->unsafe_set_edge_weight(a, b, 1);
            flipped_edges += 1;
        }
    }
    // for every pair with FORBIDDEN edge among them in the reduced_weighted copy,
    // remove all edges between respective groups in the original graph
    auto reduced_vertices = reduced_weighted->all_vertices();
    for(int ca : reduced_vertices) for(int cb : reduced_vertices) if(ca < cb){
        if(reduced_weighted->get_edge_weight(ca, cb) == FORBIDDEN_WEIGHT){
            for(int a : components[isolated_sol->component_of(ca)]){
                for(int b : components[isolated_sol->component_of(cb)]){
                    if(original->get_edge_weight(a, b) == 1){
                        original->unsafe_set_edge_weight(a, b, -1);
                        flipped_edges += 1;
                    }
                }
            }
        }
    }

    return flipped_edges;
    // cerr << "flipped: " << flipped_edges << endl;
}

void apply( ReductionRule<WeightedGraph> *reduction,
            Bound &bound,
            vector<ReductionRule<WeightedGraph>*> &reductions,
            WeightedGraph * graph){
    reductions.push_back(reduction);
    reduction->apply(graph, bound);
}

void apply_unweighted_reductions(WeightedGraph *graph, Bound &bound, vector<ReductionRule<WeightedGraph>*> &reductions){
    assert_unweighted(graph);
    apply(new CherryPackSimilarLeaves(), bound, reductions, graph);
    apply(new CherryPackSimilarNeighborhood(), bound, reductions, graph);
    graph->recompute_structures();
    assert_unweighted(graph);
}

void flip_solution_edges(Solution *s, vector<tuple<int,int>> &flipped_edges){
    for(auto p : flipped_edges){
        int a = get<0>(p);
        int b = get<1>(p);
        if(s->contains_edge(a, b)){
            s->remove_edge(a, b);
        }else{
            s->add_edge(a, b, 1);
        }
    }
}

// == experiment with the reductions here =============================================================
/*!
 * Utility function to apply reductions on the kernelization-track graphs.
 * All other tasks are already handeled elsewhere -- like solution reconstruction, etc.
 * The resulting graph should only have edges with weights 1 and -1 (this is checked later).
 */
void apply_reductions(WeightedGraph * graph, Bound &bound, vector<ReductionRule<WeightedGraph>*> &reductions){
    (void) graph; (void) bound; // yet unused, remove this
    // apply(new LeMyReduction(2, 3), bound, reductions, graph); // apply reduction like this
    int vertices_cnt = 0;
    int forbidden_edges_cnt = 0;
    int ub = -1;
    while(true){

        while(true){
            auto p = find_2k_reducible_vertex(graph);
            if(p.first != 1) break;
            apply(new TwokReduction(p.second), bound, reductions, graph);
        }

        while(true) {
            int ubx = bound.upper;
            ubx = bound.upper;
            Bound b(0, bound.upper);
            apply(new UnaffordableEdgeReduction(), b, reductions, graph);
            bound.upper = b.upper;
            if(ubx == bound.upper) break;
        }

        while(true) {
            auto almost_clique = find_almost_clique(*graph);
            if(almost_clique.size() >= 2) {
                apply(new AlmostCliqueReduction(almost_clique), bound, reductions, graph);
            }else{
                break;
            }
        }

        // compute if progress has been made
        int new_vertices_cnt = 0;
        int new_forbidden_edges_cnt = 0;
        int new_ub = bound.upper;
        for(int v : graph->all_vertices()){
            new_vertices_cnt++;
        for(int u : graph->all_vertices()){
            if(u<v) {
                if(graph->get_edge_weight(u,v)==FORBIDDEN_WEIGHT)new_forbidden_edges_cnt++;
            }
        }}

        // progress
        if(ub == -1 || new_vertices_cnt < vertices_cnt || new_forbidden_edges_cnt < forbidden_edges_cnt || new_ub < ub) {
            vertices_cnt = new_vertices_cnt;
            forbidden_edges_cnt = new_forbidden_edges_cnt;
            ub = new_ub;

            // std::cerr<<"vertices_cnt="<<vertices_cnt<<std::endl;
            // std::cerr<<"forbidden_edges_cnt="<<forbidden_edges_cnt<<std::endl;
            // std::cerr<<"ub="<<ub<<std::endl;
        }
        else {
            break;
        }
    }
}

// ====================================================================================================

// Signal handling

std::function<void(int)> shutdown_handler;
void signal_handler(int signal) { (void) signal; shutdown_handler(signal); }





void filter_out_complete_components(WeightedGraph *&reduced_graph){
    vector<vector<int>> change_graph_cliques;
    for(auto c : get_components(*reduced_graph)) {
        bool is_clique = true;
        for(int v : c) {
            for(int u : c) {
                if(u<v)if(reduced_graph->get_edge_weight(u, v) != 1) is_clique=false;
            }
        }
        if(is_clique) {
            change_graph_cliques.push_back(c);
        }
    }
    set<int> change_graph_remaining_vertices;
    for(int v : reduced_graph->all_vertices())change_graph_remaining_vertices.insert(v);
    for(auto c : change_graph_cliques) {
        for(int v : c)change_graph_remaining_vertices.erase(v);
    }

    if(change_graph_cliques.size()) {
        vector<int> change_graph_remaining_vertices_vector(change_graph_remaining_vertices.begin(), change_graph_remaining_vertices.end());
        reduced_graph = reduced_graph->induced_subgraph(change_graph_remaining_vertices_vector);
    }
}

// ====================================================================================================

int main ( void ) {
    int end_immediately = 0;
    shutdown_handler = [&end_immediately](int signal){ (void) signal; end_immediately = 1; };
    std::signal(SIGTERM, signal_handler);

    InputReader in;

    auto graphFactory = [](int SZ){ return new MaterializedWeightedGraph(SZ); };
    Graph *g = in.read( cin );
    // int SZ=g->get_vertices_count();
    WeightedGraph * input_graph = convert_graph_to_weighted_graph(*g, graphFactory);
    // WeightedGraph * original_graph = convert_graph_to_weighted_graph(*g, graphFactory);
    WeightedGraph * reduced_graph = convert_graph_to_weighted_graph(*g, graphFactory);
    delete g;

    // cerr << input_graph->size() << ' ' << reduced_graph->size() << endl;
    assert(input_graph->size() == reduced_graph->size());
    input_graph->recompute_structures();

    auto umb = meta_upper_bound(*input_graph);

    int ini_bnd = umb.first;
    Bound bound(ini_bnd, ini_bnd); // exact
    WeightedGraph *reduced_weighted = input_graph->copy();
    vector<ReductionRule<WeightedGraph>*> reductions;
    // apply_unweighted_reductions(reduced_weighted, bound, reductions);
    apply_reductions(reduced_weighted, bound, reductions);

    auto flipped_edges = changed_edges(reduced_graph, reduced_weighted, reductions);
    int cnt_flipped_edges = change_graph(reduced_graph, reduced_weighted, reductions);
    delete reduced_weighted;

    assert((int)flipped_edges.size() == cnt_flipped_edges);

    // apply_unweighted_reductions(reduced_graph, bound, reductions);

    filter_out_complete_components(reduced_graph);

    {
        Solution *solution = nullptr;
        if(reduced_graph->size() == 0){
            solution = new Solution();
        }else{
            reduced_graph->recompute_structures();
            // std::cerr<<"compute_exactly bound.upper="<<umb.first - cnt_flipped_edges<<std::endl;
            solution = compute_exactly(reduced_graph, umb.first - cnt_flipped_edges, &end_immediately);
            // std::cerr<<"compute_exactly solution="<<solution<<std::endl;
        }
        if(solution != nullptr){
            // assert(is_solution(reduced_graph, solution).size()==0);
            flip_solution_edges(solution, flipped_edges);
            cout << solution->size() << endl;
            cout << "p cep " << 0 << " " << 0 << endl;
            for(int i = 0; i < 4; ++i){
                cout << *solution;
            }
            return 0;
        }
    }


    int reduced_edges = ini_bnd - bound.upper;
    if(cnt_flipped_edges > reduced_edges){ // the kernelization may inefficiently flip edges multiple times
        cerr << "flipped_edges: " << cnt_flipped_edges << ", reduced_edges: " << reduced_edges << endl;
        assert(cnt_flipped_edges <= reduced_edges);
    }
    cout << cnt_flipped_edges << endl; // value for D
    auto edges = get_edges(reduced_graph);
    map<int,int> remapping;
    {
        auto vs = reduced_graph->all_vertices();
        for(int i=0, idx=0; i<(int)vs.size(); ++i) {
            if(remapping.find(vs[i]) == remapping.end()) remapping[vs[i]] = ++idx;
        }
    }

    cout << "p cep " << reduced_graph->size() << ' ' << edges.size() << endl;
    for(auto &e : edges) cout << remapping[e.first] << ' ' << remapping[e.second] << endl;
    auto sols = get_solutions(reduced_graph);
    for(auto sol : sols){
        auto s = sol->get_solution(reduced_graph);
        // assert(is_solution(reduced_graph, s).size()==0);
        flip_solution_edges(s, flipped_edges);

        // assert(is_solution(original_graph, s).size() == 0);
        auto sol_edges = s->getSolution();
        cout << sol_edges.size() << endl;
        for(auto p : sol_edges){
            // cout << remapping[p.first] << ' ' << remapping[p.second] << endl;
            cout << (1+p.first) << ' ' << (1+p.second) << endl;
        }
    }
    delete input_graph;
    delete reduced_graph;
    for(auto sol : sols) delete sol;
    return 0;
}
