#pragma once

#include "strategy.hpp"
#include "../node/node.hpp"

#include <functional>
#include <unordered_set>
#include <map>

using std::function;
using std::map;
using std::unordered_set;
using std::pair;

class PriorityStrategy : public SelectNodeStrategy {
    public:

        PriorityStrategy(function<int(const Node *)> evaluate_node_fun);

        virtual ~PriorityStrategy();

        virtual Node *next_node();

        virtual void created_node(Node *n);

        virtual void changed_node(Node *n);

        virtual void deleted_node(Node *n);

    private:
        int seq_num;
        unordered_set<Node*> nodes;
        map<pair<int,int>, Node*> priorities;
        function<int(const Node *)> evaluate_node_fun;
};
