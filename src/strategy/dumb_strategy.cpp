#include "dumb_strategy.hpp"

DumbStrategy::DumbStrategy() {}

DumbStrategy::~DumbStrategy() {}

Node *DumbStrategy::next_node(){
    return *nodes.begin();
}

void DumbStrategy::created_node(Node *n){
    if(should_be_evaluated(n->get_state())) {
        nodes.insert(n);
    }
}

void DumbStrategy::changed_node(Node *n){
    if(nodes.count(n) && !should_be_evaluated(n->get_state())) {
        nodes.erase(n);
    }
}

void DumbStrategy::deleted_node(Node *n){
    if(nodes.count(n)){
        nodes.erase(n);
    }
}
